﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Retro2D;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Media.Effects;
using TiledSharp;

namespace TowerDefenseMono
{
    public class Level : Node
    {
        #region Attributes

        // Use for test if unit is in camera view ! for optimisiation !!
        float _expandCamera = 0f;
        Vector2 _centerCamera = new Vector2();
        RectangleF _rectCamera = new RectangleF();

        Vector2 CenterCamera => _centerCamera;
        RectangleF RectCamera => _rectCamera;


        // Mouse for Level
        public Input.Mouse _mouseLevel = new Input.Mouse();

        // Player
        public Player _player { get; private set; }
        //public static PlayerHUD _playerHUD;

        // Path
        //string _pathFileName = "Content/level_zero_paths.xml";
        List<Gfx.Path> _listPath = new List<Gfx.Path>();

        //Gfx.Path _testPath = new Gfx.Path();

        Gfx.Path.Flag _flags =
              Gfx.Path.Flag.Name
            //| Gfx.Path.Flag.Editable
            //| Gfx.Path.Flag.ControlPoint
            //| Gfx.Path.Flag.ControlLine
            //| Gfx.Path.Flag.Border
            //| Gfx.Path.Flag.Interval
            //| Gfx.Path.Flag.IndexPoint
            //| Gfx.Path.Flag.IndexLine
            ;

        // Collision Grid
        Collision2DGrid _collision2DGrid;

        // Node Layer
        public Node _layerGame { get; private set; }
        public Node _layerGameUI { get; private set; }
        public Node _layerHUD { get; private set; }

        public bool IsLose => _player.IsGameOver;
        public bool IsWin => _endWave && _layerGame.SubGroupOf(UID.Get<Unit.Enemy0>()).Count == 0;
        //bool _isPause = false;

        //Debug 
        int _ticWaitAddEnemy = 0;
        int _tempoWaitAddEnemy = 640;
        int _ticAddEnemy = 0;
        int _tempoAddEnemy = 60;
        bool _onAddEnemy = false;


        // WAVE !!
        public const int MAX_WAVE = 25;

        public int _wave { get; private set; } = 1;
        bool _endWave = false;


        // CAMERA -----------------------------------------

        bool _isLimitCameraMove = true;

        Vector2 _camera = new Vector2();
        Vector2 _cameraSpeed = new Vector2(10, 10);
        public Vector2 Camera => _camera;

        bool IS_B_UP = false;
        bool IS_B_DOWN = false;
        bool IS_B_LEFT = false;
        bool IS_B_RIGHT = false;

        float _angleMove;
        float _speed;
        float _velocity;
        float _acceleration;
        float _vx;
        float _vy;


        // Mouse scrolling


        float _dragX;
        float _dragY;
        bool _isDragMap = false;

        // ClipBoardSystem
        //Vector2 POS_BUILDABLE = new Vector2(-1000);
        //Vector2 POS_DEF_RALLY_POINT = new Vector2(-1000);

        int _mapSizeW;
        int _mapSizeH;

        Addon.Loop _loop = new Addon.Loop();
        Shake _shake = new Shake();

        // TILE MAP
        public Texture2D _tex_miniMap { get; private set; }
        TmxMap _tmxMap;
        Texture2D _tileSet;

        TileMap2D _tileMap2D;
        TileMapLayer[] _layers = new TileMapLayer[10];

        #endregion

        public Level(ContentManager content)
        {
            LoadLevel(content);
            Init();
        }
        public void LoadLevel(ContentManager content)
        {
            _layerGame = new Node().AppendTo(this);
            _layerGameUI = new Node().AppendTo(this);
            _layerHUD = new Node().AppendTo(this);

            _layerHUD._naviGate = new NaviGate(this);
            _layerHUD._naviGate.SetNaviGate(true);

            //_tmxMap = new TmxMap("Content/level_zero.tmx");
            _tmxMap = new TmxMap("Content/level_one.tmx");

            string textureFilePath = "Image/" + Path.GetFileNameWithoutExtension(_tmxMap.Tilesets[0].Image.Source.ToString());

            System.Console.WriteLine("Load TMX MAP : " + textureFilePath);

            _tileSet = content.Load<Texture2D>(textureFilePath);

            //_tileSet = content.Load<Texture2D>("Image/" +_tmxMap.Tilesets[0].Name.ToString());

            _tex_miniMap = content.Load<Texture2D>("Image/level_one");

            // Save Layer from TmxMap
            _layers[0] = TileMap2D.CreateLayer(_tmxMap, _tmxMap.Layers["BG"], _tileSet);
            _layers[1] = TileMap2D.CreateLayer(_tmxMap, _tmxMap.Layers["FG"], _tileSet);
            _layers[2] = TileMap2D.CreateLayer(_tmxMap, _tmxMap.Layers["FG2"], _tileSet);

            _tileMap2D = new TileMap2D();
            _tileMap2D.Setup(new Rectangle(0, 0, Game1._screenW, Game1._screenH), _tmxMap.Width, _tmxMap.Height, _tmxMap.TileWidth, _tmxMap.TileHeight);

            _mapSizeW = _tmxMap.Width * _tmxMap.TileWidth;
            _mapSizeH = _tmxMap.Height * _tmxMap.TileHeight;

            // Create CollisionGrid
            _collision2DGrid = new Collision2DGrid(_mapSizeW / 64, _mapSizeH / 64 + 1, 64); // --- VERY IMPORTANT FOR PRECISE COLLISION !!

            // Test Create path from Tmx
            int index = 0;
            foreach (var tmxObject in _tmxMap.ObjectGroups["PATH"].Objects)
            {
                _listPath.Add(new Gfx.Path(tmxObject.Name, _flags));

                float X = (float)(tmxObject.X);
                float Y = (float)(tmxObject.Y);

                _listPath[index].Add(X, Y);
                for (int i = 1; i < tmxObject.Points.Count; i++)
                {
                    _listPath[index].Add((float)(tmxObject.Points[i].X + X), (float)(tmxObject.Points[i].Y + Y));
                }
                index++;
            }

            foreach (Gfx.Path path in _listPath)
            {
                path.PathSize = 90;
                path.PathDivSize = .1f;
            }

            // Setup Player with TmxMap data
            //_playerHUD = new PlayerHUD(this, Input._mouse, _tmxMap, _player);

            _player = new Player(this, _mouseLevel, _tmxMap);

            // Setuo RectView Camera ViewPort
            _layerGame.SetRectView(new RectangleF(0, 0, Game1._screenW, Game1._screenH));
            //_layerGame.SetRectView(new RectangleF(_expandCamera, _expandCamera, Game1._screenW + _expandCamera * 2, Game1._screenH + _expandCamera * 2));

        }
        public override Node Init()
        {

            //_playerHUD.Init();
            _player.Init();


            System.Console.WriteLine("Init ScreenPlay !");
            // Reset All Layers
            int nbKill = 0;

            //nbKill = _layerUI.KillAllAndKeep(UID.Get<UICircleSelect>());
            nbKill = _layerGameUI.KillAll();
            //System.Console.WriteLine("_layerUI Killed : {0}", nbKill);

            //nbKill = _layerGame.KillAllAndKeep(new int[] { UID.Get<BaseBuilding>(), UID.Get<Buildable>() });
            nbKill = _layerGame.KillAll();
            //System.Console.WriteLine("_layerGame Killed : {0}", nbKill);


            foreach (var tmxObject in _tmxMap.ObjectGroups["BUILDABLE"].Objects)
            {
                float X = (float)(tmxObject.X + tmxObject.Points[0].X);
                float Y = (float)(tmxObject.Y + tmxObject.Points[0].Y);

                float RX = (float)(tmxObject.Points[1].X + X);
                float RY = (float)(tmxObject.Points[1].Y + Y);

                new Buildable(_player, _mouseLevel, _layerGameUI, new Vector2(X, Y), new Vector2(RX, RY)).AppendTo(_layerGame);
            }


            // Debug reset Wave
            _wave = 1;
            _ticWaitAddEnemy = 0;
            _tempoWaitAddEnemy = 640;
            _ticAddEnemy = 0;
            _tempoAddEnemy = 60;
            _onAddEnemy = false;
            _endWave = false;

            _player.SetBank(480);

            //foreach(var baseBuilding in _layerGame.GroupOf(UID.Get<BaseBuilding>()))
            //{
            //    var building = baseBuilding.This<BaseBuilding>();
            //    new Buildable(_player, Input._mouse, _layerUI, new Vector2(building._x, building._y), new Vector2(building.DefaultRallyPoint.X, building.DefaultRallyPoint.Y)).AppendTo(_layerGame);
            //    System.Console.WriteLine("Buildable Recreated at : {0}, {1}", building._x, building._y);
            //    building.KillMe();
            //}

            //LoadPath(_pathFileName);

            // Reload Flags after loadingPath !
            foreach (Gfx.Path path in _listPath)
                path._flag = _flags;

            // Play music at start !
            MediaPlayer.Play(Game1._song_Music0);
            MediaPlayer.Volume = Game1._sliderMusicVolume.Value * 0.08f;
            MediaPlayer.IsRepeating = true;

            _speed = 20f;
            _acceleration = 1f;



            _loop.SetLoop(1f, 1f, 1.1f, 0.01f, Loops.PINGPONG);
            _loop.Start();

            return base.Init();
        }
        public void Move(float angleMove, float velocity)
        {
            _vx = Geo.GetVector(angleMove).X * velocity;
            _vy = Geo.GetVector(angleMove).Y * velocity;
        }
        public void SetCameraPos(float x, float y)
        {
            _camera.X = x;
            _camera.Y = y;
        }
        public override Node Update(GameTime gameTime)
        {

            _loop.Update();

            _mouseLevel.Update(Game1._relMouseX, Game1._relMouseY, (Game1._mouseState.LeftButton == ButtonState.Pressed) ? 1 : 0, Game1._mouseState.ScrollWheelValue, (int)-_camera.X, (int)-_camera.Y);

            #region Debug TEST Add Enemy Unit

            int min = 10;
            int max = 90;

            //Debug Auto Add Enemy
            if (!_endWave)
            {
                _ticWaitAddEnemy++;

                if (_ticWaitAddEnemy > _tempoWaitAddEnemy + _wave * 8)
                {
                    _ticAddEnemy++;
                    if (_ticAddEnemy > _tempoAddEnemy)
                    {
                        _ticAddEnemy = 0;
                        _onAddEnemy = true;
                    }
                    if (_ticWaitAddEnemy > _tempoWaitAddEnemy + 800 + _wave * 8)
                    {
                        //_tempoWaitAddEnemy = Misc.Rng.Next(400, 800);
                        _ticWaitAddEnemy = 0;

                        _wave++;
                        if (_wave > MAX_WAVE)
                        {
                            _wave = MAX_WAVE;
                            _endWave = true;
                        }
                    }
                }
            }

            if (Input.Button.OnePress("newEnemy", Keyboard.GetState().IsKeyDown(Keys.Insert)) || _onAddEnemy)
            {
                _onAddEnemy = false;

                int indexPath = Misc.Rng.Next(0, _listPath.Count);

                Gfx.Path path = _listPath[indexPath];

                var pathLine = path.SetPathLine(path.PathSize);

                float percent = Misc.Rng.Next(min, max);
                Vector2 start = pathLine[0].Percent(percent);

                Unit.Enemy0 unit = new Unit.Enemy0(_player, _mouseLevel, Unit.Enemy0.RangeLevel1);

                float speed = Misc.Rng.Next(12, 16) / 10f;
                unit.SetEnergy(64 + _wave * 18);
                unit.Setup(start.X, start.Y, speed);
                //unit.Setup(start.X, start.Y, 1f);

                foreach (Gfx.Line line in pathLine)
                {
                    Vector2 goal = line.Percent(percent);
                    unit.AddGoal(goal.X, goal.Y);
                }

                unit.AppendTo(_layerGame);

            }

            // Unit reach end of path !
            //foreach (Unit.Enemy0 unit in _layerGame.SubGroupOf(UID.Get<Unit.Enemy0>()))
            //{
            //    if (unit.EndOfMove())
            //    {
            //        //System.Console.WriteLine("End of Goal : " + unit._index);

            //        //var pathLine = _path.SetPathLine(_path.PathSize);

            //        //float percent = Misc.Rng.Next(min, max);
            //        //Vector2 start = pathLine[0].Percent(percent);

            //        //unit.Setup(start.X, start.Y, Misc.Rng.Next(4, 16) / 10f);

            //        //foreach (Gfx.Line line in pathLine)
            //        //{
            //        //    Vector2 goal = line.Percent(percent);
            //        //    unit.AddGoal(goal.X, goal.Y);
            //        //}

            //        unit.KillUnit();
            //    }
            //}
            #endregion

            #region Button Events

            //if (Input.Button.OnePress("Retry", Keyboard.GetState().IsKeyDown(Keys.Back)))
            //{
            //    System.Console.WriteLine("Retry");
            //    Init();
            //}

            //if (Input.Button.OnePress("PauseGame", Keyboard.GetState().IsKeyDown(Keys.Space)))
            //    _isPause = !_isPause;

            //if (Input.Button.OnePress("SavePath", Keyboard.GetState().IsKeyDown(Keys.LeftControl) && Keyboard.GetState().IsKeyDown(Keys.S)))
            //    SavePath(_pathFileName);

            //if (Input.Button.OnePress("LoadPath", Keyboard.GetState().IsKeyDown(Keys.RightControl) && Keyboard.GetState().IsKeyDown(Keys.L)))
            //    LoadPath(_pathFileName);

            #endregion

            //if (!_player.IsGameOver) //  && !IsWin())
            {
                for (int i = 0; i < _listPath.Count; i++)
                    _listPath[i].Update(_mouseLevel);

                //_testPath.Update(Input._mouse);

                #region Camera Movement Control

                IS_B_UP = Keyboard.GetState().IsKeyDown(Keys.Up) || _mouseLevel._y <= 8;
                IS_B_DOWN = Keyboard.GetState().IsKeyDown(Keys.Down) || _mouseLevel._y >= Game1._screenH - 8;
                IS_B_LEFT = Keyboard.GetState().IsKeyDown(Keys.Left) || _mouseLevel._x <= 8;
                IS_B_RIGHT = Keyboard.GetState().IsKeyDown(Keys.Right) || _mouseLevel._x >= Game1._screenW - 8;

                _vx = 0;
                _vy = 0;

                if (!(IS_B_UP || IS_B_DOWN || IS_B_LEFT || IS_B_RIGHT))
                {
                    _velocity -= _acceleration * 2f;
                    if (_velocity <= 0) _velocity = 0;
                }
                else
                {
                    _velocity += _acceleration;
                }

                if (_velocity > _speed) _velocity = _speed;

                if (IS_B_UP && !IS_B_DOWN && !IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_U, _velocity);
                if (!IS_B_UP && IS_B_DOWN && !IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_D, _velocity);
                if (!IS_B_UP && !IS_B_DOWN && IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_L, _velocity);
                if (!IS_B_UP && !IS_B_DOWN && !IS_B_LEFT && IS_B_RIGHT) Move(_angleMove = Geo.RAD_R, _velocity);

                if (IS_B_UP && !IS_B_DOWN && IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_UL, _velocity);
                if (IS_B_UP && !IS_B_DOWN && !IS_B_LEFT && IS_B_RIGHT) Move(_angleMove = Geo.RAD_UR, _velocity);
                if (!IS_B_UP && IS_B_DOWN && IS_B_LEFT && !IS_B_RIGHT) Move(_angleMove = Geo.RAD_DL, _velocity);
                if (!IS_B_UP && IS_B_DOWN && !IS_B_LEFT && IS_B_RIGHT) Move(_angleMove = Geo.RAD_DR, _velocity);

                if (_mouseLevel._mouseWheelUp) _vy = 120;
                if (_mouseLevel._mouseWheelDown) _vy = -120;

                _cameraSpeed.X = -_vx;
                _cameraSpeed.Y = -_vy;

                _camera.X += _cameraSpeed.X;
                _camera.Y += _cameraSpeed.Y;

                if (_isDragMap)
                {
                    _camera.X = _mouseLevel._x - _dragX;
                    _camera.Y = _mouseLevel._y - _dragY;
                }

                // Limit Camera Move
                if (_isLimitCameraMove)
                {
                    if (_camera.Y >= 0) _camera.Y = 0;
                    if (_camera.Y <= -_mapSizeH + Game1._screenH) _camera.Y = -_mapSizeH + Game1._screenH;
                    if (_camera.X >= 0) _camera.X = 0;
                    if (_camera.X <= -_mapSizeW + Game1._screenW) _camera.X = -_mapSizeW + Game1._screenW;
                }

                #endregion


                _layerGame.SetPosition(_camera);
                _layerGameUI.SetPosition(_camera);

                //_playerHUD.Update(gameTime);
                _player.Update(gameTime);

                _layerHUD.UpdateChilds(gameTime);

                _layerGameUI.UpdateChilds(gameTime);
                _layerGame.UpdateChilds(gameTime);

                if (_mouseLevel._onClick && !_mouseLevel._isOver && Selectable.IsSelected())
                {
                    // Reset all selections un layerGame !
                    Selectable.SetAllSelected(false);
                    System.Console.WriteLine("< Reset All >");
                }
                // Mouse Drag 
                //if (Input._mouse._isClick && !Input._mouse._isOver && !_isDragMap)
                if (_mouseLevel._isClick && !_mouseLevel._isOver && !_isDragMap)// && Keyboard.GetState().IsKeyDown(Keys.Tab))
                {
                    _isDragMap = true;

                    if (_mouseLevel._onClick)
                    {
                        _dragX = _mouseLevel._x - _camera.X;
                        _dragY = _mouseLevel._y - _camera.Y;
                    }
                }

                if (!_mouseLevel._isClick)
                    _isDragMap = false;

                // Collision2D System Run !
                _collision2DGrid.SetPosition(0, 0);
                Collision2D.ResetAllZone(_layerGame);
                Collision2D.GridSystemZone(_layerGame, _collision2DGrid);

                UpdateChilds(gameTime);


                //if (_playSoundFire && _soundFire.State == SoundState.Stopped)
                //{
                //    _soundFire.Volume = .04f;
                //    _soundFire.Pitch = .01f;
                //    _soundFire.Play();
                //}
            }

            //if (Input._mouse._onClick && Keyboard.GetState().IsKeyDown(Keys.B)) POS_BUILDABLE = new Vector2(Input._mouse.AbsX, Input._mouse.AbsY);
            //if (Input._mouse._onClick && Keyboard.GetState().IsKeyDown(Keys.R)) POS_DEF_RALLY_POINT = new Vector2(Input._mouse.AbsX, Input._mouse.AbsY);

            //if (Input.Button.OnePress("CopyPosition", Keyboard.GetState().IsKeyDown(Keys.LeftControl) && Keyboard.GetState().IsKeyDown(Keys.C)))
            //    CopyBuildingPosition();


            // Update Camera Relative ViewPort

            _centerCamera.X = Game1._screenW / 2 - _camera.X;
            _centerCamera.Y = Game1._screenH / 2 - _camera.Y;

            //_rectCamera.X = _centerCamera.X - Game1._screenW / 2 + _expandCamera;
            //_rectCamera.Y = _centerCamera.Y - Game1._screenH / 2 + _expandCamera;

            //_rectCamera.X = _expandCamera;
            //_rectCamera.Y = _expandCamera;
            //_rectCamera.Width = Game1._screenW - _expandCamera * 2;
            //_rectCamera.Height = Game1._screenH - _expandCamera * 2;

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            batch.GraphicsDevice.Clear(new Color(40, 60, 100));

            //batch.Draw(Game1._testLevel, _camera, Color.CornflowerBlue);

            // BackGround TileMap
            _tileMap2D.Render(batch, _layers[0], Color.White, (int)_camera.X, (int)_camera.Y);
            // ForeGround TileMap
            //_tileMap2D.Render(batch, _layers[1], Color.Black * .4f, (int)_camera.X, (int)_camera.Y+32);

            _tileMap2D.Render(batch, _layers[1], Color.White, (int)_camera.X, (int)_camera.Y);
            _tileMap2D.Render(batch, _layers[2], Color.White, (int)_camera.X, (int)_camera.Y);



            //Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, 32, 32, Color.Black * .2f);

            // Debug : Draw Path if flags are activated
            for (int i = 0; i < _listPath.Count; i++)
                _listPath[i].Render(batch, Game1._font_Main, _mouseLevel, _camera, 32);

            //_testPath.Render(batch, Game1._mainFont, Input._mouse, _camera, 32);
            //_path2.Render(batch, Game1._mainFont, Input._mouse, 32);
            //_path.DrawSplineVector(batch, 0, 4, Color.Gold, 2);

            _layerGame.SortZD();
            _layerGame.RenderChilds(batch);


            _layerGameUI.RenderChilds(batch);


            //Draw.Sight(batch, _game._relMouseX, _game._relMouseY, _game._screenW, _game._screenH, Color.Red * .8f, 1);

            //_collision2DGrid.Render(batch, Game1._mainFont, Color.Orange*.4f);

            //if (_player.IsGameOver)
            //{
            //    Draw.FillRectangle(batch, new Rectangle(0, Game1._screenH / 2 - 24, Game1._screenW, 80), Color.Black * .5f);
            //    Draw.CenterBorderedStringXY(batch, Game1._font_Big, "G A M E  O V E R", Game1._screenW / 2, Game1._screenH / 2, Color.OrangeRed, Color.Red);
            //    Draw.CenterBorderedStringXY(batch, Game1._font_Main, "Never Give Up", Game1._screenW / 2, Game1._screenH / 2 + 32, Color.Orange, Color.Black);
            //}

            //if (IsWin())
            //{
            //    Draw.FillRectangle(batch, new Rectangle(0, Game1._screenH / 2 - 24, Game1._screenW, 80), Color.Black * .5f);
            //    Draw.CenterBorderedStringXY(batch, Game1._font_Big, "YOU WIN", Game1._screenW / 2, Game1._screenH / 2, Color.Yellow, Color.Black);

            //    string winMessage = "CONGRATULATION";

            //    if (_player.Life <= 5) winMessage = "Hum, You need more pratice !";
            //    if (_player.Life <= 10) winMessage = "Yes you can, keep going !";
            //    if (_player.Life <= 15) winMessage = "You know it's possible !";
            //    if (_player.Life <= 18) winMessage = "Not bad, you are a PRO!";
            //    if (_player.Life == 19) winMessage = "Congratulation, near Perfection !";
            //    if (_player.Life > 19) winMessage = "Awesome ! God is You !";

            //    Draw.CenterBorderedStringXY(batch, Game1._font_Main, winMessage, Game1._screenW / 2, Game1._screenH / 2 + 32, Color.Orange, Color.Black);
            //}

            //Draw.Line(batch, POS_BUILDABLE + _camera, POS_DEF_RALLY_POINT + _camera, Color.White * .5f, 2);
            //Draw.Ellipse(batch, POS_BUILDABLE + _camera, new Vector2(64, 48), 16, Color.Yellow, 2);
            //Draw.Point(batch, POS_DEF_RALLY_POINT + _camera, 8, Color.Orange);

            //Draw.Arc(batch, new Vector2(400, 400), 80, 200, Geo.RAD_0, Geo.RAD_225, Color.Cyan, 20);
            //Draw.Point(batch, new Vector2(800, 800), 4, Color.Orange);

            if (_mouseLevel._isClick && !_mouseLevel._isOver)
            {
                //Draw.Circle(batch, new Vector2(Input._mouse._x, Input._mouse._y), 16, 16, Color.LawnGreen, 4);
                batch.Draw(
                    Game1._tex_mouseDragMove,
                    new Vector2(_mouseLevel._x, _mouseLevel._y),
                    null,
                    Color.LawnGreen,
                    0,
                    new Vector2(Game1._tex_mouseDragMove.Width / 2, Game1._tex_mouseDragMove.Height / 2),
                    new Vector2(.64f * _loop._current, .48f * _loop._current),
                    SpriteEffects.None, 0);
                //batch.Draw(Game1._mouseDragMove, new Vector2(Input._mouse._x - Game1._mouseDragMove.Width/2, Input._mouse._y - Game1._mouseDragMove.Height/2), Color.LawnGreen);
            }

            _player.Render(batch);

            _layerHUD.RenderChilds(batch);

            //Draw.RightCenterBorderedString(batch, Game1._font_Main, "Press <Back> to Retry", Game1._screenW - 8, 16, Color.Yellow, Color.DarkRed);



            #region Debug Zone
            
            //Draw.Rectangle(batch, Gfx.TranslateRect(_rectCamera, _camera), Color.BlueViolet, 2);
            //Draw.Rectangle(batch, Gfx.TranslateRect(_rectCamera, Vector2.Zero) , Color.GreenYellow, 2);

            //Draw.Circle(batch, _centerCamera + _camera, 32, 32, Color.Orange);
            //batch.DrawString(Game1._font_Main, "Center Camera = " + _centerCamera, new Vector2(Game1._screenW / 2, Game1._screenH / 2), Color.Yellow);

            #endregion

            return base.Render(batch);
        }
    }
}
