<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.3.3" name="tileSetZero" tilewidth="24" tileheight="24" tilecount="375" columns="10">
 <grid orientation="orthogonal" width="11" height="6"/>
 <image source="Image/tileSetZero.png" trans="ffffff" width="240" height="240"/>
 <tile id="0" type="0">
  <properties>
   <property name="passLevel" type="int" value="3"/>
  </properties>
 </tile>
 <tile id="1" type="0"/>
 <tile id="2" type="0"/>
 <tile id="3" type="0"/>
 <tile id="4" type="0"/>
 <tile id="5" type="0"/>
 <tile id="6" type="0"/>
 <tile id="7" type="0"/>
 <tile id="8" type="0"/>
 <tile id="9" type="0"/>
 <tile id="10" type="0"/>
 <tile id="11" type="0">
  <properties>
   <property name="passLevel" type="int" value="2"/>
  </properties>
 </tile>
 <tile id="12" type="0"/>
 <tile id="13" type="0"/>
 <tile id="14" type="0"/>
 <tile id="15" type="0"/>
 <tile id="16" type="0"/>
 <tile id="17" type="0"/>
 <tile id="18" type="0"/>
 <tile id="19" type="0"/>
 <tile id="20" type="0"/>
 <tile id="21" type="0"/>
 <tile id="22" type="0"/>
 <tile id="23" type="0"/>
 <tile id="24" type="0"/>
 <tile id="25" type="0"/>
 <tile id="26" type="0"/>
 <tile id="27" type="0"/>
 <tile id="28" type="0"/>
 <tile id="29" type="0"/>
 <tile id="30" type="0"/>
 <tile id="31" type="0"/>
 <tile id="32" type="0"/>
 <tile id="33" type="0"/>
 <tile id="34" type="0"/>
 <tile id="35" type="0"/>
 <tile id="36" type="0"/>
 <tile id="37" type="0"/>
 <tile id="38" type="0"/>
 <tile id="39" type="0"/>
 <tile id="40" type="0"/>
 <tile id="41" type="0"/>
 <tile id="42" type="0"/>
 <tile id="43" type="0"/>
 <tile id="44" type="0"/>
 <tile id="45" type="0"/>
 <tile id="46" type="0"/>
 <tile id="47" type="0"/>
 <tile id="48" type="0"/>
 <tile id="49" type="0"/>
 <tile id="50" type="0"/>
 <tile id="51" type="0"/>
 <tile id="52" type="0"/>
 <tile id="53" type="0"/>
 <tile id="54" type="0"/>
 <tile id="55" type="0"/>
 <tile id="56" type="0"/>
 <tile id="57" type="0"/>
 <tile id="58" type="0"/>
 <tile id="59" type="0"/>
 <tile id="60" type="0"/>
 <tile id="61" type="0"/>
 <tile id="62" type="0"/>
 <tile id="63" type="0"/>
 <tile id="64" type="0"/>
 <tile id="65" type="0"/>
 <tile id="66" type="0"/>
 <tile id="67" type="0"/>
 <tile id="68" type="0"/>
 <tile id="69" type="0"/>
 <tile id="70" type="0"/>
 <tile id="71" type="0"/>
 <tile id="72" type="0"/>
 <tile id="73" type="0"/>
 <tile id="74" type="0"/>
 <tile id="75" type="0"/>
 <tile id="76" type="0"/>
</tileset>
