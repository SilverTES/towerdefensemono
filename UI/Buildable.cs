﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using MonoGame.Extended;
using Retro2D;
using TowerDefenseMono.Building;

namespace TowerDefenseMono
{
    class Buildable : Selectable
    {

        #region Attributes
        Node _layerUI;
        UICircleSelect _uiSelect;

        Addon.Loop _loops = new Addon.Loop();

        float _angle = Geo.RAD_0;

        bool _autoDestroy = false;

        // Building Range
        Vector2 _range;

        Button _buttonTower1;
        Button _buttonTower2;
        Button _buttonTower3;
        Button _buttonTower4;

        Vector2 _defaultRallyPoint; // Default rallyPoint;


        
        #endregion

        public Buildable(Player player, Input.Mouse mouse, Node layerUI, Vector2 position, Vector2 defaultRallyPoint) : base(player, mouse)
        {
            _type = UID.Get<Buildable>();

            _layerUI = layerUI;

            SetSize(128, 96);
            SetPivot(Position.CENTER);
            SetPosition(position);
            _defaultRallyPoint = defaultRallyPoint;

            

            _uiSelect = new UICircleSelect(_mouse, 96);
            _uiSelect.AppendTo(_layerUI);

            _buttonTower1 = _uiSelect.AddButton(new Button(_player, Game1._tex_GunTurret, _mouse, new Message(UID.Get<Button>(), "BUILD_0", this), "Build GunTurret", false, 70), Geo.RAD_UL);
            _buttonTower2 = _uiSelect.AddButton(new Button(_player, Game1._tex_Barrack, _mouse, new Message(UID.Get<Button>(), "BUILD_1", this), "Build Barrack", false, 100), Geo.RAD_UR);
            //_buttonTower3 = _uiSelect.AddButton(new Button(Game1._player, Game1._iconTower00, _mouse, new Message(UID.Get<Button>(), "BUILD_2", this), 90), Geo.RAD_DL);
            //_buttonTower4 = _uiSelect.AddButton(new Button(Game1._player, Game1._iconTower00, _mouse, new Message(UID.Get<Button>(), "BUILD_3", this), 70), Geo.RAD_DR);

            _loops.SetLoop(0, 0, 4, .5f, Loops.PINGPONG);
            _loops.Start();
        }

        public override Node Update(GameTime gameTime)
        {
            _loops.Update();

            UpdateRect();

            _z = -_y;

            _uiSelect.SetPosition(XY);

            _range = Vector2.Zero;

            _angle -= .01f;
            if (_angle < Geo.RAD_0) _angle = Geo.RAD_360;

            _isMouseOver = Collision2D.PointInEllipse(new Vector2(_mouse._x, _mouse._y), AbsXY, _rect.Width / 2, _rect.Height / 2) && !_mouse._isOver;

            if (_onSelected)
            {
                _uiSelect.Show();
                //System.Console.WriteLine("Show UISelect");
                Game1._sound_Key.Play(.05f * Game1._volumeSound, .2f, 0f);
            }

            if (_offSelected)
            {
                _uiSelect.Hide();
                //System.Console.WriteLine("Hide UISelect");
            }


            //if (_uiSelect._isActive)
            //_uiSelect.Update(gameTime);

            if (_uiSelect.IsShow)
            {
                if (_buttonTower1._isMouseOver) { _range = GunTurret.RangeLevel1; }
                if (_buttonTower2._isMouseOver) { _range = Barrack.RangeLevel1; }
                //if (_buttonTower3._isMouseOver) { _rangeX = 256; _rangeY = 192; }
                //if (_buttonTower4._isMouseOver) { _rangeX = 224; _rangeY = 168; }
            }


            #region MessageProcess
            if (_uiSelect.HasMessage())
            {
                string receivedText = _uiSelect._message._data.ToString();

                System.Console.WriteLine("Message = " + receivedText);

                if (receivedText == "BUILD_0")
                {
                   new GunTurret(_player, _mouse,_layerUI, _defaultRallyPoint, GunTurret.RangeLevel1)
                        .SetPosition(XY)
                        .AppendTo(_parent)
                        .This<GunTurret>();

                    //SetSelected(tower); // Game Feel auto select tower

                    _autoDestroy = true;
                    _uiSelect.Hide();

                }
                else
                if (receivedText == "BUILD_1")
                {

                    new Barrack(_player, _mouse, _layerUI, _defaultRallyPoint, Barrack.RangeLevel1)
                        .SetPosition(XY)
                        .AppendTo(_parent)
                        .This<Barrack>();

                    //SetSelected(tower); // Game Feel auto select tower

                    _autoDestroy = true;
                    _uiSelect.Hide();

                }



                _uiSelect.EndMessage();
            }
            #endregion

            
            if (_autoDestroy && _uiSelect._animate.Off("hide"))
            {
                // Kill this UI & Selectable item
                _uiSelect.KillMe();
                KillMe();
            }

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            // Circle Tower Range
            if (_isSelected && _uiSelect.IsShow)
                Game1.DrawGradientCircle(batch, AbsXY, _range.X, _range.Y, Color.Yellow * .2f);

            Draw.FillSquare(batch, AbsXY,4, Color.Green * .5f);
            
            if (!_isSelected)
                Game1.DrawGradientCircle(batch, AbsXY, 32, 24, Color.DarkGoldenrod * .2f);

            float value = (_uiSelect._animate.Value() / 100f);

            // Menu UI Circle
            Game1.DrawGradientCircle(batch, AbsXY, _rect.Width * value, _rect.Width * value, Color.DarkGray * value *.5f);

            if (_isMouseOver && !_isSelected)
            {
                Game1.DrawGradientCircle(batch, AbsXY, _rect.Width/2 + _loops._current, _rect.Height/2 + _loops._current, Color.LightSkyBlue);
            }

            if (_isSelected)
                batch.Draw(Game1._tex_Radar00, AbsXY, new Rectangle(0, 0, 256, 256), Color.Black * value * .2f, _angle, new Vector2(128, 128), value, SpriteEffects.None, 0);

            //batch.DrawString(Game1._mainFont, _index.ToString(), AbsXY, Color.Yellow);

            //Draw.CenterBorderedStringXY(batch, Game1._font_Main, _uiSelect._buttonOverMessage, AbsX, AbsY, Color.Yellow, Color.DarkRed);

            return base.Render(batch);
        }
    }
}
