﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace TowerDefenseMono
{
    class Button : Node
    {
        #region Attributes

        protected Input.Mouse _mouse;

        public bool _isEnable { get; private set; } = false; // true to enable this button
        public bool _isLocked { get; private set; } = false; // true to unlock this button
        public bool _isMouseOver { get; private set; } = false;
        public bool _onMouseClick { get; private set; } = false;

        public float _angle; // use this if need !! to place button in circle by angle
        public float _radius; // use this for place button by radius

        public string _overMessage = "";

        Texture2D _tex_Icon;
        Player _player;

        public int _cost { get; private set; } = 0;

        #endregion

        public Button(Player player, Texture2D tex_Icon, Input.Mouse mouse, Message message, string overMessage, bool isLocked = false, int cost = 0,  float width = 64, float height = 64)
        {
            _type = UID.Get<Button>();
            _player = player;
            _tex_Icon = tex_Icon;
            _mouse = mouse;
            _message = message;
            _overMessage = overMessage;
            _isLocked = isLocked;
            _cost = cost;
            SetSize(width, height);
        }
        public override Node Update(GameTime gameTime)
        {
            UpdateRect();

            _isMouseOver = false;
            _onMouseClick = false;

            Vector2 mousePos = new Vector2(_mouse._x, _mouse._y);

            //if (Misc.InRect(_mouse._x, _mouse._y, AbsRect))
            if (Collision2D.PointInEllipse(mousePos, AbsXY, _rect.Width/2, _rect.Height/2))
            {
                _isMouseOver = true;
                _mouse._isOver = true;

                _parent.This<UICircleSelect>()._buttonOverMessage = _overMessage;

                if (_mouse._onClick && _isEnable && !_isLocked)
                {
                    _player.Buy(_cost);

                    _onMouseClick = true;
                    _parent._message = _message;

                    if (_parent._alpha == 1)
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                }
            }

            _isEnable = _player.CanBuy(_cost);

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            if (_tex_Icon != null)
                batch.Draw(_tex_Icon, AbsRect, _isLocked ?  Color.Black * _parent._alpha : Color.White * _parent._alpha);

            Game1.DrawGradientCircle(batch, AbsXY, _rect.Width/2, _rect.Height/2, Color.Black * _parent._alpha);

            if (_isMouseOver)
                Game1.DrawGradientCircle(batch, AbsXY, _rect.Width / 2, _rect.Height / 2, Color.LightGreen * _parent._alpha);

            if (!_isEnable)
                //Draw.Circle(batch, AbsXY, _rect.Width / 2, 16, Color.Red * .4f * _parent._alpha, _rect.Width / 2);
                batch.Draw(Game1._tex_fillCircle, AbsRect, Color.Black * .8f * _parent._alpha);

            if (_isLocked)
                batch.Draw(Game1._tex_Icon_Locked, AbsRect, Color.White * _parent._alpha);

            if (_cost > 0 && _parent._isActive && !_isLocked)
                Draw.CenterBorderedStringXY(batch, Game1._font_Main, _cost.ToString(), AbsX, AbsY + _rect.Height/2, Color.LightSteelBlue * _parent._alpha, Color.DarkBlue * _parent._alpha);

            return base.Render(batch);
        }
    }
}
