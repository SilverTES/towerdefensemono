﻿using Retro2D;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using MonoGame.Extended;
using System.Collections.Generic;
using Microsoft.Xna.Framework.Input;

namespace TowerDefenseMono
{
    public class Selectable : Node
    {
        #region Attributes

        protected static List<Selectable> _listSelected = new List<Selectable>();

        protected Input.Mouse _mouse;

        public bool _isSelected { get; protected set; } = false;
        protected bool _onSelected = false;
        protected bool _offSelected = false;

        protected bool _isMouseOver = false;
        protected bool _onMouseOver = false;
        protected bool _offMouseOver = false;

        bool _isMouseEnter = false;

        protected bool _onMouseClick = false;

        protected Player _player;

        public string BottomInfo = "";

        #endregion

        public Selectable(Player playerHUD, Input.Mouse mouse)
        {
            _type = UID.Get<Selectable>();

            _player = playerHUD;
            _mouse = mouse;
        }
        public void SetSelected(bool isSelected)
        {
            _isSelected = isSelected;
        }
        /// <summary>
        /// true if have Multiselection
        /// </summary>
        public static bool IsMultiSelected() { return _listSelected.Count > 1; }
        /// <summary>
        /// true if have selection
        /// </summary>
        public static bool IsSelected() { return _listSelected.Count > 0; }
        /// <summary>
        /// true if have only one selected
        /// </summary>
        public static bool IsOneSelected() { return _listSelected.Count == 1; }
        public static void SetAllSelected(bool isSelected)
        {
            // Reset All childs _isFocus to false !

            if (!Keyboard.GetState().IsKeyDown(Keys.LeftShift)) // Hold LShif For add selectable to focus ! 
                if (_listSelected.Count > 0)
                {
                    for (int i = 0; i < _listSelected.Count; i++)
                    {

                        if (_listSelected[i]._isSelected != isSelected)
                        {
                            if (!_listSelected[i]._isSelected)
                                _listSelected[i]._onSelected = true;
                            else
                                _listSelected[i]._offSelected = true;

                            _listSelected[i]._isSelected = isSelected;
                        }

                    }
                    _listSelected.Clear();
                }
        }
        public static void SetSelected(Selectable selectable, bool isSelected = true)
        {
            SetAllSelected(false);
            selectable._isSelected = isSelected;

            if (selectable._isSelected)
                selectable._onSelected = true;
            else
                selectable._offSelected = true;

            _listSelected.Add(selectable);
        }
        public override Node Update(GameTime gameTime)
        {
            _onMouseClick = false;
            _onSelected = false;
            _offSelected = false;
            _onMouseOver = false;
            _offMouseOver = false;

            //if (_mouse._onClick && !_mouse._isOver && _listSelected.Count > 0)
            //{
            //    SetAllSelected(false);
            //    System.Console.WriteLine("< Reset All >");
            //}

            if (_isMouseOver && !_mouse._isOver && !_isMouseEnter)
            {
                _isMouseEnter = true;
                _onMouseOver = true;
                //System.Console.WriteLine("on Mouse Over");
            }
            if (!_isMouseOver && _isMouseEnter)
            {
                _isMouseEnter = false;
                _offMouseOver = true;
                //System.Console.WriteLine("off Mouse Over");
            }

            if (_isMouseOver && !_mouse._isOver)
            {
                _mouse._isOver = true; // mouse is over something !

                if (_mouse._onClick && _isMouseOver)
                {
                    _onMouseClick = true;

                    //System.Console.Write("< Before Selectable _onClick : " + _isSelected + " >");

                    bool isSelected = _isSelected; // ----------- Save the last selected state
                    SetAllSelected(false);
                    _isSelected = !isSelected; // ------------ Restore last selected state

                    if (_isSelected) 
                        _onSelected = true; 
                    else 
                        _offSelected = true;

                    _listSelected.Add(this);

                    //System.Console.Write("< After Selectable _onClick : " + _isSelected + " >");
                }
            }

            return base.Update(gameTime);
        }
    }
}
