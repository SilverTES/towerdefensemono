﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;

namespace TowerDefenseMono
{
    class DialBox : Gui.Base
    {
        bool _isShow = false;

        Node _prevNaviGateNode = null;

        public DialBox(Input.Mouse mouse, int start = 0, int end = 480, int duration = 16) : base(mouse)
        {
            _naviGate = new NaviGate(this);

            Style._focusBorderColor._value = Color.Yellow;
            SetFocusable(false);

            _naviGate.SetNaviGate(false);
            //_naviGate.SetNaviNodeFocusAt(0);

            _animate.Add("show", Easing.CircularEaseOut, new Tweening(end, start, duration));
            _animate.Add("hide", Easing.CircularEaseIn, new Tweening(start, end, duration));
            //_animate.Start("up");

        }

        //public override Node Init()
        //{
        //    return base.Init();
        //}

        public void ToggleShow(Node prevNaviGateNode)
        {
            _prevNaviGateNode = prevNaviGateNode;

            _isShow = !_isShow;

            if (_isShow)
                Show();
            else
                Hide();
        }

        public bool IsShow()
        {
            return _isShow;
        }

        public void Show()
        {
            _animate.Start("show");
            Game1._sound_Open1.Play(.05f * Game1._volumeSound, 1f, 0);
        }

        public void Hide()
        {
            _animate.Start("hide");
            Game1._sound_Open1.Play(.05f * Game1._volumeSound, .6f, 0);

        }

        public override Node Update(GameTime gameTime)
        {
            if (_animate.On("show"))
            {
                PostMessage("ON_SHOW");

                if (null != _prevNaviGateNode)
                    _prevNaviGateNode._naviGate.SetNaviGate(false);
            }
            if (_animate.On("hide"))
            {
                PostMessage("ON_HIDE");
                _naviGate.SetNaviGate(false); // Loose NaviGate 
            }

            if (_animate.Off("show"))
            {
                PostMessage("OFF_SHOW");

                _naviGate.MoveNaviGateTo(this, _prevNaviGateNode);

                //if (null != _parent) // if parent exist then move NaviGate from parent to this
                //{
                //    if (null != _parent._naviGate)
                //    {
                //        System.Console.WriteLine("MOVE NAVIGATE !");

                //        _parent._naviGate.MoveNaviGateTo(this);
                //        //_naviGate.SetNaviGate(true);
                //    }
                //}
                //else
                //{
                //    _naviGate.SetNaviGate(true);
                //}

            }

            if (_animate.Off("hide"))
            {
                PostMessage("OFF_HIDE");

                _naviGate.BackNaviGate();
                //Console.WriteLine("BackNaviGate :"+_navigate._prevNaviGateNode);
            }

            if (_animate.IsPlay())
                _y = _animate.Value();

            _animate.NextFrame();


            //if (_naviGate.IsNaviGate())
            //{
            //    Game1.UpdateNaviGateButton(_naviGate);

            //    if (Input.Button.OnPress("back"))
            //    {
            //        //Console.WriteLine("dialbox back button pressed");
            //        //_navigate.BackNaviGate();
            //    }

            //    if (_naviGate.OnSubmit())
            //    {
            //        _naviGate.MoveNaviGateToFocusedChild();
            //        //Game1._sound_Clock.Play(.5f, .05f, 0);

            //        //Console.WriteLine("Coucou");
            //    }

            //    if (_naviGate.OnChangeFocus() && !Retro2D.Screen.IsTransition())
            //    {
            //        //Game1._sound_Clock.Play(.5f, .05f, 0);
            //    }
            //}


            //UpdateChilds();

            if (HasMessage())
            {
                Data data = (Data)_message._data;

                if (data._msg == "OK")
                {
                    ToggleShow(_prevNaviGateNode);
                }

                EndMessage();
            }

            return base.Update(gameTime);
        }

        //public override Node Render(SpriteBatch batch)
        //{
        //    base.Render(batch);

        //    return this;
        //}

    }
}
