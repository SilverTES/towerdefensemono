﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System;
using System.Linq;
using TiledSharp;
using TowerDefenseMono.Unit;

namespace TowerDefenseMono.UI
{
    public class MiniMap
    {
        public const int MiniMapSize = 200;

        float _rationDisplayMiniMap;
        Point _miniMapHUDPos;
        float _miniMapSizeW;
        float _miniMapSizeH;
        Rectangle _miniMapRect = new Rectangle();
        float _offSetDisplayMiniMapX = 0;
        float _offSetDisplayMiniMapY = 0;

        RectangleF _rectMiniView = new RectangleF();

        bool _isMouseOverMiniMap = false;
        bool _isMouseOverRectView = false;
        bool _isDragMiniMap = false;

        float _dragX;
        float _dragY;

        // TileMap
        TmxMap _tmxMap;
        float _mapSizeW;
        float _mapSizeH;

        Input.Mouse _mouse;
        Level _level;

        public MiniMap(Input.Mouse mouse, Level level)
        {
            _mouse = mouse;
            _level = level;

            _miniMapHUDPos = new Point(0, Game1._screenH - MiniMapSize - 2);

        }
        public void OnDragMiniMap()
        {
            // Drag become true

            _isDragMiniMap = true;
            _dragX = _mouse._x - _rectMiniView.X;
            _dragY = _mouse._y - _rectMiniView.Y;
        }
        public void SetMiniMap(TmxMap tmxMap)
        {
            _tmxMap = tmxMap;

            _mapSizeW = _tmxMap.Width * _tmxMap.TileWidth;
            _mapSizeH = _tmxMap.Height * _tmxMap.TileHeight;

            // LandScape
            if (_mapSizeW >= _mapSizeH)
            {
                _miniMapSizeW = MiniMapSize;
                _rationDisplayMiniMap = MiniMapSize / _mapSizeW;
                _miniMapSizeH = _mapSizeH * _rationDisplayMiniMap;

                _offSetDisplayMiniMapY = (float)Math.Round((MiniMapSize - _miniMapSizeH) / 2f);
            }
            // Portrait
            else
            {
                _miniMapSizeH = MiniMapSize;
                _rationDisplayMiniMap = MiniMapSize / _mapSizeH;
                _miniMapSizeW = _mapSizeW * _rationDisplayMiniMap;

                _offSetDisplayMiniMapX = (float)Math.Round((MiniMapSize - _miniMapSizeW) / 2f);
            }

            _rectMiniView.Width = (Game1._screenW * _rationDisplayMiniMap);
            _rectMiniView.Height = (Game1._screenH * _rationDisplayMiniMap);

        }
        public void UpdateCameraGame()
        {
            if (_rectMiniView.X <= _miniMapHUDPos.X + _offSetDisplayMiniMapX)
                _rectMiniView.X = _miniMapHUDPos.X + _offSetDisplayMiniMapX;

            if (_rectMiniView.Y <= _miniMapHUDPos.Y + _offSetDisplayMiniMapY)
                _rectMiniView.Y = _miniMapHUDPos.Y + _offSetDisplayMiniMapY;

            float limitX = _miniMapRect.Width - _rectMiniView.Width;
            float limitY = _miniMapRect.Height - _rectMiniView.Height;

            if (_rectMiniView.X >= _miniMapHUDPos.X + _offSetDisplayMiniMapX + limitX)
                _rectMiniView.X = _miniMapHUDPos.X + _offSetDisplayMiniMapX + limitX;

            if (_rectMiniView.Y >= _miniMapHUDPos.Y + _offSetDisplayMiniMapY + limitY)
                _rectMiniView.Y = _miniMapHUDPos.Y + _offSetDisplayMiniMapY + limitY;

            float cameraX = (_miniMapHUDPos.X - _rectMiniView.X + _offSetDisplayMiniMapX) / _rationDisplayMiniMap;
            float cameraY = (_miniMapHUDPos.Y - _rectMiniView.Y + _offSetDisplayMiniMapY) / _rationDisplayMiniMap;

            _level.SetCameraPos(cameraX, cameraY);
        }
        public void Update(GameTime gameTime)
        {
            //_buttonMenu.Update(gameTime);

            _miniMapRect.X = (int)(_miniMapHUDPos.X + _offSetDisplayMiniMapX);
            _miniMapRect.Y = (int)(_miniMapHUDPos.Y + _offSetDisplayMiniMapY);
            _miniMapRect.Width = (int)_miniMapSizeW;
            _miniMapRect.Height = (int)_miniMapSizeH;

            _isMouseOverRectView = false;
            _isMouseOverMiniMap = false;

            if (Misc.InRect(_mouse._x, _mouse._y, _miniMapRect) && !_mouse._isOver)
            {
                _mouse._isOver = true;
                _isMouseOverMiniMap = true;


            }


            // Set Rally point from miniMap if one hero is selected
            if (_isMouseOverMiniMap && _level._player._selectedHero != null)
            {
                if (_level._player._selectedHero._isWaitRallyPoint && _mouse._onClick)
                {
                    System.Console.WriteLine("RAlly point from minimap !");

                    Vector2 rallyPoint = new Vector2();

                    rallyPoint.X = (_mouse._x - _miniMapRect.X) / _rationDisplayMiniMap;
                    rallyPoint.Y = (_mouse._y - _miniMapRect.Y) / _rationDisplayMiniMap;

                    _level._player._selectedHero.This<HeroSoldier>().SetRallyPoint(rallyPoint);

                    Selectable.SetSelected(_level._player._selectedHero, false);

                    return;
                }
            }


            if (Misc.InRect(_mouse._x, _mouse._y, _rectMiniView))
            {
                _isMouseOverRectView = true;

                if (_mouse._onClick)
                {
                    OnDragMiniMap();
                }

            }

            if (!_mouse._isClick)
                _isDragMiniMap = false;

            if (_isDragMiniMap) // Drag the mini View
            {
                _rectMiniView.X = _mouse._x - _dragX;
                _rectMiniView.Y = _mouse._y - _dragY;

                UpdateCameraGame();
            }
            else
            {
                if (_isMouseOverMiniMap) // Click on the minimap outside mini View
                {
                    if (_mouse._onClick)
                    {
                        // replace rectView center at mouse position
                        _rectMiniView.X = _mouse._x - _rectMiniView.Width / 2;
                        _rectMiniView.Y = _mouse._y - _rectMiniView.Height / 2;

                        // now become draggable
                        OnDragMiniMap();

                        UpdateCameraGame();

                    }
                }
                else
                {
                    _rectMiniView.X = _miniMapHUDPos.X + _offSetDisplayMiniMapX - (_level.Camera.X * _rationDisplayMiniMap);
                    _rectMiniView.Y = _miniMapHUDPos.Y + _offSetDisplayMiniMapY - (_level.Camera.Y * _rationDisplayMiniMap);
                }
            }




        }

        public void Render(SpriteBatch batch)
        {
            Draw.FillRectangle(batch, new Rectangle(_miniMapHUDPos.X, _miniMapHUDPos.Y - 48, MiniMapSize, MiniMapSize + 48), Color.Black);

            //Draw.Rectangle(batch, new RectangleF(_miniMapHUDPos.X, _miniMapHUDPos.Y, MiniMapSize, MiniMapSize), Color.Black, 1f);


            Rectangle rect = new Rectangle(_miniMapHUDPos.X + (int)_offSetDisplayMiniMapX, _miniMapHUDPos.Y + (int)_offSetDisplayMiniMapY,
                (int)_miniMapSizeW, (int)_miniMapSizeH);

            batch.Draw(_level._tex_miniMap, rect, Color.White);

            Draw.Rectangle(batch,
                new RectangleF(_miniMapHUDPos.X + _offSetDisplayMiniMapX, _miniMapHUDPos.Y + _offSetDisplayMiniMapY,
                _miniMapSizeW, _miniMapSizeH), Color.DarkGray);



            // Display all enemies in mini Map
            for (int i = 0; i < _level._layerGame._childs.Count(); i++)
            {
                Node node = _level._layerGame._childs.At(i);

                if (node != null)
                {
                    float x = node._x * _rationDisplayMiniMap + _offSetDisplayMiniMapX;
                    float y = node._y * _rationDisplayMiniMap + _offSetDisplayMiniMapY;

                    if (Misc.InRect(x, y, new RectangleF(new Vector2(_offSetDisplayMiniMapX, _offSetDisplayMiniMapY), new Size2(_miniMapSizeW, _miniMapSizeH))))
                    {
                        if (node._subType == UID.Get<Unit.Enemy0>())
                        {
                            Draw.FillSquare(batch, new Vector2(x, y) + _miniMapHUDPos.ToVector2(), 2, Color.Red);
                        }

                        if (node._subType == UID.Get<Unit.Marine>())
                        {
                            Draw.FillSquare(batch, new Vector2(x, y) + _miniMapHUDPos.ToVector2(), 2, Color.LightSkyBlue);
                        }

                        if (node._subType == UID.Get<Unit.HeroSoldier>())
                        {
                            Draw.FillSquare(batch, new Vector2(x, y) + _miniMapHUDPos.ToVector2(), 2, Color.YellowGreen);
                        }

                        if (node._type == UID.Get<BaseBuilding>())
                        {
                            Draw.FillSquare(batch, new Vector2(x, y) + _miniMapHUDPos.ToVector2(), 3, Color.GreenYellow);
                        }
                    }
                }
            }


            // Draw Rect Mini View
            Draw.Rectangle(batch, _rectMiniView, _isMouseOverRectView ? Color.Yellow : Color.Yellow * .4f);

            //_buttonMenu.Render(batch);
        }


    }
}
