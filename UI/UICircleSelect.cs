﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using MonoGame.Extended;
using Retro2D;
using System.Collections.Generic;

namespace TowerDefenseMono
{
    class UICircleSelect : Node
    {

        #region Attributes
        
        Input.Mouse _mouse;

        //public bool IsActive { get; set; } = true;
        public bool IsShow { get; private set; } = false;
        public bool IsHide { get; private set; } = true;

        bool _toggle = false;
        public string _buttonOverMessage = "";
        List<Button> _listButton = new List<Button>();

        float _radius;

        #endregion

        public UICircleSelect(Input.Mouse mouse, float radius)
        {

            _type = UID.Get<UICircleSelect>();

            _mouse = mouse;

            SetSize(128, 96);
            SetPivot(64, 48);

            _radius = radius;

            _animate.Add("show", Easing.CircularEaseOut, new Tweening(0, 100, 12));
            _animate.Add("hide", Easing.CircularEaseIn, new Tweening(100, 0, 8));

            _alpha = 0f;

            SetActive(false);
        }

        public Button AddButton(Button button, float angle, float radius = -1)
        {
            button.SetPivot(Position.CENTER);
            button._angle = angle;
            if (radius == -1)
                button._radius = _radius;
            else
                button._radius = radius;

            button.AppendTo(this);

            return button;
        }
        public void ToggleShow()
        {
            _toggle = !_toggle;

            if (_toggle)
                Show();
            else
                Hide();
        }

        public void Show()
        {
            _isActive = true;
            _animate.Start("show");
            //Game1._sound_key.Play(.5f, .5f, 0);
        }

        public void Hide()
        {
            _animate.Start("hide");
            //Game1._sound_key.Play(.5f, .2f, 0);
        }

        public bool HasButton() { return _listButton.Count > 0;}
        public Button GetButton(int index)
        {
            if (index >= 0 && index < _listButton.Count)
                return _listButton[index];

            return null;
        }

        public override Node Update(GameTime gameTime)
        {
            _buttonOverMessage = ""; // Reset button over message ! 

            // animate buttons mouvement
            for (int i=0; i<NbActive(); i++)
            {
                //if (_childs.At(i)._type == UID.Get<Button>())
                Button button = _childs.At(i).This<Button>();
                button.SetPosition(Geo.GetVector(button._angle) * button._radius * _alpha);
                button._x += _oX;
                button._y += _oY;
                
            }

            if (_animate.On("show"))
            {
                //System.Console.WriteLine("ON_SHOW");
                IsHide = false;
            }
            if (_animate.On("hide"))
            {
                //System.Console.WriteLine("ON_HIDE");
                IsShow = false;
                //IsActive = false;
            }

            if (_animate.Off("show"))
            {
                //System.Console.WriteLine("OFF_SHOW");
                IsShow = true;
            }

            if (_animate.Off("hide"))
            {
                //System.Console.WriteLine("OFF_HIDE");
                IsHide = true;
                _isActive = false;
            }

            _animate.NextFrame();

            UpdateRect();

            UpdateChilds(gameTime);

            _alpha = (_animate.Value() / 100f);

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            //Game1.DrawGradientCircle(batch, AbsXY , 64, 48, Color.Black * .4f);
            //batch.DrawString(Game1._mainFont, ":" + _animate.Value(), AbsXY, Color.Yellow);
            if (IsShow)
                Draw.CenterBorderedStringXY(batch, Game1._font_Main, _buttonOverMessage, AbsX, AbsY, Color.Yellow, Color.DarkRed);

            RenderChilds(batch);


            return base.Render(batch);
        }
    }
}
