﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using MonoGame.Extended;
using Retro2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerDefenseMono
{
    public class ProgressBar
    {
        // display 
        Rectangle _rect;
        Color _colorBorder;
        Color _colorFG;
        Color _colorBG;
        float _borderWidth = 1f;
        Position _position;

        // value
        float _maxValue;
        float _currentValue;

        // display value
        float _nbDiv = 1;
        int _widthValue;
        bool _isVisible = true;

        public ProgressBar(float currentValue, float maxValue, int width, int height, Color colorFG, Color colorBG, Color colorBorder, float borderWidth = 1f, float nbDiv = 1f, Position position = Position.CENTER)
        {
            _currentValue = currentValue;
            _maxValue = maxValue;

            _rect = new Rectangle(0, 0, width, height);
            _position = position;
            _colorFG = colorFG;
            _colorBG = colorBG;
            _colorBorder = colorBorder;
            _borderWidth = borderWidth;

            _nbDiv = nbDiv;
        }

        public void SetColor(Color colorFG = default, Color colorBG = default, Color colorBorder = default)
        {
            if (colorFG != default) _colorFG = colorFG;
            if (colorBG != default) _colorBG = colorBG;
            if (colorBorder != default) _colorBorder = colorBorder;

        }
        public void SetMaxValue(float maxValue)
        {
            _maxValue = maxValue;
        }
        public void SetVisible(bool isVisible)
        {
            _isVisible = isVisible;
        }
        public void SetValue(float currentValue)
        {
            _currentValue = currentValue;
        }
        public void Render(SpriteBatch batch, float x, float y)
        {
            if (_isVisible)
            {
                if (_position == Position.CENTER)
                {
                    _rect.X = (int)(x - _rect.Width / 2);
                    _rect.Y = (int)(y - _rect.Height / 2);
                }

                _widthValue = _rect.Width - (int)(_rect.Width * _currentValue / _maxValue);

                Draw.FillRectangle(batch, _rect, _colorBG);
                Draw.FillRectangle(batch, Gfx.AddRect(_rect, 0, 0, -_widthValue, 0), _colorFG);

                Draw.Line(batch, _rect.Left, _rect.Top + 1, _rect.Right, _rect.Top + 1, Color.White * .6f, 2);
                Draw.Line(batch, _rect.Left, _rect.Bottom - 1, _rect.Right, _rect.Bottom - 1, Color.Black * .6f, 2);

                float div = _rect.Width / _nbDiv;

                for (float i=0; i<_nbDiv; i++)
                {
                    float divx = i * div + _rect.X;
                    Draw.Line(batch, divx, _rect.Y, divx, _rect.Y + _rect.Height, _colorBorder, 2f);
                }

                Draw.Rectangle(batch, _rect, _colorBorder, _borderWidth);
            }
        }

    }
}
