﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using System.Collections.Generic;
using TiledSharp;
using TowerDefenseMono.UI;
using TowerDefenseMono.Unit;

namespace TowerDefenseMono
{
    public class PlayerHUD
    {
        #region Attributes

        Player _player;

        public Message _message;

        public const int BottomBarSize = 16;
        public const int TopBarSize = 16;

        public string BottomInfo = "";

        Level _level;
        Input.Mouse _mouse;

        // MiniMap
        MiniMap _miniMap;
        
        // Slot Heroes Button
        SlotHeroesButton _slotHeroesButton;

        #endregion

        public PlayerHUD(Level level, Input.Mouse mouse, TmxMap tmxMap, Player player)
        {
            _level = level;
            _mouse = mouse;
            _player = player;

            _miniMap = new MiniMap(mouse, level);
            _miniMap.SetMiniMap(tmxMap);

            new Gui.Button(_level._mouseLevel)
                .AppendTo(_level._layerHUD)
                .SetSize(8 * 22, 8 * 3)
                .SetPivot(Position.CENTER)
                .SetPosition(MiniMap.MiniMapSize / 2, Game1._screenH - MiniMap.MiniMapSize - 24)
                //.SetPosition(Game1._screenW/2, Game1._screenH- 3*8)
                .This<Gui.Button>().SetLabel("PAUSE")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                        Node._root["ScreenPlay"].This<ScreenPlay>().ToggleShowPauseMenu();

                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            _slotHeroesButton = new SlotHeroesButton(player, level, mouse);


        }
        public void Init()
        {
            _slotHeroesButton.Init();
        }
        public void Update(GameTime gameTime)
        {
            _miniMap.Update(gameTime);

            _slotHeroesButton.Update(gameTime);
        }
        public void Render(SpriteBatch batch, Vector2 pos)
        {

            // Top Bar
            //Draw.FillRectangle(batch, new Rectangle(0, 0, Game1._screenW, TopBarSize), Color.Black * .6f);

            // Tool Bar
            Draw.FillRectangle(batch, new Rectangle(0, Game1._screenH - BottomBarSize, Game1._screenW, BottomBarSize), Color.Black * 1f);

            // MiniMap
            _miniMap.Render(batch);

            //Slot Heroes Button
            _slotHeroesButton.Render(batch);

            Draw.FillRectangle(batch, 0, 0, 280, 28, Color.Black * .8f);
            Draw.Rectangle(batch, 0, 0, 280, 28, Color.Black * .4f, 4f);

            Draw.LeftTopBorderedString(batch, Game1._font_Medium, "LIFE " + _player.Life, pos.X, pos.Y, Color.Orange, Color.Black);
            Draw.LeftTopBorderedString(batch, Game1._font_Medium, "$" + _player.Bank, pos.X + 160, pos.Y, Color.Yellow, Color.Black);

            if (_level._wave == Level.MAX_WAVE)
                Draw.TopCenterBorderedString(batch, Game1._font_Medium, "- FINAL WAVE -", Game1._screenW / 2, pos.Y, Color.Cyan, Color.Black);
            else
                Draw.TopCenterBorderedString(batch, Game1._font_Medium, "WAVE  " + _level._wave + " / " + Level.MAX_WAVE , Game1._screenW / 2, pos.Y, Color.Cyan, Color.Black);


            //Draw.CenterBorderedStringXY(batch, Game1._mainFont, "Camera Pos :" + _screenPlay.Camera.X + " , " + _screenPlay.Camera.Y, 
            //    Game1._screenW / 2, 600, Color.Cyan, Color.Black);

            Draw.TopCenterBorderedString(batch, Game1._font_Main, BottomInfo,
               Game1._screenW / 2, Game1._screenH - 16, Color.Gold, Color.DarkRed);
        }
    }
}
