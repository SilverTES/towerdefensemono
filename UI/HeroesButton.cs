﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using System;

namespace TowerDefenseMono
{
    public class HeroesButton
    {
        #region Attributes

        public const int AvatarSize = 80;

        public Keys _shortKey;

        protected Input.Mouse _mouse;

        public int _id { get; private set; }
        public string _name { get; private set; }

        public bool _isEnable { get; private set; } = false; // true to enable this button
        public bool _isLocked { get; private set; } = false; // true to unlock this button
        public bool _isMouseOver { get; private set; } = false;
        //public bool _isHighLight { get; private set; } = false;
        public bool _onMouseClick { get; private set; } = false;

        bool _isSelected = false;
        public bool IsSelected => _isSelected;

        Texture2D _tex_Icon;
        Player _player;
        Level _level;

        RectangleF _rectF;
        Rectangle _rect;

        float _ticEnable = 1f;

        Animate _animate = new Animate();

        bool _isHeroAlive = false;

        BaseUnit _hero;
        public BaseUnit Hero => _hero;

        Addon.Loop _loop = new Addon.Loop();

        bool _onPressButton = false;

        ProgressBar _enableBar;

        #endregion

        public HeroesButton(int id, Keys shortKey, string name, Player player, Level level, Texture2D tex_Icon, Input.Mouse mouse, bool isLocked = false)
        {
            _id = id;
            _shortKey = shortKey;
            _name = name;
            //_playerHUD = playerHUD;
            _player = player;
            _level = level;

            _tex_Icon = tex_Icon;
            _mouse = mouse;
            //_message = message;
            _isLocked = isLocked;

            _rectF = new RectangleF(0, 0, AvatarSize, AvatarSize);
            _rect = new Rectangle(0, 0, AvatarSize, AvatarSize);

            _animate.Add("show", Easing.CircularEaseOut, new Tweening(0, 100, 12));
            _animate.Add("hide", Easing.CircularEaseIn, new Tweening(100, 0, 8));

            _enableBar = new ProgressBar(0f, 1f, AvatarSize-2, 6, Color.Yellow, Color.Black, Color.Black, 2, 6);

            _loop.SetLoop(1f, 1f, 1.1f, 0.01f, Loops.PINGPONG);
            _loop.Start();
        }
        public void UseSelectedHero()
        {
            _isHeroAlive = true;
            _isEnable = false;
            _ticEnable = 0;

            //new Particles.RallyPointCursor(new Vector2(_mouse.AbsX, _mouse.AbsY)).AppendTo(_screenPlay._layerUI);
            Game1.PlaySound(4, Game1._sound_ReadyToFight, .2f, .0001f);

            //Debug : insert Hero Soldier
            _hero = new Unit.HeroSoldier(_player, _mouse, Unit.HeroSoldier.RangeLevel3, _level._layerGameUI)
                .SetSpeed(2.8f)
                .SetPosition(new Vector2(_mouse.AbsX, _mouse.AbsY))
                .AppendTo(_level._layerGame)
                .This<Unit.HeroSoldier>();

        }
        public void SetSelected()
        {
            _isSelected = true;
        }
        public void ReloadHero()
        {
            _hero = null;

            _ticEnable = 0f; // Reset to 0f 

            _isHeroAlive = false;

            _isEnable = false;
        }
        public void Update(GameTime gameTime, Vector2 pos)
        {
            _enableBar.SetValue(_ticEnable);

            _loop.Update();

            _rectF.X = pos.X;
            _rectF.Y = pos.Y;

            _rect.X = (int)_rectF.X;
            _rect.Y = (int)_rectF.Y;


            _isMouseOver = Misc.InRect(_mouse._x, _mouse._y, _rectF) && !_mouse._isOver;
            if (_isMouseOver) _mouse._isOver = true;

            // If user press or select this button
            _onPressButton = Input.Button.OnePress("InvocHero" + _name, Keyboard.GetState().IsKeyDown(_shortKey)) || (_mouse._onClick && _isMouseOver);


            // USE HERO
            #region Hero Management
            if (_hero == null && !_isLocked)
            {
                if (_onPressButton && _isEnable)
                {
                    if (_isEnable)
                    {
                        _isSelected = !_isSelected;
                        Game1.PlaySound(1, Game1._sound_Clock, .1f, .01f);
                    }
                }

                if (_mouse._onClick)
                {
                    if (!_isMouseOver)
                    {
                        if (_isSelected)
                        {
                            Console.WriteLine("Hero  Created : " + _name);
                            UseSelectedHero();

                            _mouse._isOver = true; // Avoid hero Reselection when pose hero for first time
                        }
                        else
                        {
                            _isSelected = false;
                        }

                    }
                }
            }

            if (_hero != null)
            {
                if (_onPressButton) // If player click on button or press shortcut for select Hero !
                {
                    _isSelected = !_isSelected;
                    Selectable.SetSelected(_hero, !_hero._isSelected);
                    
                    _hero.SetWaitRallyPoint(_isSelected);
                }
                else
                if (_hero._isSelected) // If player select Hero by Mouse
                {
                    _isSelected = true;
                }

                if (_hero.IsDead) // If Hero dead
                {
                    _hero = null;
                    ReloadHero();
                }
            }
            #endregion

            // Reset selected if click somewhere out of button !
            if (!_isMouseOver && _mouse._onClick)
                _isSelected = false;


            if (!_isEnable && !_isHeroAlive)
                _ticEnable += 0.0005f;
            //_ticEnable += 1f;


            if (_ticEnable >= 1f)
            {
                //_ticUnlock = 0f;
                _isEnable = true;
            }


            if (_isSelected)
                _player._selectedHero = _hero;

        }
        public void Render(SpriteBatch batch)
        {
            Draw.FillRectangle(batch, _rectF, Color.Black *.6f);

            Draw.Grid(batch, _rect.X, _rect.Y, AvatarSize, AvatarSize, 8, 8, Color.Green * .2f);

            //if (_isMouseOver)
            //    Draw.Rectangle(batch, _rect, Color.Cyan, 2f);

            if (!_isLocked)
            {
                // Draw Heroes Avatar
                if (_tex_Icon != null)
                    batch.Draw(_tex_Icon, _rect, _isEnable || _isHeroAlive ? Color.White : Color.LightBlue * .4f);

                if (_isSelected)
                    Draw.Rectangle(batch, _rectF, _isSelected ? Color.Gold : Color.Black, 4f);

                //if (_isHighLight)
                //    Draw.Rectangle(batch, _rectF, Color.Gold * .8f);

                if (!_isHeroAlive)
                {
                    //Draw.Arc(batch, _rectF.Position + new Vector2(16,16), 12, 32, Geo.RAD_0, Geo.RAD_360, Color.DarkRed, 4);
                    //Draw.Arc(batch, _rectF.Position + new Vector2(16,16), 12, 32, Geo.RAD_0, Geo.RAD_360 * _ticEnable, _ticEnable < .5f ? Color.Orange :  Color.Yellow, 4);

                    if (!_isEnable)
                        _enableBar.Render(batch, _rectF.Position.X + AvatarSize / 2, _rectF.Position.Y - 6);

                }

            }
            else
            {
                if (_tex_Icon != null)
                    batch.Draw(_tex_Icon, _rect, Color.Black);

                batch.Draw(Game1._tex_Icon_Locked, _rect, Color.LightBlue * .4f);
                //batch.Draw(Game1._tex_Icon_Locked, _rect, _isSelected || _isMouseOver ? Color.White : Color.LightBlue * .4f);
            }
            

            Draw.CenterBorderedStringXY(batch, Game1._font_Main, _shortKey.ToString(), _rect.X + AvatarSize - 8, _rect.Y + 8, Color.Orange, Color.Black);
            Draw.CenterBorderedStringXY(batch, Game1._font_Main, _name, _rect.X + AvatarSize/2, _rect.Y + AvatarSize, Color.Orange, Color.Black);

            if (_hero == null && _isSelected)
            {
                //Draw.TopCenterBorderedString(batch, Game1._mainFont, "HERO : " + _listHeroes[_idSelectedHero]._name,
                //   Game1._screenW/2, Game1._screenH-16, Color.Gold, Color.DarkRed);
                //_avatarPos.X + (_listHeroes.Count * (HeroesButton.AvatarSize + SpacingAvatar))/2, _avatarPos.Y-16, Color.Gold, Color.DarkRed);

                //Draw.Sight(batch, _mouse._x, _mouse._y, Game1._screenW, Game1._screenH, Color.Gold * .2f);

                //batch.Draw(Game1._tex_PlaceCursor, new Vector2(_mouse._x - 32, _mouse._y - 32), Color.Gold);

                Game1.DrawGradientCircle(batch, new Vector2(Input._mouse._x, Input._mouse._y),
                    64 * _loop._current, 48 * _loop._current, Color.Orange * .4f);

                batch.Draw(
                    Game1._tex_PlaceCursor,
                    new Vector2(Input._mouse._x, Input._mouse._y),
                    null,
                    Color.Gold,
                    0,
                    new Vector2(Game1._tex_PlaceCursor.Width / 2, Game1._tex_PlaceCursor.Height / 2),
                    new Vector2(1f * _loop._current, .75f * _loop._current),
                    SpriteEffects.None, 0);

                //BottomInfo = "HERO : " + _listHeroes[_idSelectedHero]._name;

            }

            if (_hero != null)
                if (_hero._isSelected)
                {
                    Game1.DrawGradientCircle(batch, new Vector2(_mouse._x, _mouse._y), 32, 24, Color.Yellow);
                    Draw.FillSquare(batch, new Vector2(_mouse._x, _mouse._y), 4, Color.Yellow);
                }


            

        }
    }
}
