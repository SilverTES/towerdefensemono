﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using System.Collections.Generic;

namespace TowerDefenseMono
{
    public class SlotHeroesButton
    {
        #region Attributes

        List<HeroesButton> _listHeroes = new List<HeroesButton>();
        Vector2 _avatarPos;
        const float SpacingAvatar = 8;

        #endregion

        public SlotHeroesButton(Player player, Level level, Input.Mouse mouse)
        {

            _listHeroes.Add(new HeroesButton(0, Keys.X, "SOLDIER", player, level, Game1._tex_Icon_Soldier, mouse));
            _listHeroes.Add(new HeroesButton(1, Keys.C, "SLICER", player, level, Game1._tex_Icon_Slicer, mouse, true));
            _listHeroes.Add(new HeroesButton(2, Keys.V, "HEALER", player, level, Game1._tex_Icon_Healer, mouse, true));
            _listHeroes.Add(new HeroesButton(3, Keys.B, "SNIPER", player, level, Game1._tex_Icon_Sniper, mouse, true));

            _avatarPos.X = Game1._screenW - (_listHeroes.Count * (HeroesButton.AvatarSize + SpacingAvatar));
            _avatarPos.Y = Game1._screenH - HeroesButton.AvatarSize - PlayerHUD.BottomBarSize - 8;
        }
        public void Init()
        {
            for (int i = 0; i < _listHeroes.Count; i++)
            {
                _listHeroes[i].ReloadHero();
            }
        }
        public void Update(GameTime gameTime)
        {
            for (int i = _listHeroes.Count - 1; i >= 0; i--)
            {
                float x = _avatarPos.X + i * (HeroesButton.AvatarSize + SpacingAvatar);

                _listHeroes[i].Update(gameTime, new Vector2(x, _avatarPos.Y));
            }
        }
        public void Render(SpriteBatch batch)
        {
            #region Avatars

            for (int i = 0; i < _listHeroes.Count; i++)
                _listHeroes[i].Render(batch);

            #endregion
        }
    }
}
