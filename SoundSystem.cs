﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerDefenseMono
{
    public static class SoundSystem
    {
        #region Attributes

        // Sound anti saturation !
        static bool _playSoundFire = false;
        static int _ticPlaySoundFire = 0;
        static int _tempoPlaySoundFire = 30;

        #endregion

        public static void PrepareSound()
        {
            _playSoundFire = false;
        }
        public static void PlaySound()
        {
            _playSoundFire = true;
        }

        public static void PlayAllSound()
        {
            _ticPlaySoundFire++;
            if (_playSoundFire && _ticPlaySoundFire > _tempoPlaySoundFire)
            {
                _ticPlaySoundFire = 0;
                Game1._sound_Fire.Play(.04f * Game1._volumeSound, .01f, 0f);
            }
        }

    }
}
