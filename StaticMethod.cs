﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using System.Xml;

namespace TowerDefenseMono
{
    public class Camera2d
    {
        protected float _zoom; // Camera Zoom
        public Matrix _transform; // Matrix Transform
        public Vector2 _pos; // Camera Position
        protected float _rotation; // Camera Rotation

        public Camera2d()
        {
            _zoom = 1.0f;
            _rotation = 0.0f;
            _pos = Vector2.Zero;
        }

        // Sets and gets zoom
        public float Zoom
        {
            get { return _zoom; }
            set { _zoom = value; if (_zoom < 0.1f) _zoom = 0.1f; } // Negative zoom will flip image
        }

        public float Rotation
        {
            get { return _rotation; }
            set { _rotation = value; }
        }

        // Auxiliary function to move the camera
        public void Move(Vector2 amount)
        {
            _pos += amount;
        }
        // Get set position
        public Vector2 Pos
        {
            get { return _pos; }
            set { _pos = value; }
        }
        public Matrix get_transformation(float ViewportWidth, float ViewportHeight)
        {
            _transform =       // Thanks to o KB o for this solution
              Matrix.CreateTranslation(new Vector3(-_pos.X, -_pos.Y, 0)) *
                                         Matrix.CreateRotationZ(Rotation) *
                                         Matrix.CreateScale(new Vector3(Zoom, Zoom, 1)) *
                                         Matrix.CreateTranslation(new Vector3(ViewportWidth * 0.5f, ViewportHeight * 0.5f, 0));
            return _transform;
        }

    }




    public class Data
    {
        public enum Type
        {
            NONE,
            ON_PRESS_BUTTON,
            SELECT_LEVEL,
            ADD_SCORE,
            GET_DAMAGE,
            ADD_ENERGY,
            ADD_POWER,
            ADD_MAX_ACTIVE_BOMB
        }

        public Type _type = Type.NONE;
        public string _msg;
        public int _value;

        public Data SetMsg(string msg)
        {
            _msg = msg;

            return this;
        }
    }
    public partial class Game1 : Game
    {

        // Proto Import Node
        public static void ImportNodeFromFile(Node node, string fileName) 
        {
            Console.WriteLine("Import GUI in this NodeGui : " + node._name);

            XmlTextReader reader = null;  

            try
            {
                reader = new XmlTextReader(fileName);

                Style styleRoot = new Style();

                //int nbKill = node.KillAll(UID.Get<Gui.Base>());
                //Console.WriteLine("Nb Kill Gui.Base type = " + nbKill);

                while (reader.Read())
                {
                    switch (reader.NodeType)
                    {
                        case XmlNodeType.Element: // The node is an element.
                            Console.Write("<" + reader.Name);

                            if (reader.Name.ToLower() == "gui")
                            {
                                Console.WriteLine("Create Gui !!!");
                            }

                            while (reader.MoveToNextAttribute()) // Read the attributes.
                            {
                                //Console.Write(" " + reader.Name + "='" + reader.Value + "'");

                                if (reader.Name.ToLower() == "style")
                                {
                                    Console.WriteLine(" styleRoot = "+ reader.Value);

                                    //ImportStyleFromFile(lastFolderName + "/" + reader.Value, ref style);
                                    //style = StyleContainer.ByName(reader.Value);
                                    if (StyleContainer.Import(reader.Value, ref styleRoot))
                                        Console.WriteLine("Import Style Success ::: " + styleRoot._fontName);
                                }


                            }

                            Console.WriteLine(">");

                            break;
                    
                        case XmlNodeType.Text: //Display the text in each element.

                            //style._font = _font_Big;

                            if (node._node.ContainsKey("RootXmlText"))
                                node["RootXmlText"]
                                    .SetName(reader.Value)
                                    .SetSize(80,32)
                                    .SetX(Position.CENTER)
                                    .SetY(Position.CENTER)
                                    .This<Gui.Text>().SetStyle(styleRoot)
                                    .This<Gui.Text>().SetLabel(reader.Value)
                                    .SetPivot(Position.LEFT);
                            else
                                node["RootXmlText"] = new Gui.Text(Input._mouse)
                                    .AppendTo(node)
                                    .SetName(reader.Value)
                                    .SetSize(80, 32)
                                    .SetX(Position.CENTER)
                                    .SetY(Position.CENTER)
                                    .This<Gui.Text>().SetStyle(styleRoot)
                                    .This<Gui.Text>().SetLabel(reader.Value)
                                    .SetPivot(Position.LEFT);

                            //Console.WriteLine(reader.Value);

                            break;
                    
                        case XmlNodeType.EndElement: //Display the end of the element.
                            Console.Write("</" + reader.Name);
                            Console.WriteLine(">");
                            break;
                    }
                }
            }
            catch (Exception e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"ERROR IMPORT NodeFromFile : {fileName} | {e.Message}");
                Console.ResetColor();
            }
            finally
            {
                if (reader != null)
                    reader.Close();

                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine($"CLOSE IMPORT NodeFromFile : {fileName}");
                Console.ResetColor();
            }

        }


        public static void Quit() { _isQuit = true; }

        #region Mouse Methods
        void ResizeMouseCursor()
        {
            if (_window.ViewW > 1920)
                Mouse.SetCursor(MouseCursor.FromTexture2D(_tex_mouseCursor, 0, 0));
            else
                Mouse.SetCursor(MouseCursor.FromTexture2D(_tex_mouseCursorMini, 0, 0));

            //IsMouseVisible = true;
        }
        #endregion

        #region Draw & Effect Methods
        public static void Shake(float intensity, float step)
        {
            _shake.SetIntensity(intensity, step);
            _sound_TowerKill.Play(.2f * Game1._volumeSound, .001f, 0);
        }

        public static void DrawGradientCircle(SpriteBatch batch, Vector2 position, float rx, float ry, Color color)
        {
            batch.Draw(_tex_gradientCircle, new Rectangle((int)(position.X - rx), (int)(position.Y - ry), (int)rx * 2, (int)ry * 2), color);
        }
        #endregion

        #region Sound Methods
        public static void UpdatePlaySound()
        {
            for (int c = 0; c < _soundInstances.GetLength(0); c++)
            {
                for (int i = 0; i < _soundInstances.GetLength(1); i++)
                {
                    if (null != _soundInstances[c, i])
                        if (_soundInstances[c, i].State == SoundState.Stopped)
                        {
                            //Console.WriteLine("Delete Sound Instance");
                            _soundInstances[c, i] = null;
                        }
                }
            }
        }
        public static void PlaySound(int channel, SoundEffect sound, float volume = 1f, float pitch = 1f, float pan = 0f, bool isLooped = false)
        {
            int index = 0;

            bool find = false; // find free instance in array

            // find a null instance in array and set it
            for (int i = 0; i < _soundInstances.GetLength(1); i++)
            {
                if (_soundInstances[channel, i] == null)
                {
                    index = i;
                    find = true;
                    break;
                }
            }

            if (find)
            {
                SoundEffectInstance soundInstance = sound.CreateInstance();

                soundInstance.Volume = volume * _volumeSound;
                soundInstance.Pitch = pitch;
                soundInstance.Pan = pan;
                soundInstance.IsLooped = isLooped;

                soundInstance.Play();

                _soundInstances[channel, index] = soundInstance;
            }

        }
        #endregion

        //public static void UpdateNaviGateButton(NaviGate naviGate, bool crossNavi = false) // Utilisé pour la navigation dans un Node avec un NaviGate !
        //{
        //    if (crossNavi)
        //    {
        //        naviGate.Update(UID.Get<Gui.Base>());

        //        naviGate.UpdateSubmitButton(Keyboard.GetState().IsKeyDown(Keys.Enter));

        //        if (Input.Button.OnPress("up", Keyboard.GetState().IsKeyDown(Keys.Up))) naviGate.ToDirection(Position.UP);
        //        if (Input.Button.OnPress("down", Keyboard.GetState().IsKeyDown(Keys.Down))) naviGate.ToDirection(Position.DOWN);
        //        if (Input.Button.OnPress("left", Keyboard.GetState().IsKeyDown(Keys.Left))) naviGate.ToDirection(Position.LEFT);
        //        if (Input.Button.OnPress("right", Keyboard.GetState().IsKeyDown(Keys.Right))) naviGate.ToDirection(Position.RIGHT);

        //        // back button
        //        Input.Button.OnPress("back", Keyboard.GetState().IsKeyDown(Keys.Back));

        //    }
        //    else
        //    {
        //        naviGate.Update(UID.Get<Gui.Base>());

        //        naviGate.UpdateSubmitButton(Keyboard.GetState().IsKeyDown(Keys.Enter));

        //        if (Input.Button.OnPress("up", Keyboard.GetState().IsKeyDown(Keys.Up))) naviGate.ToPrevNaviNode(UID.Get<Gui.Base>());
        //        if (Input.Button.OnPress("down", Keyboard.GetState().IsKeyDown(Keys.Down))) naviGate.ToNextNaviNode(UID.Get<Gui.Base>());

        //        // back button
        //        Input.Button.OnPress("back", Keyboard.GetState().IsKeyDown(Keys.Back));
        //    }


        //}

    }
}
