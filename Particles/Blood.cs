﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace TowerDefenseMono
{
    public class Blood : Node
    {
        Texture2D _image;
        //Point _position;
        float _opacity;

        public Blood(Texture2D image, float x, float y)
        {
            _image = image;
            SetPosition(x, y);
            _opacity = 1f;
        }

        public override Node Update(GameTime gameTime)
        {
            if (_opacity <= 0f)
                KillMe();

            _opacity -= 0.001f;

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            int px = AbsX - (int)(_image.Width * .5f);
            int py = AbsY - (int)(_image.Height * .5f);

            batch.Draw(_image, new Rectangle(px, py + 8, _image.Width, _image.Height - 16), Color.DarkGray * _opacity);
            return base.Render(batch);
        }
    }
}
