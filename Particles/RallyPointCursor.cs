﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace TowerDefenseMono.Particles
{
    public class RallyPointCursor : Node
    {

        public RallyPointCursor(Vector2 mousePos)
        {
            SetPosition(mousePos);
            _animate.Add("show", Easing.ElasticEaseOut, new Tweening(0, 1, 40));
            _animate.Start("show");
        }

        public override Node Update(GameTime gameTime)
        {
            if (_animate.Off("show"))
                KillMe();

            _animate.NextFrame(1f);

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            if (_animate.IsPlay())
            {
                Game1.DrawGradientCircle(batch, AbsXY, 32 * _animate.Value(), 24 * _animate.Value(), Color.LightGreen);
                Game1.DrawGradientCircle(batch, AbsXY, 24 * _animate.Value(), 18 * _animate.Value(), Color.LightGreen);
                //batch.DrawEllipse(_mousePosRallyPoint, new Vector2(24 * _animateRallyPoint.Value(), 18 * _animateRallyPoint.Value()), 16, Color.Orange, 4);
                Draw.FillSquare(batch, AbsXY, 8, Color.Yellow);
            }

            return base.Render(batch);
        }


    }
}
