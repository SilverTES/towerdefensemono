﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace TowerDefenseMono
{
    namespace Transition
    {
        class FadeInOut : Node
        {
            public override Node Init()
            {
                _animate.Add("close", Easing.SineEaseIn, new Tweening(0, 255, 32));
                _animate.Add("open", Easing.SineEaseIn, new Tweening(255, 0, 32));
                _animate.Start("close");

                return this;
            }

            public override Node Update(GameTime gameTime)
            {

                if (_animate.On("close"))
                {
                    Screen.StartTransition();
                    Screen.PrevScreen().Pause();
                }

                if (_animate.Off("close"))
                {
                    _animate.Start("open");

                    Screen.Swap();
                    //Game1._closeDoor.Play(0.5f, 1.0f, 1.0f);
                    //Console.WriteLine("SWAP SCREEN !");

                }

                if (_animate.Off("open"))
                {
                    //Screen.CurScreen().Start();
                    Screen.CurScreen().Resume();
                    Screen.StopTransition();
                    //Game1._clock.Play(0.5f, 1.0f, 1.0f);

                }

                _animate.NextFrame();

                return this;
            }

            public override Node Render(SpriteBatch batch)
            {

                Draw.FillRectangle(batch, new Rectangle(0, 0, Game1._screenW, Game1._screenH), new Color(0.0f, 0.0f, 0.0f, _animate.Value() / 255));

                return this;
            }

        }
    }
}