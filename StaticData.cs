﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Retro2D;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TowerDefenseMono
{
    public class SubTexture
    {
        public int frameHeight { get; set; }
        public int y { get; set; }
        public int frameX { get; set; }
        public int width { get; set; }
        public int frameY { get; set; }
        public int height { get; set; }
        public string name { get; set; }
        public int frameWidth { get; set; }
        public int x { get; set; }
    }
    public class DragonBonesAnimation
    {
        public string imagePath { get; set; }
        public string name { get; set; }
        public List<SubTexture> SubTexture { get; set; }
    }


    public partial class Game1 : Game
    {
        #region Attributes

        // Version
        public static string VERSION = "0.41";
        public static string _pathVersion = "version.txt";

        // Window 
        public static Window _window = new Window();
        public FrameCounter _frameCounter = new FrameCounter();
        static bool _isQuit = false;

        // Screen
        public static NaviState _naviState = new NaviState(); // Game Screen State
        public const int _screenW = 1920;
        public const int _screenH = 1080;
        public const int _finalScreenW = 1920;
        public const int _finalScreenH = 1080;        

        // Mouse
        public static MouseState _mouseState;
        public static int _relMouseX;
        public static int _relMouseY;

        // Messages
        public static MessageQueue _messageQueue = new MessageQueue();

        // Shake effect
        static Shake _shake = new Shake();

        // Fonts
        public static SpriteFont _font_Main;
        public static SpriteFont _font_Medium;
        public static SpriteFont _font_Big;
        public static SpriteFont _font_Mega;

        // Texture2Ds
        public static Texture2D _tex_mouseCursor;
        public static Texture2D _tex_mouseCursorMini;
        public static Texture2D _tex_mouseDragMove;
        public static Texture2D _tex_PlaceCursor;
        //public static Texture2D _testLevel;
        public static Texture2D _tex_fillCircle;
        public static Texture2D _tex_gradientCircle;

        public static Texture2D _tex_WorldMap;

        public static Texture2D _tex_Tower0;
        public static Texture2D _tex_Barrack;
        public static Texture2D _tex_GunTurret;

        public static Texture2D _tex_Icon_Tower00;
        public static Texture2D _tex_Radar00;
        public static Texture2D _tex_ToonSmoke;
        public static Texture2D _tex_Smoke;

        public static Texture2D _tex_Icon_Locked;
        public static Texture2D _tex_Icon_Refund;
        public static Texture2D _tex_Icon_Goal;

        public static Texture2D _tex_Icon_Soldier;
        public static Texture2D _tex_Icon_Slicer;
        public static Texture2D _tex_Icon_Healer;
        public static Texture2D _tex_Icon_Sniper;
        public static Texture2D _tex_Blood;

        public static Texture2D _tex_Skin_Soldier;

        static public Texture2D _tex_skinGui;

        static public Texture2D _tex_Marine_Idle;
        static public Texture2D _tex_Marine_Walk;
        static public Texture2D _tex_Marine_Fire;

        static public Texture2D _tex_Monster_Idle;
        static public Texture2D _tex_Monster_Walk;
        static public Texture2D _tex_Monster_Walk_Front;
        static public Texture2D _tex_Monster_Walk_Back;

        static public Texture2D _tex_gameOver;

        // SoundEffects
        static public float _volumeSound = 1f;
        static public Gui.Slider _sliderMusicVolume;
        static public Gui.Slider _sliderSoundVolume;
        

        public static SoundEffect _sound_lose;
        public static SoundEffect _sound_win;

        public static SoundEffect _sound_Fire;
        public static SoundEffect _sound_Rodger;
        public static SoundEffect _sound_ReadyToFight;
        public static SoundEffect _sound_Gun;
        public static SoundEffect _sound_PowerGun;
        public static SoundEffect _sound_LaserReload;
        public static SoundEffect _sound_Key;
        public static SoundEffect _sound_Open1;
        public static SoundEffect _sound_Open2;
        public static SoundEffect _sound_TowerKill;
        public static SoundEffect _sound_Clock;
        public static SoundEffect _sound_LaserGun;
        public static SoundEffect _sound_ShotGunR;
        public static SoundEffect _sound_Whip;

        public static List<SoundEffect> _sounds_Hurt = new List<SoundEffect>();
        public static List<SoundEffect> _sounds_Unit = new List<SoundEffect>();
        public static List<SoundEffect> _sounds_EnemyDeath = new List<SoundEffect>();

        const int MAX_CHANNEL_SOUND = 8;
        const int MAX_PLAYING_SOUND = 8;
        static SoundEffectInstance[,] _soundInstances = new SoundEffectInstance[MAX_CHANNEL_SOUND, MAX_PLAYING_SOUND];

        // Songs
        public static Song _song_Music0;
        public static Song _song_Music1;

        // Animations
        public static Animation _animation_ToonSmoke;
        public static Animation _animation_Smoke;

        public static Animation _anim_Marine_Idle; 
        public static Animation _anim_Marine_Walk; 
        public static Animation _anim_Marine_Fire; 

        public static Animation _anim_Monster_Idle; 
        public static Animation _anim_Monster_Walk; 
        public static Animation _anim_Monster_Walk_Front; 
        public static Animation _anim_Monster_Walk_Back;

        // Sprite
        public static Sprite _sprite_Monster0;

        //Gui Style

        public static SkinGui _skinGui_00;
        public static SkinGui _skinGui_01;
        public static SkinGui _skinGui_02;
        public static SkinGui _skinGui_03;

        public static Style _style_Main;
        public static Style _style_Medium;


        // TexturePacker Atlas
        static public TexturePackerAtlas _tpa_Monster0;


        static public List<string> _listDifficulty = new List<string>() { "NOVICE", "NORMAL", "EXPERT", "GOD" };

        #endregion

        protected override void LoadContent()
        {
            #region Load Sprite from TexturePacker File

            _sprite_Monster0 = TexturePacker.LoadSpriteFromFile(Content, "Content/Animation/Anim_Monster0.xml");

            _sprite_Monster0.Animation("Idle").SetDuration(2f);
            _sprite_Monster0.Animation("Fight").SetLoop(Loops.ONCE); // Slow down walk animation

            //Console.WriteLine("Nb Animation in Sprite ========= "+_sprite_Monster0.NbAnimation);

            #endregion

            #region Load Fonts
            //_font_Main = Content.Load<SpriteFont>("Font/mainFont");
            _font_Main = ContentLoader.SpriteFonts["mainFont"];
            _font_Medium = ContentLoader.SpriteFonts["mediumFont"];
            _font_Big = Content.Load<SpriteFont>("Font/bigFont");
            _font_Mega = Content.Load<SpriteFont>("Font/megaFont");
            #endregion

            #region Load Texture2D
            // Mouse
            _tex_mouseDragMove = Content.Load<Texture2D>("Image/DragMove");
            _tex_mouseCursor = Content.Load<Texture2D>("Image/mouse_cursor");
            _tex_mouseCursorMini = Content.Load<Texture2D>("Image/mouse_cursor_mini");
            _tex_PlaceCursor = Content.Load<Texture2D>("Image/PlaceCursor");

            // Primtives 
            _tex_fillCircle = Content.Load<Texture2D>("Image/fill_circle_1080x1080");
            _tex_gradientCircle = Content.Load<Texture2D>("Image/circle_gradient");

            // Building
            _tex_Tower0 = Content.Load<Texture2D>("Image/tower00");
            _tex_Barrack = Content.Load<Texture2D>("Image/barrack01");
            _tex_GunTurret = Content.Load<Texture2D>("Image/gunturret");

            // WorldMap
            _tex_WorldMap = Content.Load<Texture2D>("Image/WorldMap");


            // UI & Button Icon
            _tex_Icon_Tower00 = Content.Load<Texture2D>("Image/icon_tower00");
            _tex_Radar00 = Content.Load<Texture2D>("Image/radar00");

            _tex_gameOver = Content.Load<Texture2D>("Image/game_over");

            // Particles
            _tex_Blood = Content.Load<Texture2D>("Image/blood_64");
            _tex_Smoke = Content.Load<Texture2D>("Image/smoke");
            _tex_ToonSmoke = Content.Load<Texture2D>("Image/toon_smoke");
            _tex_Icon_Locked = Content.Load<Texture2D>("Image/icon_locked");
            _tex_Icon_Refund = Content.Load<Texture2D>("Image/icon_refund");
            _tex_Icon_Goal = Content.Load<Texture2D>("Image/icon_goal");

            // Heroes Icon
            _tex_Icon_Soldier = Content.Load<Texture2D>("Image/icon_soldier");
            _tex_Icon_Slicer = Content.Load<Texture2D>("Image/icon_slicer");
            _tex_Icon_Healer = Content.Load<Texture2D>("Image/icon_healer");
            _tex_Icon_Sniper = Content.Load<Texture2D>("Image/icon_sniper");

            _tex_Marine_Idle = Content.Load<Texture2D>("Image/Marines2_tex");
            _tex_Marine_Walk = Content.Load<Texture2D>("Image/Marines_Walk");
            _tex_Marine_Fire = Content.Load<Texture2D>("Image/Marines_Fire");

            _tex_Monster_Idle = Content.Load<Texture2D>("Image/Monster0_Idle");
            _tex_Monster_Walk = Content.Load<Texture2D>("Image/Monster0_Walk");
            _tex_Monster_Walk_Front = Content.Load<Texture2D>("Image/Monster0_Walk_Front");
            _tex_Monster_Walk_Back = Content.Load<Texture2D>("Image/Monster0_Walk_Back");

            // Skin GUI
            _tex_skinGui = Content.Load<Texture2D>("Image/skinGUI00");

            // Skin Character
            _tex_Skin_Soldier = Content.Load<Texture2D>("Image/skin_soldier");

            #endregion

            #region Load SoundEffect
            // Weapons
            _sound_Fire = Content.Load<SoundEffect>("Sound/burstfire");
            _sound_Rodger = Content.Load<SoundEffect>("Sound/Rodger");
            _sound_LaserGun = Content.Load<SoundEffect>("Sound/ability_fireball");
            _sound_Gun = Content.Load<SoundEffect>("Sound/laser-pistol-gun");
            _sound_LaserReload = Content.Load<SoundEffect>("Sound/laser-gun-recharge");
            _sound_PowerGun = Content.Load<SoundEffect>("Sound/Gun_Shot");
            _sound_ShotGunR = Content.Load<SoundEffect>("Sound/shotgun_reload");

            _sound_Whip = Content.Load<SoundEffect>("Sound/whip_1");

            // UI & Buttons
            _sound_Key = Content.Load<SoundEffect>("Sound/key2");
            _sound_Clock = Content.Load<SoundEffect>("Sound/clock");

            _sound_lose = Content.Load<SoundEffect>("Sound/lose");
            _sound_win = Content.Load<SoundEffect>("Sound/success_complete");

            // Constructions
            _sound_Open1 = Content.Load<SoundEffect>("Sound/electric_door_opening_1");
            _sound_Open2 = Content.Load<SoundEffect>("Sound/electric_door_opening_2");
            _sound_TowerKill = Content.Load<SoundEffect>("Sound/towerkill");

            // Quotes
            _sound_ReadyToFight = Content.Load<SoundEffect>("Sound/Ready_to_fight");
            for (int i = 0; i < 5; i++)
            {
                SoundEffect sound = Content.Load<SoundEffect>("Sound/Hurt/hurt0" + (i + 1));
                _sounds_Hurt.Add(sound);
            }
            for (int i = 0; i < 6; i++)
            {
                SoundEffect sound = Content.Load<SoundEffect>("Sound/UnitQuotes/unit" + i);
                _sounds_Unit.Add(sound);
            }
            for (int i = 0; i < 3; i++)
            {
                SoundEffect sound = Content.Load<SoundEffect>("Sound/EnemyDeath/enemy" + i);
                _sounds_EnemyDeath.Add(sound);
            }
            #endregion

            #region Load Songs
            _song_Music0 = Content.Load<Song>("Song/ambiant_horror");
            _song_Music1 = Content.Load<Song>("Song/Doubt");
            #endregion

            #region Load Animations
            Rectangle rect = new Rectangle(0, 0, 100, 100);

            // Marines Animation

            _anim_Marine_Idle = new Animation(_tex_Marine_Idle, "Idle").SetLoop(Loops.REPEAT);

            bool success = false;

            DragonBonesAnimation anim = FileIO.JsonSerialization.ReadFromJsonFile<DragonBonesAnimation>("Content/Animation/Marines2_tex.json", ref success);
            for (int i = 0; i < anim.SubTexture.Count; i++)
            {
                //Console.WriteLine(": " + anim.SubTexture[i].name);

                rect.X = anim.SubTexture[i].x;
                rect.Y = anim.SubTexture[i].y;
                rect.Width = anim.SubTexture[i].width;
                rect.Height = anim.SubTexture[i].height;

                _anim_Marine_Idle.Add(new Frame(rect, 2, 68, 90));

            }

            _anim_Marine_Walk = new Animation(_tex_Marine_Walk, "Walk").SetLoop(Loops.REPEAT);

            anim = FileIO.JsonSerialization.ReadFromJsonFile<DragonBonesAnimation>("Content/Animation/Marines_Walk.json", ref success);
            for (int i = 0; i < anim.SubTexture.Count; i++)
            {
                //Console.WriteLine(": " + anim.SubTexture[i].name);

                rect.X = anim.SubTexture[i].x;
                rect.Y = anim.SubTexture[i].y;
                rect.Width = anim.SubTexture[i].width;
                rect.Height = anim.SubTexture[i].height;

                _anim_Marine_Walk.Add(new Frame(rect, 1, 68, 90));
            }


            _anim_Marine_Fire = new Animation(_tex_Marine_Fire, "Fire").SetLoop(Loops.REPEAT);

            anim = FileIO.JsonSerialization.ReadFromJsonFile<DragonBonesAnimation>("Content/Animation/Marines_Fire.json", ref success);
            for (int i = 0; i < anim.SubTexture.Count; i++)
            {
                //Console.WriteLine(": " + anim.SubTexture[i].name);

                rect.X = anim.SubTexture[i].x;
                rect.Y = anim.SubTexture[i].y;
                rect.Width = anim.SubTexture[i].width;
                rect.Height = anim.SubTexture[i].height;

                _anim_Marine_Fire.Add(new Frame(rect, 1, 120, 120));
            }




            _anim_Monster_Idle = new Animation(_tex_Monster_Idle, "Idle").SetLoop(Loops.REPEAT);

            anim = FileIO.JsonSerialization.ReadFromJsonFile<DragonBonesAnimation>("Content/Animation/Monster0_Idle.json", ref success);
            for (int i = 0; i < anim.SubTexture.Count; i++)
            {
                //Console.WriteLine(": " + anim.SubTexture[i].name);

                rect.X = anim.SubTexture[i].x;
                rect.Y = anim.SubTexture[i].y;
                rect.Width = anim.SubTexture[i].width;
                rect.Height = anim.SubTexture[i].height;

                _anim_Monster_Idle.Add(new Frame(rect, 1, 130, 100));
            }

            _anim_Monster_Walk = new Animation(_tex_Monster_Walk, "Walk").SetLoop(Loops.REPEAT);

            anim = FileIO.JsonSerialization.ReadFromJsonFile<DragonBonesAnimation>("Content/Animation/Monster0_Walk.json", ref success);
            for (int i = 0; i < anim.SubTexture.Count; i++)
            {
                //Console.WriteLine(": " + anim.SubTexture[i].name);

                rect.X = anim.SubTexture[i].x;
                rect.Y = anim.SubTexture[i].y;
                rect.Width = anim.SubTexture[i].width;
                rect.Height = anim.SubTexture[i].height;

                _anim_Monster_Walk.Add(new Frame(rect, 1, 106, 100));
            }



            _anim_Monster_Walk_Front = new Animation(_tex_Monster_Walk_Front, "Walk_Front").SetLoop(Loops.REPEAT);

            anim = FileIO.JsonSerialization.ReadFromJsonFile<DragonBonesAnimation>("Content/Animation/Monster0_Walk_Front.json", ref success);
            for (int i = 0; i < anim.SubTexture.Count; i++)
            {
                //Console.WriteLine(": " + anim.SubTexture[i].name);

                rect.X = anim.SubTexture[i].x;
                rect.Y = anim.SubTexture[i].y;
                rect.Width = anim.SubTexture[i].width;
                rect.Height = anim.SubTexture[i].height;

                _anim_Monster_Walk_Front.Add(new Frame(rect, 1, 63, 142));
            }

            _anim_Monster_Walk_Back = new Animation(_tex_Monster_Walk_Back, "Walk_Back").SetLoop(Loops.REPEAT);

            anim = FileIO.JsonSerialization.ReadFromJsonFile<DragonBonesAnimation>("Content/Animation/Monster0_Walk_Back.json", ref success);
            for (int i = 0; i < anim.SubTexture.Count; i++)
            {
                //Console.WriteLine(": " + anim.SubTexture[i].name);

                rect.X = anim.SubTexture[i].x;
                rect.Y = anim.SubTexture[i].y;
                rect.Width = anim.SubTexture[i].width;
                rect.Height = anim.SubTexture[i].height;

                _anim_Monster_Walk_Back.Add(new Frame(rect, 1, 63, 142));
            }


            // Smoke Animation
            _animation_ToonSmoke = new Animation(_tex_ToonSmoke, "Idle").SetLoop(Loops.NONE);
            
            for (int j = 0; j < 1; j++)
            {
                for (int i = 0; i < 7; i++)
                    _animation_ToonSmoke.Add(new Frame(new Rectangle(i * 100, j * 100, 100, 100), 4, 50, 42, rect));
            }


            rect = new Rectangle(0, 0, 16, 16);
            _animation_Smoke = new Animation(_tex_Smoke, "Idle").SetLoop(Loops.NONE)
                .Add(new Frame(new Rectangle(0, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(16, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(32, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(48, 0, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(0, 16, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(16, 16, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(32, 16, 16, 16), 4, 8, 14, rect))
                .Add(new Frame(new Rectangle(48, 16, 16, 16), 4, 8, 14, rect));

            #endregion

            #region GUI Style
            // Load SkinGui texture !

            int oX = 24;
            int oY = 24;

            _skinGui_00 = new SkinGui(_tex_skinGui)
                .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8, 8) })
                .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8, 8) })
                .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8, 8) })
                .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8, 8) })
                .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8) })
                .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8) });

            oX = 24;
            oY = 0;

            _skinGui_01 = new SkinGui(_tex_skinGui)
                 .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                 .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8) });

            oX = 0;
            oY = 0;

            _skinGui_02 = new SkinGui(_tex_skinGui)
                 .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                 .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8) });

            oX = 48;
            oY = 24;

            _skinGui_03 = new SkinGui(_tex_skinGui)
                 .SetSkinGui(Position.MIDDLE, new Skin() { _rect = new Rectangle(8 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_CENTER, new Skin() { _rect = new Rectangle(8 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.LEFT_CENTER, new Skin() { _rect = new Rectangle(0 + oX, 8 + oY, 8, 8) })
                 .SetSkinGui(Position.RIGHT_CENTER, new Skin() { _rect = new Rectangle(16 + oX, 8 + oY, 8, 8) })

                 .SetSkinGui(Position.TOP_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.TOP_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 0 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_LEFT, new Skin() { _rect = new Rectangle(0 + oX, 16 + oY, 8, 8) })
                 .SetSkinGui(Position.BOTTOM_RIGHT, new Skin() { _rect = new Rectangle(16 + oX, 16 + oY, 8, 8) });


            _style_Main = new Style
            {
                _font = _font_Main,
                _skinGui = _skinGui_01,
                _showFocus = false,
                _color = { _value = Color.DarkGray },
                _textBorder = true,
                _colorTextBorder = { _value = Color.DarkBlue },
                _focusBackgroundFillColor = { _value = Color.Red * .4f },
                _focusBorderColor = { _value = Color.Red },
                _border = new Style.Space(3),
                _overBorderColor = { _value = Color.Blue },
                _overBackgroundFillColor = { _value = Color.RoyalBlue },
            };

            _style_Medium = _style_Main.Clone();
            _style_Medium._font = _font_Medium;
            _style_Medium._color = new Style.ColorValue(){ _value = Color.Gold };


            #endregion

        }

        protected override void UnloadContent()
        {
        }

    }


}
