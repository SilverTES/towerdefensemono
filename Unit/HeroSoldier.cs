﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace TowerDefenseMono.Unit
{
    class HeroSoldier : BaseUnit
    {
        #region Attributes
        public static Vector2 RangeLevel1 { get; private set; } = new Vector2(120, 90);
        public static Vector2 RangeLevel2 { get; private set; } = new Vector2(160, 120);
        public static Vector2 RangeLevel3 { get; private set; } = new Vector2(200, 150);

        //Vector2 _holdPosition; // Position before fight !
        //Node _owner;
        Node _layerUI;
        //public bool _isOwnerSelected = false;
        float _ticSmoke = 0f;

        // Energy regeneration
        float _ticEnergyRecovery = 0f;
        float _speedRecovery = 0.005f;

        Sprite _sprite;

        #endregion

        public HeroSoldier(Player player, Input.Mouse mouse, Vector2 range, Node layerUI) : base(player, mouse, range)
        {
            //_owner = owner;
            _subType = UID.Get<HeroSoldier>();
            _isHero = true;

            _layerUI = layerUI;

            _maxEnergy = 480;
            _energy = _maxEnergy;
            _powerAttack = 24;
            _speedAttack = .016f;

            //SetSize(16, 20);
            //SetPivot(8, 12);

            _sprite = new Sprite();
            _sprite.Add(Game1._anim_Marine_Idle);
            _sprite.Add(Game1._anim_Marine_Walk);
            _sprite.Add(Game1._anim_Marine_Fire);
            _sprite.Start("Walk", 1, 0);

            _energyBar.SetColor(Color.Orange);
        }
        public override Node Init()
        {


            return base.Init();
        }
        public override void SetRallyPoint(Vector2 rallyPoint)
        {
            base.SetRallyPoint(rallyPoint);

            _isWaitRallyPoint = false;

            SetTarget(null);
            GotoGoal(XY, rallyPoint);

            new Particles.RallyPointCursor(new Vector2(rallyPoint.X, rallyPoint.Y)).AppendTo(_layerUI);
            //Game1.PlaySound(1, Game1._sound_Clock, .1f, .01f);

            if (Misc.Rng.Next(0,10) > 6)
                Game1.PlaySound(4, Game1._sound_ReadyToFight, .2f, .0001f);
        }
        public override Node Update(GameTime gameTime)
        {
            _sprite.Update();

            #region Collisions
            var _listZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(ZONE_RANGE), UID.Get<BaseUnit>(), BaseUnit.ZONE_BODY);

            if (_listZone.Count > 0 && _target == null && _isGoal)
            {
                //int minGoalCount = 1000;

                BaseUnit unit = null;

                for (int i = 0; i < _listZone.Count; i++)
                {
                    Node collider = _listZone[i]._node;

                    if (Collision2D.PointInEllipse(collider.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2))
                    {
                        //if is a MobilUnit
                        if (collider._subType == UID.Get<Enemy0>())
                        {
                            unit = collider.This<Enemy0>();

                            _target = unit; // By default target the enemy in rangeZone

                            // Select the most Far Unit ! near the goal
                            //if (unit.QueueGoalCount() < minGoalCount)
                            //{
                            //    minGoalCount = unit.QueueGoalCount();
                            //    _target = unit;
                            //    //unit._isTargetBy = this;
                            //}

                            // if find no targeted enemy by other player units, Break the search
                            if (null == unit._isTargetBy)
                            {
                                _target = unit;
                                break;
                            }


                        }

                    }

                }


                if (null != unit)
                {
                    UpdateRect();
                    _holdPosition = XY;

                    float randY = Misc.Rng.Next(-8, 8);

                    // soldier is left of target
                    if (_x <= unit._x)
                        GotoGoal(XY, new Vector2(unit._x - 48, unit._y + randY));
                    else
                        GotoGoal(XY, new Vector2(unit._x + 48, unit._y + randY));
                }

            }
            #endregion

            if (_isSelected) _isWaitRallyPoint = true;

            if (_isWaitRallyPoint)
            {
                if (_mouse._onClick && !_mouse._isOver)
                {
                    SetRallyPoint(new Vector2(_mouse.AbsX, _mouse.AbsY));

                }
            }


            // Check if enemy is out of range , null target in this case
            _onAttack = false;

            if (_target != null)
            {
                var unit = _target.This<BaseUnit>();
                //if (_isGoal)
                //{
                //    _sprite.SetAnimation("Fire");

                //    if (_sprite.CurFrame == 4)
                //        new Particles.PopInfo("-" + _powerAttack, Color.Cyan, Color.Blue)
                //            .SetPosition(_target._x, _target._y - 32)
                //            .AppendTo(_parent);
                //}
                //else
                //    _sprite.SetAnimation("Walk");

                if (_isGoal)
                {
                    _ticAttack += _speedAttack;

                    if (_ticAttack >= 1f)
                    {
                        _ticAttack = 0;
                        _onAttack = true;
                        _isAttack = true;
                    }

                    if (!_sprite.IsPlay)
                        _sprite.SetAnimation("Idle");


                    if (_sprite.CurAnimation == "Fire")
                    {
                        if (_sprite.CurFrame == 4 && _isAttack)  // Visual Hit
                        {
                            _isAttack = false;

                            unit.ShowHitDamage(_powerAttack);
                            unit.HitDamage(-_powerAttack);

                            if (InRectView())
                                Game1.PlaySound(1, Game1._sound_ShotGunR, .04f, .01f);
                        }
                    }

                }
                else
                    _sprite.SetAnimation("Walk");


                if (_target._subType == UID.Get<Enemy0>())
                {
                    unit._isTargetBy = this; // Mark unit as target

                    if (_x <= unit._x)
                        _currentDirection = Position.RIGHT;
                    else
                        _currentDirection = Position.LEFT;

                    if (_onAttack && _isGoal)
                    {
                        //_isAttack = false;

                        _sprite.Start("Fire", 1, 0);

                        //unit.ShowHitDamage(_powerAttack); // Visual Effect
                        //unit.HitDamage(-_powerAttack);

                        ////SoundSystem.PlaySound();
                        //if (InRectView())
                        //    Game1.PlaySound(1, Game1._sound_ShotGunR, .04f, .01f);

                    }

                    if (!Collision2D.PointInEllipse(_target.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2)
                        || unit.IsDead)
                    {
                        // null both unit : target & isTargetBy
                        unit._isTargetBy = null; 
                        _isTargetBy = null;

                        unit._target = null;
                        _target = null;

                        _ticAttack = 0;
                        _isAttack = false;

                        AddGoal(_holdPosition.X, _holdPosition.Y);
                    }
                }

            }
            else
            {
                if (IsMove)
                    _sprite.SetAnimation("Walk");
                else
                    _sprite.SetAnimation("Idle");
            }

            if (_energy <= 0)
            {
                //if (_owner != null)
                //    _owner._message = new Message(UID.Get<Soldier>(), "HERO SOLDIER DEAD : " + _index, this);

                new Blood(Game1._tex_Blood, _x, _y).AppendTo(_parent);

                Game1.PlaySound(1, Game1._sounds_Hurt[Misc.Rng.Next(0, 4)], .1f, .01f);

                KillUnit();
            }

            _ticSmoke += 0.1f;
            if (_ticSmoke >= 1f && _isMove)
            {
                _ticSmoke = 0;
                new Smoke(_x, _y, 3)
                    .AppendTo(_parent);
            }


            if (_energy > 0 && _energy < _maxEnergy && !_isMove && _target == null)
            {
                _ticEnergyRecovery += _speedRecovery;
                if (_ticEnergyRecovery >= 1f )
                {
                    _ticEnergyRecovery = 0;

                    HealEnergy(10);

                    if (_energy >= _maxEnergy) _energy = _maxEnergy;
                }
            }

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            base.Render(batch);

            bool flipX = _currentDirection == Position.RIGHT;
            _sprite.Render(batch, AbsX, AbsY + 16, Color.Black * .42f, .24f, .1f, 0, 0, 0, flipX ? SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically : SpriteEffects.FlipVertically, flipX);
            _sprite.Render(batch, AbsX, AbsY + (_isMove ? _animZ : 0), Color.GreenYellow, .42f, .42f, 0, 0, 0, flipX ? SpriteEffects.FlipHorizontally : SpriteEffects.None, flipX);


            //if (_target != null)
            //{
            //    Draw.Line(batch, AbsXY, _target.AbsXY, Color.Violet * .8f, 3);
            //}

            //// Shadow
            //batch.Draw(Game1._tex_fillCircle, new Rectangle(AbsX - 10, AbsY, 20, 15), Color.Black * 0.4f);

            //if (_isOwnerSelected)
            //    Game1.DrawGradientCircle(batch, new Vector2(AbsX, AbsY + 8), 16, 12, Color.Orange);

            //Draw.FillRectangle(batch, AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.YellowGreen);
            //Draw.Rectangle(batch, AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.Green, 4);

            ////batch.Draw(Game1._tex_Skin_Soldier, Gfx.TranslateRect(AbsRect, new Point(0, (int)_animZ)), Color.White);

            //if (_onAttack)
            //    Draw.Rectangle(batch, AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.White, 8);

            return this;
        }
    }
}
