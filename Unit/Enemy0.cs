﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using System;

namespace TowerDefenseMono.Unit
{
    class Enemy0 : BaseUnit
    {
        #region Attributes

        public const int KILL_POINT = 7;
        public static Vector2 RangeLevel1 { get; private set; } = new Vector2(100, 75);
        public static Vector2 RangeLevel2 { get; private set; } = new Vector2(120, 90);
        public static Vector2 RangeLevel3 { get; private set; } = new Vector2(160, 120);

        Sprite _sprite;

        #endregion

        public Enemy0(Player player, Input.Mouse mouse, Vector2 range) : base(player, mouse, range)
        {
            _subType = UID.Get<Enemy0>();

            _maxEnergy = 64; 
            _energy = _maxEnergy;
            _powerAttack = 6;
            _speedAttack = .020f;

            _sprite = Game1._sprite_Monster0.Clone();
            //_sprite.Add(Game1._anim_Monster_Idle);
            //_sprite.Add(Game1._anim_Monster_Walk);
            //_sprite.Add(Game1._anim_Monster_Walk_Front);
            //_sprite.Add(Game1._anim_Monster_Walk_Back);
            _sprite.Start("Walk", 1, 0);

            _energyBar.SetColor(Color.Yellow, Color.Red);

        }
        public override Node Init()
        {

            return base.Init();
        }
        public override void ShowHitDamage(int damage)
        {
            new Particles.PopInfo("-" + damage, Color.Orange, Color.DarkRed)
                .SetPosition(_x, _y - 32)
                .AppendTo(_parent);
        }
        public override Node Update(GameTime gameTime)
        {
            _sprite.Update();

            #region Collisions
            //var _listZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(ZONE_RANGE), UID.Get<BaseUnit>(), ZONE_BODY);

            //if (_listZone.Count > 0 && _target == null)
            //{
            //    //int minGoalCount = 1000;

            //    for (int i = 0; i < _listZone.Count; i++)
            //    {
            //        Node collider = _listZone[i]._node;

            //        if (Collision2D.PointInEllipse(collider.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2))
            //        {
            //            //if is a MobilUnit
            //            if (collider._subType == UID.Get<Marine>() || collider._subType == UID.Get<HeroSoldier>())
            //            {
            //                var unit = collider.This<BaseUnit>();

            //                //// Select the most Far Unit ! near the goal
            //                //if (unit.QueueGoalCount() < minGoalCount)
            //                //{
            //                //    minGoalCount = unit.QueueGoalCount();
            //                //    _target = unit;
            //                //}

            //                _target = unit;
            //            }



            //        }

            //    }

            //}
            #endregion

            _target = _isTargetBy; // enemy target by player units is the target !

            if (_isTargetBy != null) // Stop move when is Target by player units
            {
                if (_isTargetBy._subType == UID.Get<Marine>() || _isTargetBy._subType == UID.Get<HeroSoldier>())
                {
                    if (_x <= _isTargetBy._x) // Fight Direction
                        _currentDirection = Position.RIGHT;
                    else
                        _currentDirection = Position.LEFT;


                    if (!_isPauseMove)
                    {
                        _sprite.SetAnimation("Idle");
                        _isPauseMove = true;
                    }

                }
            }

            // Check if enemy is out of range , null target in this case
            _onAttack = false;
            
            if (_target != null)
            {
                var unit = _target.This<BaseUnit>();

                if (_isPauseMove)// Attack only if stop move !
                {
                    _ticAttack += _speedAttack;

                    if (_ticAttack >= 1f)
                    {
                        _ticAttack = 0;
                        _onAttack = true;
                        _isAttack = true;
                    }

                    if (!_sprite.IsPlay)
                        _sprite.SetAnimation("Idle");

                    if (_sprite.CurAnimation == "Fight")
                    {
                        if (_sprite.CurFrame == 8 && _isAttack)  // Visual Hit
                        {
                            _isAttack = false;

                            unit.ShowHitDamage(_powerAttack);
                            unit.HitDamage(-_powerAttack);
                            
                        }

                    }

                }
                else
                {
                    if (Math.Abs(_currentVector.X) >= Math.Abs(_currentVector.Y))
                        _sprite.SetAnimation("Walk");
                    else
                    {
                        if (_currentVector.Y > 0)
                            _sprite.SetAnimation("Walk_Front");
                        else
                            _sprite.SetAnimation("Walk_Back");
                    }
                }

                if (_target._subType == UID.Get<Marine>() || _target._subType == UID.Get<HeroSoldier>())
                {
                    
                    unit._isTargetBy = this; // Mark unit as target

                    if (_onAttack)
                    {
                        _sprite.Start("Fight", 1, 0);

                        if (InRectView())
                            Game1.PlaySound(5, Game1._sound_Whip, .04f, .4f);

                    }

                    if (!Collision2D.PointInEllipse(_target.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2)
                        || unit.IsDead)
                    {
                        // null both unit : target & isTargetBy
                        unit._isTargetBy = null;
                        _isTargetBy = null;

                        unit._target = null;
                        _target = null; 

                        _ticAttack = 0;
                        _isAttack = false;

                        if (unit.IsDead)
                            Console.WriteLine("Enemy Kill Soldier !!!");

                        _isPauseMove = false;

                        _sprite.Resume(); // resume loop animation because Fight Animation is only play once !

                    }
                }

            }
            else
            {
                if (_isPauseMove || _isTargetBy != null)
                    _sprite.SetAnimation("Idle");
                else
                {
                    if (Math.Abs(_currentVector.X) >= Math.Abs(_currentVector.Y))
                        _sprite.SetAnimation("Walk");
                    else
                    {
                        if (_currentVector.Y > 0)
                            _sprite.SetAnimation("Walk_Front");
                        else
                            _sprite.SetAnimation("Walk_Back");
                    }
                    //_sprite.SetAnimation("Walk");
                }
            }



            if (_energy <= 0)
            {
                new Blood(Game1._tex_Blood, _x, _y).AppendTo(_parent);
                
                Game1.PlaySound(2, Game1._sounds_EnemyDeath[Misc.Rng.Next(0, 2)], .05f, .01f);

                _player.AddPoint(KILL_POINT);

                new Particles.PopInfo("+"+KILL_POINT, Color.Yellow, Color.OrangeRed)
                    .SetPosition(_x, _y - 24)
                    .AppendTo(_parent);

                KillUnit();
            }

            // enemy readh end of path
            if (EndOfMove)
            {
                _player.HitDamage(1);

                // Shake the screen !!
                Game1.Shake(10f, .1f);

                System.Console.WriteLine("Attack Player Enegy ============= !!");
                KillUnit();
            }

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            base.Render(batch);

            //if (_target != null)
            //{
            //    batch.DrawLine(AbsXY, _target.AbsXY, Color.Yellow * .5f, 1);
            //}

            // Shadow
            //batch.Draw(Game1._tex_fillCircle, new Rectangle(AbsX, AbsY, 48, 24), Color.Black * 0.4f);

            //Draw.FillRectangle(batch, AbsX - 10, AbsY - 12 + _animZ, 20, 20, Color.DarkRed);
            //Draw.Rectangle(batch, AbsX - 10, AbsY - 12 + _animZ, 20, 20, Color.Black, 1);


            bool flipX = _currentDirection == Position.RIGHT;

            //_sprite.Render(batch, AbsX, AbsY + 16, Color.Black * .4f, .24f, .1f, 0, 0, 0, flipX ? SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically: SpriteEffects.FlipVertically, flipX);

            _sprite.Render(batch, AbsX, AbsY + (_isPauseMove ? 0: 0), Color.White, .6f, .6f, 0, 0, 0, flipX ? SpriteEffects.FlipHorizontally : SpriteEffects.None, flipX);



            //if (_onAttack)
            //    Draw.Rectangle(batch, AbsX - 10, AbsY - 12 + _animZ, 20, 20, Color.White, 2);

            //if (InRectView()) Draw.Circle(batch, AbsXY, 32, 32, Color.Magenta, 4);


            return this;
        }
    }
}
