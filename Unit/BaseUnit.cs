﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using MonoGame.Extended;
using Retro2D;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Windows.Media.Converters;

namespace TowerDefenseMono
{
    public class BaseUnit : Selectable
    {
        #region Attributes

        Queue<Vector2> _queueGoal = new Queue<Vector2>();

        public const int ZONE_BODY = 0;
        public const int ZONE_RANGE = 1;


        // if is HERO
        public bool _isHero = false;
        public bool _isWaitRallyPoint { get; protected set; }


        protected Vector2 _holdPosition; // Position before fight !
        protected Vector2 _rallyPoint; // Position where the player click !
        protected bool _hasRallyPoint = false;

        // Optimisation !
        int _tempoScanRange = 30;
        int _ticScanRange = 0;

        // Target 
        public Node _target = null; // your target !
        public Node _isTargetBy = null; // who is target me !

        protected float _animZ = 0f;
        protected float _speedZ = .25f;
        protected float _vZ = -.25f;
        protected float _upZ = -2f;

        protected float _speed = 1f;
        protected int _maxEnergy = 48;
        protected int _energy = 48;
        protected bool _isDead = false;
        public bool IsDead => _isDead;
        protected bool _isPauseMove = false;

        // Attack
        protected int _powerAttack = 2;
        protected float _speedAttack = .04f;
        protected float _ticAttack = 0;
        protected bool _onAttack = false;
        protected bool _isAttack = false;


        float _current = 0f;
        float _distance = 0f;
        protected Vector2 _currentStart;
        protected Vector2 _currentGoal;
        protected Vector2 _currentVector;
        Gfx.Line _curentPathLine;

        public int _indexPathLine { get; private set; }

        protected bool _onMove = false;
        protected bool _isMove = false;
        protected bool _onGoal = false;
        protected bool _isGoal = true;

        public bool OnMove => _onMove;
        public bool IsMove => _isMove;
        public bool OnGoal => _onGoal;
        public bool IsGoal => _isGoal;
        public bool IsFight => _target != null;
        public bool EndOfMove => _onGoal && _queueGoal.Count == 0; // If End of path
        public int QueueGoalCount => _queueGoal.Count;

        protected RectangleF _rectZoneRange = new RectangleF(0, 0, 160, 120);
        protected Vector2 _range = Vector2.Zero;

        public Position _currentDirection = Position.LEFT;

        protected ProgressBar _energyBar;

        #endregion

        public BaseUnit(Player player, Input.Mouse mouse, Vector2 range) : base(player, mouse)
        {
            _type = UID.Get<BaseUnit>();

            _player = player;

            SetSize(16, 20);
            SetPivot(8, 12);

            _range = range;
            // Range of the unit attack !
            _rectZoneRange.Width = _range.X * 2;
            _rectZoneRange.Height = _range.Y * 2;

            SetCollideZone(ZONE_BODY, _rect);
            SetCollideZone(ZONE_RANGE, _rectZoneRange);

            _energyBar = new ProgressBar(0, _maxEnergy, 32, 5, Color.ForestGreen, Color.Black, Color.Black*.4f, 2, 4);

            //Init();
        }
        //public override Node Init()
        //{
        //    _animZ = 0f;
        //    _speedZ = .25f;
        //    _vZ = -.25f;
        //    _upZ = -2f;

        //    _speed = 1f;
        //    _energy = 48;
        //    _isDead = false;
        //    _isTargetBy = null;

        //    // Attack
        //    _powerAttack = 4;
        //    _speedAttack = .01f;
        //    _ticAttack = 0;
        //    _onAttack = false;

        //    _current = 0f;
        //    _distance = 0f;

        //    _onMove = false;
        //    _isMove = false;
        //    _onGoal = false;
        //    _isGoal = false;

        //    _rectZoneRange = new RectangleF(0, 0, 240, 180);
        //    _tempoScanRange = 30;
        //    _ticScanRange = 0;

        //    return base.Init();
        //}

        public void SetHoldPosition(Vector2 holdPosition)
        {
            _holdPosition = holdPosition;
        }
        public virtual void SetRallyPoint(Vector2 rallyPoint)
        {
            _hasRallyPoint = true;
            _rallyPoint = rallyPoint;
        }
        public void HitDamage(int damage)
        {
            _energy += damage;

        }
        public virtual void ShowHitDamage(int damage)
        {
            new Particles.PopInfo("-" + damage, Color.Cyan, Color.Blue)
                .SetPosition(_x, _y - 32)
                .AppendTo(_parent);
        }
        public virtual void HealEnergy(int energy)
        {
            _energy += energy;

            new Particles.PopInfo("+" + energy, Color.GreenYellow, Color.DarkGreen)
                .SetPosition(_x, _y - 32)
                .AppendTo(_parent);
        }

        public void SetWaitRallyPoint(bool isWaitRallyPoint) { _isWaitRallyPoint = isWaitRallyPoint; }
        public void KillUnit() { _isDead = true; KillMe(); }
        public void SetEnergy(int energy) { _maxEnergy = _energy = energy; }
        public void SetTarget(BaseUnit target) { _target = target; }
        public void SetIndexPathLine(int index)
        {
            _indexPathLine = index;
        }
        public int NextIndexPathLine()
        {
            _indexPathLine++;

            return _indexPathLine;
        }
        public Node Setup(float x, float y, float speed = 1)
        {
            _x = x;
            _y = y;
            _speed = speed;
            return this;
        }
        public Node SetSpeed(float speed =  1)
        {
            _speed = speed;
            return this;
        }
        public void AddGoal(float x, float y)
        {
            _queueGoal.Enqueue(new Vector2(x, y));
        }
        public void AddGoal(Gfx.Line line, float percent)
        {
            Vector2 goal = line.Percent(percent);
            _queueGoal.Enqueue(goal);
        }
        public void ClearAllGoal()
        {
            _queueGoal.Clear();
        }
        public void GotoGoal(Vector2 start, Vector2 goal)
        {
            _isGoal = false;
            _isMove = true;
            _onMove = true;

            _current = 0;

            UpdateRect(); // update XY = (_x , _y)
            _currentStart = start;
            _currentGoal = goal;

            _distance = (float)Geo.GetDistance(_currentStart, _currentGoal);
            if (_distance == 0) _distance = 1;

            //Console.WriteLine("distance = " + _distance);
            //_currentVector = Geo.GetVector(new Vector2(_x, _y), _currentGoal, _speed);
        }
        public Position GetDirection(float vxMin = 0.3f)
        {
            float vx = _currentVector.X;
            float vy = _currentVector.Y;

            if (vx > vxMin) return Position.E;
            if (vx < -vxMin) return Position.W;

            if (vy > 0) return Position.S;
            if (vy < 0) return Position.N;

            return Position.CENTER;
        }
        //public Position CurrentDir()
        //{
        //    if (_currentStart.X <= _currentGoal.X)
        //        return Position.LEFT;

        //    return Position.CENTER;
        //}
        public override Node Update(GameTime gameTime)
        {
            _energyBar.SetValue(_energy);
            _energyBar.SetMaxValue(_maxEnergy);

            UpdateRect();

            UpdateCollideZone(ZONE_BODY, _rect, true);

            #region Collisions
            _rectZoneRange.Position = new Vector2(-_rectZoneRange.Width / 2, -_rectZoneRange.Height / 2) + XY;

            _ticScanRange ++;
            UpdateCollideZone(ZONE_RANGE, _rectZoneRange, _ticScanRange > _tempoScanRange);
            if (_ticScanRange > _tempoScanRange) _ticScanRange = 0;


            #endregion

            _isMouseOver = Misc.InRect(_mouse.AbsX, _mouse.AbsY, _rect);

            _z = -_y;

            _onMove = false;
            _onGoal = false;



            if (!_isMove)
            {
                if (_queueGoal.Count > 0)
                {
                    GotoGoal(XY, _queueGoal.Peek());
                }
            }
            else
            {


                //if (!_isSelected) // Debug
                if (!_isPauseMove) // Debug
                {
                    _current += _speed;

                    if (_currentStart.X <= _currentGoal.X)
                        _currentDirection = Position.RIGHT;
                    else
                        _currentDirection = Position.LEFT;

                }

                _x = Easing.GetValue(Easing.Linear, _current, _currentStart.X, _currentGoal.X, _distance);
                _y = Easing.GetValue(Easing.Linear, _current, _currentStart.Y, _currentGoal.Y, _distance);


                _currentVector.X = _currentGoal.X - _currentStart.X;
                _currentVector.Y = _currentGoal.Y - _currentStart.Y;

                //_x += _currentVector.X;
                //_y += _currentVector.Y;

                if (_distance - _current <= _speed)
                {
                    _isGoal = true;
                    _onGoal = true;
                    _isMove = false;

                    if (_queueGoal.Count > 0)
                        _queueGoal.Dequeue();
                }
            }

            _animZ += _vZ;

            if (_animZ < _upZ) _vZ = _speedZ;
            if (_animZ > 0) _vZ = -_speedZ;

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            //Draw path
            //if (IsQueueGoal())
            //{
            //    Vector2 prevVec = new Vector2(_x, _y);

            //    foreach (Vector2 vec in _queueGoal)
            //    {
            //        batch.DrawLine(prevVec, vec, Color.Lavender * .6f, 1);
            //        batch.DrawPoint(vec, Color.GreenYellow, 3);
            //        prevVec = vec;
            //    }
            //}

            if (_isMouseOver)
                Game1.DrawGradientCircle(batch, AbsXY + new Vector2(0, 6), _rectZoneRange.Width / 2, _rectZoneRange.Height / 2, Color.Orange *.4f);

            if (_isSelected)
                Game1.DrawGradientCircle(batch, AbsXY, 24, 18, Color.Orange);

            //if (_isMouseOver)
            //    Draw.Rectangle(batch, AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.Orange, 1);

            //float x = AbsX, y = AbsY;

            //if (GetDirection() == Position.N) y = AbsY - 4;
            //if (GetDirection() == Position.S) y = AbsY + 4;
            //if (GetDirection() == Position.W) x = AbsX - 4;
            //if (GetDirection() == Position.E) x = AbsX + 4;

            // Debug
            //batch.FillRectangle (x-2, y-2 + _animZ, 4, 4, Color.DarkOliveGreen);

            //string info = _currentVector.X + " : " + _currentVector.Y;

            //string info = _index.ToString();// + " : " + (_distance - _current) + " : " + AbsRectF;
            //batch.DrawString(Game1._mainFont, _queueGoal.Count.ToString(), AbsXY + new Vector2(0, -24), Color.DeepSkyBlue);
            //Color color = Color.Yellow;

            //if (_energy < (_maxEnergy/3)*2) color = Color.Orange;
            //if (_energy < (_maxEnergy/3)*1) color = Color.Red;

            //batch.DrawString(Game1._font_Main, _energy.ToString(), AbsXY + new Vector2(-_oX, -_oY+28), color);
            _energyBar.Render(batch, AbsX, AbsY + 16);

            //Debug next path line
            //batch.DrawString(Game1._font_Main, _queueGoal.Count.ToString(), AbsXY + new Vector2(-_oX, -_oY-32), Color.Aqua);

            #region Debug Visual
            //if (_isSelected)
            //    Game1.DrawGradientCircle(batch, AbsXY, 24, 18, Color.Cyan);

            #endregion

            return this;
        }
    }
}
