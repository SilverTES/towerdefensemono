﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;

namespace TowerDefenseMono.Unit
{
    class Marine : BaseUnit
    {
        #region Attributes
        public static Vector2 RangeLevel1 { get; private set; } = new Vector2(160, 120);
        public static Vector2 RangeLevel2 { get; private set; } = new Vector2(200, 150);
        public static Vector2 RangeLevel3 { get; private set; } = new Vector2(240, 180);

        Node _owner;
        public bool _isOwnerSelected = false;

        Sprite _sprite;

        #endregion

        public Marine(Player player, Input.Mouse mouse, Node owner, Vector2 range) : base(player, mouse, range)
        {
            _owner = owner;
            _subType = UID.Get<Marine>();

            _maxEnergy = 64; 
            _energy = _maxEnergy;
            _powerAttack = 4;
            _speedAttack = .024f;

            _sprite = new Sprite();
            _sprite.Add(Game1._anim_Marine_Idle);
            _sprite.Add(Game1._anim_Marine_Walk);
            _sprite.Add(Game1._anim_Marine_Fire);
            _sprite.Start("Walk", 1, 0);

        }
        public override Node Init()
        {


            return base.Init();
        }
        public override Node Update(GameTime gameTime)
        {
            _sprite.Update();

            #region Collisions
            var _listZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(ZONE_RANGE), UID.Get<BaseUnit>(), BaseUnit.ZONE_BODY);

            if (_listZone.Count > 0 && _target == null)
            {
                //int minGoalCount = 1000;

                BaseUnit unit = null;

                for (int i = 0; i < _listZone.Count; i++)
                {
                    Node collider = _listZone[i]._node;

                    if (Collision2D.PointInEllipse(collider.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2))
                    {
                        //if is a MobilUnit
                        if (collider._subType == UID.Get<Enemy0>())
                        {
                            unit = collider.This<Enemy0>();

                            _target = unit; // By default target the enemy in rangeZone

                            // Select the most Far Unit ! near the goal
                            //if (unit.QueueGoalCount() < minGoalCount)
                            //{
                            //    minGoalCount = unit.QueueGoalCount();
                            //    _target = unit;
                            //    //unit._isTargetBy = this;
                            //}

                            // if find no targeted enemy by other player units, Break the search
                            if (null == unit._isTargetBy)
                            {
                                _target = unit;
                                break;
                            }


                        }

                    }

                }

                
                if (null != unit)
                {
                    UpdateRect();
                    _holdPosition = XY;

                    float randY = Misc.Rng.Next(-8, 8);

                    // soldier is left of target
                    if (_x <= unit._x)
                        GotoGoal(XY, new Vector2(unit._x - 48, unit._y + randY));
                    else
                        GotoGoal(XY, new Vector2(unit._x + 48, unit._y + randY));
                }

            }
            #endregion

            // Check if enemy is out of range , null target in this case
            _onAttack = false;

            if (_target != null)
            {
                var unit = _target.This<BaseUnit>();

                if (_isGoal)
                {
                    _ticAttack += _speedAttack;
                    if (_ticAttack >= 1f)
                    {
                        _ticAttack = 0;
                        _onAttack = true;
                        _isAttack = true;
                    }

                    if (!_sprite.IsPlay)
                        _sprite.SetAnimation("Idle");


                    if (_sprite.CurAnimation == "Fire")
                    {
                        if (_sprite.CurFrame == 4 && _isAttack)  // Visual Hit
                        {
                            _isAttack = false;

                            unit.ShowHitDamage(_powerAttack);
                            unit.HitDamage(-_powerAttack);

                            if (InRectView())
                                SoundSystem.PlaySound();
                        }
                    }

                }
                else
                    _sprite.SetAnimation("Walk");

                if (_target._subType == UID.Get<Enemy0>())
                {

                    unit._isTargetBy = this; // Mark unit as target

                    if (_x <= unit._x)
                        _currentDirection = Position.RIGHT;
                    else
                        _currentDirection = Position.LEFT;

                    if (_onAttack && _isGoal)
                    {
                        _sprite.Start("Fire", 1, 0);
                    }

                    if (!Collision2D.PointInEllipse(_target.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2)
                        || unit.IsDead)
                    {
                        // null both unit : target & isTargetBy
                        unit._isTargetBy = null;
                        _isTargetBy = null;

                        unit._target = null;
                        _target = null;

                        _ticAttack = 0;
                        _isAttack = false;


                        if (_hasRallyPoint)
                            AddGoal(_rallyPoint.X, _rallyPoint.Y);
                        else
                            AddGoal(_holdPosition.X, _holdPosition.Y);
                    }
                }

            }
            else
            {
                if (IsMove)
                    _sprite.SetAnimation("Walk");
                else
                    _sprite.SetAnimation("Idle");
            }

            if (_energy <= 0)
            {
                if (_owner != null)
                    _owner._message = new Message(UID.Get<Marine>(),  "SOLDIER DEAD : "+_index , this);

                new Blood(Game1._tex_Blood, _x, _y).AppendTo(_parent);

                Game1.PlaySound(1, Game1._sounds_Hurt[Misc.Rng.Next(0,4)], .1f, .01f);

                KillUnit();
            }

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            base.Render(batch);

            //if (_target != null)
            //{
            //    Draw.Line(batch, AbsXY, _target.AbsXY, Color.Red * .8f, 2);
            //}

            // Shadow
            //batch.Draw(Game1._tex_fillCircle, new Rectangle(AbsX - 10, AbsY, 24, 18), Color.Black * 0.4f);

            if (_isOwnerSelected)
                Game1.DrawGradientCircle(batch, new Vector2(AbsX, AbsY + 8), 16,  12, Color.Orange);

            //batch.FillRectangle(AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.MediumSpringGreen);
            //Draw.FillRectangle(batch, AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.LightSkyBlue);
            //Draw.Rectangle(batch, AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.Black, 1);

            bool flipX = _currentDirection == Position.RIGHT;

            _sprite.Render(batch, AbsX, AbsY + 16, Color.Black * .4f, .24f, .1f, 0, 0, 0, flipX ? SpriteEffects.FlipHorizontally | SpriteEffects.FlipVertically : SpriteEffects.FlipVertically, flipX);

            _sprite.Render(batch, AbsX, AbsY + (_isMove? _animZ: 0), Color.White,.4f, .4f,0,0,0, flipX ?  SpriteEffects.FlipHorizontally: SpriteEffects.None, flipX);

            // Debug
            //Draw.Line(batch, AbsXY, _rallyPoint + _parent.AbsXY, Color.Red * .4f, 3f);
            //Draw.Point(batch, _rallyPoint + _parent.AbsXY, 5, Color.Red * .4f);

            //if (_onAttack)
            //    Draw.Rectangle(batch, AbsX - 8, AbsY - 12 + _animZ, 16, 20, Color.White, 2);

            //if (InRectView()) Draw.Circle(batch, AbsXY, 32, 32, Color.Magenta, 4);

            //batch.DrawString(Game1._font_Main, _sprite.CurrentAnimation + " : " + _sprite.CurFrame, AbsXY, Color.Orange);


            return this;
        }
    }
}
