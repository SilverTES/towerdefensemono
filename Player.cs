﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Retro2D;
using TiledSharp;

namespace TowerDefenseMono
{
    public class Player
    {
        #region Attributes

        public const int MAX_LIFE = 20;
        int _life = MAX_LIFE;
        public int Life => _life;
        bool IsAlive => _life > 0;
        int _bank = 0;
        public int Bank => _bank;

        public bool IsGameOver => _life == 0;

        PlayerHUD _playerHUD;

        public BaseUnit _selectedHero = null;

        public Level _level { get; private set; }

        #endregion

        public Player(Level level, Input.Mouse mouse, TmxMap tmxMap)
        {
            _level = level;
            _playerHUD = new PlayerHUD(level, mouse, tmxMap, this);
        }
        public void Init()
        {
            _life = MAX_LIFE;
            _playerHUD.Init();
        }
        public void HitDamage(int damage)
        {
            _life -= damage;

            if (_life <= 0)
            {
                _life = 0;
            }
        }
        public void SetBank(int bank)
        {
            _bank = bank;
        }
        public void AddPoint(int points)
        {
            _bank += points;
        }
        public bool CanBuy(int needPoint)
        {
            return needPoint <= _bank;
        }
        public bool Buy(int needPoint)
        {
            if (CanBuy(needPoint))
            {
                AddPoint(-needPoint);
                return true;
            }

            return false;
        }
        public void Update(GameTime gameTime)
        {
            _playerHUD.Update(gameTime);
        }
        public void Render(SpriteBatch batch)
        {

            _playerHUD.Render(batch, new Vector2(8, 4));
        }
    }
}
