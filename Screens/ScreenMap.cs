﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Retro2D;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TowerDefenseMono
{
    public class ScreenMap : Node
    {
        bool _isGotoPlay = false;
        static public Gui.Slider _sliderDifficulty;

        //Curve curveX = new Curve();
        //Curve curveY = new Curve();

        //public Vector2 GetPointOnCurve(float time)
        //{
        //    Vector2 point = new Vector2();
        //    point.X = curveX.Evaluate(time);
        //    point.Y = curveY.Evaluate(time);
        //    return point;
        //}

        public class Curve2D
        {
            Curve _curveX;
            Curve _curveY;

            float _length = 0f;

            public float Length => _length;

            public Curve2D()
            {
                _curveX = new Curve();
                _curveY = new Curve();
            }
            public void ComputeTangents(CurveTangent curveTangent) 
            {
                _curveX.ComputeTangents(curveTangent);
                _curveY.ComputeTangents(curveTangent);

                _curveX.PreLoop = _curveY.PreLoop = CurveLoopType.Constant;
            }

            public float KeyDistance(int index)
            {
                if (index >= 0 && index < _curveX.Keys.Count - 1)
                    return _curveX.Keys[index].Position;

                return 0;
            }

            public void AddKey(float x, float y)
            {
                #region Method Constant Space 
                Vector2 point = new Vector2(x, y);
                Vector2 prevPoint = new Vector2(x, y);
                Vector2 position = new Vector2();
                Vector2 prePosition = new Vector2();

                int prevIndex = 0;

                if (_curveX.Keys.Count > 1)
                    prevIndex = _curveX.Keys.Count - 1;

                if (_curveX.Keys.Count > 0 && _curveY.Keys.Count > 0)
                    if (_curveX.Keys[prevIndex] != null && _curveY.Keys[prevIndex] != null)
                    {
                        prevPoint.X = _curveX.Keys[prevIndex].Value;
                        prevPoint.Y = _curveY.Keys[prevIndex].Value;

                        prePosition.X = _curveX.Keys[prevIndex].Position;
                        prePosition.Y = _curveY.Keys[prevIndex].Position;

                    }

                float distance = (float)Geo.GetDistance(prevPoint, point);

                position.X = prePosition.X + distance;
                position.Y = prePosition.Y + distance;

                _curveX.Keys.Add(new CurveKey(position.X, x));
                _curveY.Keys.Add(new CurveKey(position.Y, y));

                _length += distance;
                #endregion

                #region Method Proportional Space
                //_curveX.Keys.Add(new CurveKey(_curveX.Keys.Count, x));
                //_curveY.Keys.Add(new CurveKey(_curveY.Keys.Count, y));
                //_length = _curveX.Keys.Count;
                #endregion
            }
            public Vector2 Point(float time)
            {
                Vector2 point = new Vector2();
                point.X = _curveX.Evaluate(time);
                point.Y = _curveY.Evaluate(time);
                return point;
            }

        }

        Curve2D _curve2D = new Curve2D();

        public ScreenMap(ContentManager content)
        {
            _naviGate = new NaviGate(this);

            this["quit"] = new Gui.Button(Input._mouse)
                .AppendTo(this)
                .SetSize(80, 32)
                .SetPivot(Position.LEFT_CENTER)
                .SetPosition(16, Game1._screenH - 32)
                .This<Gui.Button>().SetLabel("Quit")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        //    this["box"].This<MessageBox>().ToggleShow();
                        //Screen.GoTo(_root["ScreenTitle"].Init(), new Transition.FadeInOut().Init());
                        Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            this["option"] = new Gui.Button(Input._mouse).This<Gui.Button>() //.SetFocusable(false)
                .AppendTo(this)
                .SetSize(8 * 20, 8 * 4)
                .SetPivot(Position.CENTER)
                .SetPosition(Game1._screenW - 4 * 20 - 16, 32)
                .This<Gui.Button>().SetLabel("OPTIONS")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        _root["box"].This<DialBox>().ToggleShow(this);
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            this["level"] = new Gui.Button(Input._mouse)
                .AppendTo(this)
                .SetSize(48, 48)
                .SetPivot(Position.CENTER)
                .SetPosition(470, 875)
                .This<Gui.Button>().SetLabel("1")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        //    this["box"].This<MessageBox>().ToggleShow();
                        //Screen.GoTo(_root["ScreenPlay"].Init(), new Transition.FadeInOut().Init());
                        this["box"].This<MessageBox>().ToggleShow(this);
                        
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Medium)
                .Get<Addon.Draggable>().SetDraggable(false);

            //this["save"].This<Gui.Base>().SetStyle(this["save"].This<Gui.Base>().Style.Clone());
            //this["save"].This<Gui.Base>().Style._font = Game1._font_Medium;

            this["level"].Clone()
                .AppendTo(this)
                .This<Gui.Button>().SetLabel("2")
                .SetPosition(540, 660)
                ;

            this["level"].Clone()
                .AppendTo(this)
                .This<Gui.Button>().SetLabel("3")
                .SetPosition(800, 620)
                ;

            this["level"].Clone()
                .AppendTo(this)
                .This<Gui.Button>().SetLabel("4")
                .SetPosition(920, 320)
                ;

            _curve2D.AddKey(470, 875);
            _curve2D.AddKey(540, 660);
            _curve2D.AddKey(800, 620);
            _curve2D.AddKey(920, 320);

            _curve2D.ComputeTangents(CurveTangent.Smooth);

            //curveX.Keys.Add(new CurveKey(0, 470));
            //curveX.Keys.Add(new CurveKey(1, 540));
            //curveX.Keys.Add(new CurveKey(2, 800));
            //curveX.Keys.Add(new CurveKey(3, 920));

            //curveY.Keys.Add(new CurveKey(0, 875));
            //curveY.Keys.Add(new CurveKey(1, 660));
            //curveY.Keys.Add(new CurveKey(2, 620));
            //curveY.Keys.Add(new CurveKey(3, 320));

            //curveX.ComputeTangents(CurveTangent.Smooth);
            //curveY.ComputeTangents(CurveTangent.Smooth);

            //curveX.PostLoop = CurveLoopType.Constant;
            //curveY.PostLoop = CurveLoopType.Constant;


            #region Box Mission Start
            this["box"] = new MessageBox(Input._mouse, 0, 60, 20)
                .AppendTo(this)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(800, 600)
                //.SetPivot(Position.CENTER)
                .This<MessageBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_SHOW")
                    {
                        //System.Console.WriteLine("Start BOX ON_SHOW");
                        //_isPause = true;
                        _isGotoPlay = false;
                    }
                    if (m._message == "OFF_SHOW")
                    {
                        //System.Console.WriteLine("Mission OFF SHOW");
                        //if (this["pauseMenu"].This<DialBox>().IsShow()) this["pauseMenu"].This<DialBox>()._naviGate.SetNaviGate(false);

                    }

                    if (m._message == "OFF_HIDE")
                    {
                        if (_isGotoPlay)
                            Screen.GoTo(Game1._naviState.PushState(_root["ScreenPlay"].Init()), new Transition.FadeInOut().Init());
                        //System.Console.WriteLine("Mission OFF HIDE");
                        //if (this["pauseMenu"].This<DialBox>().IsShow()) this["pauseMenu"].This<DialBox>()._naviGate.SetNaviGate(true);
                    }
                });

            this["box"].SetPosition(Game1._screenW / 2 + this["box"]._rect.Width / 2, Game1._screenH / 2 + this["box"]._rect.Height / 2);
            this["box"].Init();

            // Title
            new Gui.Base(Input._mouse)
                .AppendTo(this["box"])
                .SetSize(200, 32)
                .SetPivot(Position.CENTER)
                .SetPosition(Position.TOP_CENTER)
                .This<Gui.Base>().SetLabel("Mission")
                .This<Gui.Base>().SetStyle(Game1._style_Medium);

            #region Button Accept Decline
            new Gui.Button(Input._mouse)
                .AppendTo(this["box"])
                .SetSize(80, 24)
                .SetPivot(Position.CENTER)
                .SetX(this["box"]._rect.Width/2 - 60)
                .SetY(this["box"]._rect.Height)
                .This<Gui.Button>().SetLabel("Accept")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                        _isGotoPlay = true;

                        System.Console.WriteLine("Accept Mission");
                        Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this["box"]);

                        
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Button(Input._mouse)
                .AppendTo(this["box"])
                .SetSize(80, 24)
                .SetPivot(Position.CENTER)
                .SetX(this["box"]._rect.Width / 2 + 60)
                .SetY(this["box"]._rect.Height)
                .This<Gui.Button>().SetLabel("Decline")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                        System.Console.WriteLine("Accept Mission");
                        Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this["box"]);

                        //this["box"].This<MessageBox>().ToggleShow(this);
                        //Screen.GoTo(Game1._naviState.PushState(_root["ScreenPlay"].Init()), new Transition.FadeInOut().Init());
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);
            #endregion

            var _tex_miniMap = content.Load<Texture2D>("Image/level_one");

            Rectangle destRect = new Rectangle(0, 0, (int)(_tex_miniMap.Width * .75f), (int)(_tex_miniMap.Height *.75f));

            new Gui.Image(Input._mouse, _tex_miniMap, Color.White)
                .AppendTo(this["box"])
                .SetSize(destRect.Width, destRect.Height)
                .SetPivot(Position.TOP_LEFT)
                .This<Gui.Base>().ShowSkin(false)
                .SetX(20)
                .SetY(40);
                

            new Gui.Text(Input._mouse)
                .AppendTo(this["box"])
                //.SetSize(80, 8)
                //.SetPivot(Position.TOP_LEFT)
                .SetX(this["box"]._rect.Width/2 + 120)
                .SetY(120)
                .This<Gui.Base>().SetFocusable(false)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .This<Gui.Base>().SetLabel
                (
                    "---= MISSION OBJECTIVE =---\n" +
                    "\n" +
                    "\n" +
                    "\n- Defend the portal." +
                    "\n" +
                    "\n- Kill all enemies." +
                    "\n" +
                    "\n- Survive the 25 enemy waves."
                );

            float posSlidersX = this["box"]._rect.Width / 2;

            _sliderDifficulty = new Gui.Slider(Input._mouse, null, Game1._listDifficulty.Count - 1);
            _sliderDifficulty
                .AppendTo(this["box"])
                .SetSize(320, 32)
                .SetPivot(Position.CENTER)
                .This<Gui.Base>().SetLabelTop("Difficulty", -16)
                .This<Gui.Base>().SetLabelBottom("", 16)
                .This<Gui.Base>().ShowSkin(true)
                .This<Gui.Base>().ShowOver(false)
                //.This<Gui.Base>()._resize.SetResizable(true)
                .SetPosition(posSlidersX, 460)
                //.Get<Addon.Draggable>().SetDraggable(true)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .This<Gui.Slider>().OnMessage((m) =>
                {
                    //if (m._message == "ON_CLICK")
                    //{
                    //    Console.WriteLine("Cursor => " + m._message);
                    //    Game1._sound_Clock.Play(.1f, .001f, 0f);
                    //}
                    if (m._message == "ON_CHANGE")
                    {
                        Console.WriteLine($"Cursor => {m._message} : {_sliderDifficulty.Index}");
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                        _sliderDifficulty._labelBottom = Game1._listDifficulty[_sliderDifficulty.Index];
                    }
                });

            _sliderDifficulty.This<Gui.Slider>().SetValue(1);
            _sliderDifficulty.This<Gui.Slider>().Cursor.ShowSkin(false);
            //_sliderDifficulty.This<Gui.Base>().Style._color = Style.ColorValue.MakeColor(Color.DarkGray);
            _sliderDifficulty.This<Gui.Base>().Style._backgroundColor = Style.ColorValue.MakeColor(Color.Black * .4f);
            _sliderDifficulty.This<Gui.Base>().Style._overColor = Style.ColorValue.MakeColor(Color.Yellow);
            _sliderDifficulty.This<Gui.Base>().Style._borderColor = Style.ColorValue.MakeColor(Color.Gray);

            #endregion

        }

        public override Node Init()
        {
            //InitChilds();

            _naviGate.SetNaviGate(true);
            _naviGate.SetNaviNodeFocusAt(0);

            _isGotoPlay = false;

            return base.Init();
        }

        public override Node Update(GameTime gameTime)
        {
            //Input._mouse.Update(Game1._relMouseX, Game1._relMouseY, (Game1._mouseState.LeftButton == ButtonState.Pressed) ? 1 : 0);

            UpdateChilds(gameTime);

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            batch.Draw(Game1._tex_WorldMap, Vector2.Zero, Color.White);
            Draw.FillRectangle(batch, new Rectangle(0, 0, Game1._screenW, Game1._screenH), Color.DarkBlue * .4f);


            //if (GroupOf("level").Count > 0)
            //{
            //    Vector2 prevPoint = GroupOf("level")[0].XY;

            //    foreach(var node in GroupOf("level"))
            //    {
            //        Draw.Line(batch, prevPoint, node.XY, Color.Cyan, 3);
            //        Draw.Line(batch, prevPoint, node.XY, Color.DarkCyan * .8f, 5);
            //        Draw.Line(batch, prevPoint, node.XY, Color.DarkCyan * .4f, 7);
            //        prevPoint = node.XY;
            //    }
            //}

            //Vector2 prevPoint = GetPointOnCurve(0);

            //for (float i=0f; i<=3f; i+= .1f )
            //{
            //    Vector2 currentPoint = GetPointOnCurve(i);

            //    Draw.Line(batch, prevPoint, currentPoint, Color.Black * .6f, 5);
            //    Draw.Line(batch, prevPoint, currentPoint, Color.Black * .4f, 7);
            //    Draw.Line(batch, prevPoint, currentPoint, Color.Black * .2f, 9);

            //    prevPoint = currentPoint;

            //}

            for (float i = 0f; i <= _curve2D.Length; i += _curve2D.KeyDistance(1)/8f)
                Draw.Point(batch, _curve2D.Point(i), 5, i <= _curve2D.KeyDistance(1) ? Color.Orange * .4f : Color.Red * .4f);
                //Draw.Point(batch, GetPointOnCurve(i), 5, Color.Orange * .8f);

            //Draw.Line(batch, Vector2.Zero, this["box"].AbsXY, Color.Yellow, 8f);
            //Draw.String(batch, Game1._font_Medium, $"Start Box  : {this["box"].This<MessageBox>()._x} , {this["box"].This<MessageBox>()._y}", Game1._screenW / 2, 20, Color.Yellow, Style.HorizontalAlign.Center, Style.VerticalAlign.Top);
            //Draw.String(batch, Game1._font_Medium, $"Start Box  : {this["box"].This<MessageBox>()._xOrigin} , {this["box"].This<MessageBox>()._yOrigin}", Game1._screenW / 2, 40, Color.Yellow, Style.HorizontalAlign.Center, Style.VerticalAlign.Top);

            RenderChilds(batch);

            return base.Render(batch);
        }

    }
}
