﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Retro2D;
using System;
using System.Globalization;
using TowerDefenseMono.UI;

namespace TowerDefenseMono
{
    public class ScreenPlay : Node
    {
        #region Attributes

        Level _nodeLevel;

        bool _isPause = false; // ScreenPlay Pause !
        //private bool _activePause = false;
        bool _onPause = false; // Trigger for save mouse level position before pause
        bool _offPause = false; // Trigger for save mouse level position before pause

        bool _onGameOver = false;
        bool _isGameOver = false;

        bool _onGameWin = false;
        bool _isGameWin = false;

        bool _isQuitLevel = false;
        bool _isRestartLevel = false;

        //bool _isStartUp = true; // is first start of the level !
        bool _onStartUp = true; // on first start of the level !

        MessageBox _levelStartupBox; // Message Box at each level startup !

        Node _topLayer;

        #endregion

        #region PATH & MAP Edition
        //void SavePath(string fileName)
        //{
        //    FileIO.XmlSerialization.WriteToXmlFile(fileName, _listPath);
        //    System.Console.WriteLine("Save All Paths to : " + fileName);
        //}

        //void LoadPath(string fileName)
        //{
        //    System.Console.WriteLine("Load All Paths from : " + fileName);
        //    _listPath = FileIO.XmlSerialization.ReadFromXmlFile<List<Gfx.Path>>(fileName);

        //    // Reconnect points by line to keep same references point !!!
        //    foreach (Gfx.Path path in _listPath)
        //        path.ReconnectPointLine(); 
        //}

        //void CopyBuildingPosition() //Vector2 buildablePos, Vector2 defRallyPoint)
        //{
        //    //POS_BUILDABLE = buildablePos;
        //    //POS_DEF_RALLY_POINT = defRallyPoint;

        //    string clipboardText= "new Buildable(Game1._player, Input._mouse, _layerUI, new Vector2(" + POS_BUILDABLE.X+","+POS_BUILDABLE.Y+"), new Vector2("+POS_DEF_RALLY_POINT.X+","+POS_DEF_RALLY_POINT.Y+")).AppendTo(_layerGame);";

        //    new Buildable(_player, Input._mouse, _layerUI, POS_BUILDABLE, POS_BUILDABLE).AppendTo(_layerGame);

        //    Clipboard.SetText(clipboardText);
        //}

        #endregion

        public ScreenPlay(ContentManager content)
        {
            _naviGate = new NaviGate(this);
            _nodeLevel = new Level(content);

            _animate.Add("fadeIn", Easing.Linear, 0f, 1f, 24f);
            _animate.Add("fadeOut", Easing.Linear, 1f, 0f, 24f);

            _topLayer = new Node();
            _topLayer._naviGate = new NaviGate(_topLayer);
            //_topLayer._naviGate.SetNaviGate(true);

            // Box Creation !
            #region Box Mission Start
            this["box"] = new MessageBox(Input._mouse, 0, 60, 12)
                .AppendTo(_topLayer)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(360, 320)
                .This<MessageBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_SHOW")
                    {
                        System.Console.WriteLine("Start BOX ON_SHOW");
                        _isPause = true;
                    }
                    if (m._message == "OFF_SHOW")
                    {
                        System.Console.WriteLine("Mission OFF SHOW");
                        //if (this["pauseMenu"].This<DialBox>().IsShow()) this["pauseMenu"].This<DialBox>()._naviGate.SetNaviGate(false);

                    }

                    if (m._message == "OFF_HIDE")
                    {
                        System.Console.WriteLine("Mission OFF HIDE");
                        //if (this["pauseMenu"].This<DialBox>().IsShow()) this["pauseMenu"].This<DialBox>()._naviGate.SetNaviGate(true);
                    }
                });

            // Title
            new Gui.Base(Input._mouse)
                .AppendTo(this["box"])
                .SetSize(200, 32)
                .SetPivot(Position.CENTER)
                .SetPosition(Position.TOP_CENTER)
                .This<Gui.Base>().SetLabel("Mission")
                .This<Gui.Base>().SetStyle(Game1._style_Medium);

            new Gui.Button(Input._mouse)
                .AppendTo(this["box"])
                .SetSize(160, 24)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(this["box"]._rect.Height - 32)
                .This<Gui.Button>().SetLabel("Accept Mission")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                        System.Console.WriteLine("Accept Mission");
                        Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this["box"]);

                        if (!this["pauseMenu"].This<DialBox>().IsShow())
                            _isPause = false;

                        //_offPause = true;
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Text(Input._mouse)
                .AppendTo(this["box"])
                .SetSize(80, 8)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(80)
                .This<Gui.Base>().SetFocusable(false)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .This<Gui.Base>().SetLabel
                (
                    "\n" +
                    "\n" +
                    "\n" +
                    "\n- Defend the portal." +
                    "\n" +
                    "\n- Kill all enemies." +
                    "\n" +
                    "\n- Survive the 25 enemy waves."
                );
            #endregion

            #region DialBox PauseMenu
            this["pauseMenu"] = new DialBox(Input._mouse, Game1._screenH / 2, Game1._screenH + 320)
                .AppendTo(this)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(400, 400)
                .SetPivot(Position.CENTER)
                .SetX(Game1._screenW / 2)
                .SetY(Game1._screenH + 640)
                .Init()
                .This<DialBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_SHOW")
                    {
                        //MediaPlayer.Volume = 0.02f;

                        //System.Console.WriteLine("_isPause = true");
                        _isPause = true;
                        _onPause = true;

                    }
                    if (m._message == "ON_HIDE")
                    {
                        //MediaPlayer.Volume = 0.02f;

                        //System.Console.WriteLine("_isPause = true");
                        _offPause = true;

                    }
                    if (m._message == "OFF_HIDE") 
                    {
                        //MediaPlayer.Volume = 0.04f;

                        if (!this["box"].This<MessageBox>().IsShow())
                            _isPause = false;

                        if (_isQuitLevel) // Quit current level !!
                        {
                            _isQuitLevel = false;
                            //Screen.GoTo(_root["ScreenMap"].Init(), new Transition.FadeInOut().Init());
                            Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());

                            MediaPlayer.Play(Game1._song_Music1);
                            MediaPlayer.Volume = Game1._sliderMusicVolume.Value * 0.24f;
                            MediaPlayer.IsRepeating = true;

                        }

                        if (_isRestartLevel) // Restart level !
                        {
                            _isRestartLevel = false;
                            _onStartUp = true;

                            Init();

                            System.Console.WriteLine(" Restart Level");
                        }
                    }

                });

            // Title
            new Gui.Button(Input._mouse)
                .AppendTo(this["pauseMenu"])
                .SetSize(240, 32)
                .SetPivot(Position.CENTER)
                .SetPosition(Position.TOP_CENTER)
                .This<Gui.Base>().SetLabel("PAUSE MENU")
                .This<Gui.Base>().OnMessage((m) =>
                {
                    if (m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        this["pauseMenu"].This<DialBox>().ToggleShow(this);
                    }
                })
                .This<Gui.Base>().SetStyle(Game1._style_Medium);


            Vector2 buttonSize = new Vector2(176, 24);

            new Gui.Button(Input._mouse)
                .AppendTo(this["pauseMenu"])
                .SetSize(buttonSize)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(80)
                .This<Gui.Button>().SetLabel("Mission")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        _levelStartupBox?.ToggleShow(this["pauseMenu"]);
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Button(Input._mouse)
                .AppendTo(this["pauseMenu"])
                .SetSize(buttonSize)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(130)
                .This<Gui.Button>().SetLabel("Options")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK") 
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        //Retro2D.Screen.GoTo(Game1._naviState.PushState(_root["Options"]), new Transition.FadeInOut().Init());
                        _root["box"].This<DialBox>().ToggleShow(this["pauseMenu"]);
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Button(Input._mouse)
                .AppendTo(this["pauseMenu"])
                .SetSize(buttonSize)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(180)
                .This<Gui.Button>().SetLabel("Quit Mission")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        this["pauseMenu"].This<DialBox>().ToggleShow(this);
                        _isQuitLevel = true;
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Button(Input._mouse)
                .AppendTo(this["pauseMenu"])
                .SetSize(buttonSize)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(230)
                .This<Gui.Button>().SetLabel("Restart")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        this["pauseMenu"].This<DialBox>().ToggleShow(this);

                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                        _isRestartLevel = true;

                        //Init();

                        //_onStartUp = true;
                        //_level.Init();
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Button(Input._mouse)
                .AppendTo(this["pauseMenu"])
                .SetSize(buttonSize.X, buttonSize.Y)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(this["pauseMenu"]._rect.Height)
                .This<Gui.Button>().SetLabel("Resume")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {

                        this["pauseMenu"].This<DialBox>().ToggleShow(this);
                        //System.Console.WriteLine("dialbox OK");
                        //Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this["pauseMenu"]);

                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .This<Gui.Button>().SetAsNaviNodeFocus() // Get the focus here !!
                .Get<Addon.Draggable>().SetDraggable(false);
            #endregion


            #region DialBox GameOver
            this["gameOver"] = new MessageBox(Input._mouse)
                .AppendTo(_topLayer)
                .SetPosition(Game1._screenW / 2 + 240, Game1._screenH / 2 + 90)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(480, 192)
                .SetPivot(Position.CENTER)
                //.SetX(Game1._screenW / 2)
                //.SetY(720)
                .Init()
                .This<MessageBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_SHOW")
                    {
                        _onPause = true;
                        //System.Console.WriteLine("_isPause = true");
                        _isPause = true;
                    }

                    if (m._message == "OFF_HIDE")
                    {
                        _offPause = true;
                        //System.Console.WriteLine("_isPause = false");
                        _isPause = false;

                        if (_isQuitLevel) // Quit current level !!
                        {
                            _isQuitLevel = false;

                            //Screen.GoTo(_root["ScreenMap"].Init(), new Transition.FadeInOut().Init());
                            Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());

                            //_nodeLevel = null;

                            //MediaPlayer.Play(Game1._song_intro); // restart menu music !
                            //    MediaPlayer.Volume = 0.2f;
                            //MediaPlayer.IsRepeating = true;

                            //Retro2D.Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                        }

                        if (_isRestartLevel) // Restart level !
                        {
                            _onStartUp = true;

                            Init();

                            _isRestartLevel = false;
                            System.Console.WriteLine(" Restart Level");
                        }
                    }

                });

            new Gui.Button(Input._mouse)
                .AppendTo(this["gameOver"])
                .SetSize(80, 16)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(130)
                .This<Gui.Button>().SetLabel("Quit")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        this["gameOver"].This<MessageBox>().ToggleShow(this);

                        _isQuitLevel = true;
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Button(Input._mouse)
                .AppendTo(this["gameOver"])
                .SetSize(80, 16)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(170)
                .This<Gui.Button>().SetLabel("Retry")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        this["gameOver"].This<MessageBox>().ToggleShow(this);

                        _isRestartLevel = true;
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            //new Gui.Label(Game1._nodeGui, true)
            //    .AppendTo(this["gameOver"])
            //    .SetSize(80, 8)
            //    .SetPivot(Position.CENTER)
            //    .SetX(Position.CENTER)
            //    .SetY(40)
            //    .This<Gui.Label>().SetStyle(Game1._style_main)
            //    .This<Gui.Label>().SetLabel("GAME OVER");

            new Gui.Image(Input._mouse, Game1._tex_gameOver, Color.White, new Rectangle(0, 0, 440, 80))
                .AppendTo(this["gameOver"])
                .This<Gui.Base>().ShowSkin(false)
                .SetSize(440, 80)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(64)
                .This<Gui.Base>().SetFocusable(false);

            #endregion

            #region DialBox GameWin
            this["gameWin"] = new MessageBox(Input._mouse)
                .AppendTo(_topLayer)
                .SetPosition(Game1._screenW / 2 + 240, Game1._screenH / 2 + 90)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(480, 320)
                .SetPivot(Position.CENTER)
                //.SetX(Game1._screenW / 2)
                //.SetY(720)
                .Init()
                .This<MessageBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_SHOW")
                    {
                        _onPause = true;
                        //System.Console.WriteLine("_isPause = true");
                        _isPause = true;
                    }

                    if (m._message == "OFF_HIDE")
                    {
                        _offPause = true;
                        //System.Console.WriteLine("_isPause = false");
                        _isPause = false;

                        if (_isQuitLevel) // Quit current level !!
                        {
                            _isQuitLevel = false;

                            //Screen.GoTo(_root["ScreenMap"].Init(), new Transition.FadeInOut().Init());
                            Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());

                            //_nodeLevel = null; 

                            //MediaPlayer.Play(Game1._song_intro); // restart menu music !
                            //    MediaPlayer.Volume = 0.2f;
                            //MediaPlayer.IsRepeating = true;

                            //Retro2D.Screen.GoTo(Game1._naviState.PopState(), new Transition.FadeInOut().Init());
                        }

                        if (_isRestartLevel) // Restart level !
                        {
                            _onStartUp = true;

                            Init();

                            _isRestartLevel = false;

                            System.Console.WriteLine(" Restart Level");
                        }
                    }

                });

            new Gui.Button(Input._mouse)
                .AppendTo(this["gameWin"])
                .SetSize(80, 16)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(240)
                .This<Gui.Button>().SetLabel("Quit")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        this["gameWin"].This<MessageBox>().ToggleShow(this);
                        
                        _isQuitLevel = true;
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Button(Input._mouse)
                .AppendTo(this["gameWin"])
                .SetSize(80, 16)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(280)
                .This<Gui.Button>().SetLabel("Restart")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        this["gameWin"].This<MessageBox>().ToggleShow(this);

                        _isRestartLevel = true;
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            new Gui.Image(Input._mouse, Game1._tex_gameOver, Color.White, new Rectangle(0, 80, 440, 80))
                .AppendTo(this["gameWin"])
                .This<Gui.Base>().ShowSkin(false)
                .SetSize(440, 80)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(64)
                .This<Gui.Base>().SetFocusable(false);

            this["gameWinLife"] = new Gui.Text(Input._mouse)
                .AppendTo(this["gameWin"])
                .SetSize(80, 8)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(140)
                .This<Gui.Base>().SetFocusable(false)
                .This<Gui.Base>().SetStyle(new Style { _font = Game1._font_Big, _color = { _value = Color.Gold }, _colorTextBorder = { _value = Color.DarkRed }, _textBorder = true,_horizontalAlign = Style.HorizontalAlign.Center })
                .This<Gui.Base>().SetLabel("MISSION COMPLETE");

            this["gameWinInfo"] = new Gui.Text(Input._mouse)
                .AppendTo(this["gameWin"])
                .SetSize(80, 8)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(180)
                .This<Gui.Base>().SetFocusable(false)
                .This<Gui.Base>().SetStyle(new Style { _font = Game1._font_Main, _color = { _value = Color.YellowGreen }, _colorTextBorder = { _value = Color.Black }, _textBorder = true, _horizontalAlign = Style.HorizontalAlign.Center } )
                .This<Gui.Base>().SetLabel("MISSION COMPLETE");

            #endregion


            _levelStartupBox = (MessageBox)this["box"];
            _levelStartupBox.SetPosition(Game1._screenW / 2 + _levelStartupBox._rect.Width / 2, Game1._screenH / 2 + _levelStartupBox._rect.Height / 2);
            _levelStartupBox.Init();

        }

        public void ToggleShowPauseMenu()
        {
            this["pauseMenu"].This<DialBox>().ToggleShow(this);
        }
        public override Node Init()
        {
            _nodeLevel.Init();
            //InitChilds();
            //_naviGate.SetNaviGate(true);
            //_naviGate.SetNaviNodeFocusAt(0);

            _onStartUp = true;

            //_animate.Start("fadeOut");
            return base.Init();
        }
        public override Node Update(GameTime gameTime)
        {
            // Manage : GameWin & GameOver
            if (null != _nodeLevel)
            {
                if (_nodeLevel.This<Level>().IsWin) _isGameWin = true;
                if (_nodeLevel.This<Level>().IsLose) _isGameOver = true;
            }

            // Game Over Trigger !
            if (!_isGameOver) _onGameOver = false;
            if (_isGameOver && !_onGameOver)
            {
                _onGameOver = true;

                System.Console.WriteLine("On Game Over !!");
                this["gameOver"].This<MessageBox>().ToggleShow(this);

                Game1._sound_lose.Play(.4f * Game1._volumeSound, .1f, 0);
            }

            // Game Win Trigger !
            if (!_isGameWin) _onGameWin = false;
            if (_isGameWin && !_onGameWin)
            {
                _onGameWin = true;

                System.Console.WriteLine("On Game Win !!");

                string winMessage = "CONGRATULATION";

                if (_nodeLevel._player.Life > 19) winMessage = "Awesome, God is You ?!";
                if (_nodeLevel._player.Life <= 19) winMessage = "Congratulation, near Perfection !";
                if (_nodeLevel._player.Life <= 18) winMessage = "Not bad, you are a PRO!";
                if (_nodeLevel._player.Life <= 15) winMessage = "You know it's possible !";
                if (_nodeLevel._player.Life <= 10) winMessage = "Yes you can, keep going !";
                if (_nodeLevel._player.Life <= 5) winMessage = "Hum, You need more pratice !";

                string gameWinLife = _nodeLevel._player.Life + " / " + Player.MAX_LIFE;

                this["gameWinLife"].This<Gui.Base>().SetLabel(gameWinLife);

                string gameWinLabel =
                    "\n" +
                    "\n " + winMessage +
                    "\n" 
                ;

                this["gameWinInfo"].This<Gui.Base>().SetLabel(gameWinLabel);

                this["gameWin"].This<MessageBox>().ToggleShow(this);

                Game1._sound_win.Play(.4f * Game1._volumeSound, .001f, 0);
            }

            #region Debug

            //if (Input.Button.OnePress("ShowWin", Keyboard.GetState().IsKeyDown(Keys.K))) _isGameWin = true; // this["gameWin"].This<MessageBox>().ToggleShow();
            //if (Input.Button.OnePress("ShowLose", Keyboard.GetState().IsKeyDown(Keys.L))) this["gameOver"].This<MessageBox>().ToggleShow();

            #endregion


            _onPause = false;
            _offPause = false;

            //Input._mouse.Update(Game1._relMouseX , Game1._relMouseY, (Game1._mouseState.LeftButton == ButtonState.Pressed) ? 1 : 0);

            //_levelStartupBox?.Update(gameTime);
            _topLayer.UpdateChilds(gameTime);

            UpdateChilds(gameTime);

            if (!_isPause)
            {
                SoundSystem.PrepareSound();
                _nodeLevel.Update(gameTime);
                SoundSystem.PlayAllSound();
            }

            if (_onPause)
            {
                Console.WriteLine(" ON PAUSE !");
                _animate.Start("fadeIn");
            }
            if (_offPause)
            {
                Console.WriteLine(" OFF PAUSE !");
                _animate.Start("fadeOut");
            }

            if (_onStartUp)// || Input.Button.OnePress("box", Keyboard.GetState().IsKeyDown(Keys.F8)) )
            {
                _isPause = true;

                if (!Screen.IsTransition())
                {
                    Console.WriteLine("Level StartUp");
                    _onStartUp = false;

                    _isGameOver = false; // Reset GameOver !
                    _isGameWin = false; // Reset GameWin !

                    _levelStartupBox?.ToggleShow(this);
                }
            }

            //if (_levelStartupBox._animate.OnEnd("hide") && !this["pauseMenu"].This<DialBox>().IsShow())
            //    _isPause = false;



            _animate.NextFrame();

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            _nodeLevel.Render(batch);

            // fade In Out effect when pause
            Draw.FillRectangle(batch, new Rectangle(0, 0, Game1._screenW, Game1._screenH), Color.Black * .6f * _animate.Value());
            //Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, 32, 32, Color.MidnightBlue * .6f * _animate.Value());

            RenderChilds(batch);

            //_levelStartupBox?.Render(batch);
            _topLayer.RenderChilds(batch);

            //Draw.String(batch, Game1._font_Main, "this['pauseMenu']._naviGate.IsNaviGate = " + this["pauseMenu"]._naviGate.IsNaviGate, 20, 200, Color.Gold, Style.HorizontalAlign.Left);

            return base.Render(batch);
        }
    }
}
