﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Retro2D;
using System;
using System.IO;

namespace TowerDefenseMono
{
    public class ScreenTitle : Node
    {
        bool _isGotoMap = false;

        public ScreenTitle(ContentManager content)
        {

            SetSize(Game1._screenW, Game1._screenH);

            _naviGate = new NaviGate(this);

            #region Main Menu

            this["start"] = new Gui.Button(Input._mouse)
                .AppendTo(this)
                .SetSize(8*20, 8*4)
                .SetPivot(Position.CENTER)
                .SetPosition(Game1._screenW / 2, 760)
                .This<Gui.Button>().SetLabel("START")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        //    this["box"].This<MessageBox>().ToggleShow();
                        //Screen.GoTo(_root["ScreenSave"].Init(), new Transition.FadeInOut().Init());
                        //Screen.GoTo(Game1._naviState.PushState(_root["ScreenSave"]), new Transition.FadeInOut().Init());

                        this["saveBox"].This<DialBox>().ToggleShow(this);
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            this["option"] = new Gui.Button(Input._mouse).This<Gui.Button>() //.SetFocusable(false)
                .AppendTo(this)
                .SetSize(8 * 20, 8 * 4)
                .SetPivot(Position.CENTER)
                .SetPosition(Game1._screenW / 2, 820)
                .This<Gui.Button>().SetLabel("OPTIONS")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        _root["box"].This<DialBox>().ToggleShow(this);
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            this["exit"] = new Gui.Button(Input._mouse)
                .AppendTo(this)
                .SetSize(8*20, 8*4)
                .SetPivot(Position.CENTER)
                .SetPosition(Game1._screenW / 2, 880)
                .This<Gui.Button>().SetLabel("EXIT")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        Game1.Quit();
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);

            #endregion

            #region Save Box

            this["saveBox"] = new DialBox(Input._mouse, Game1._screenH / 2 - 160, - 560 * 2, 28)
                .AppendTo(this)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(680, 560)
                .SetPivot(Position.CENTER)
                .SetX(Game1._screenW / 2)
                .SetY(Game1._screenH + 640)
                .Init()
                .This<DialBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_SHOW")
                    {
                        _isGotoMap = false;
                    }
                    if (m._message == "ON_HIDE")
                    {

                    }
                    if (m._message == "OFF_HIDE")
                    {
                        if (_isGotoMap)
                            Screen.GoTo(Game1._naviState.PushState(_root["ScreenMap"].Init()), new Transition.FadeInOut().Init());
                    }

                });

            // Title
            new Gui.Base(Input._mouse)
                .AppendTo(this["saveBox"])
                .SetSize(200, 32)
                .SetPivot(Position.CENTER)
                .SetPosition(Position.TOP_CENTER)
                .This<Gui.Base>().SetLabel("Game Save")
                .This<Gui.Base>().SetStyle(Game1._style_Medium);

            // Cancel
            new Gui.Button(Input._mouse)
                .AppendTo(this["saveBox"])
                .SetSize(120, 32)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(this["saveBox"]._rect.Height - 32)
                .This<Gui.Button>().SetLabel("Cancel")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {

                        this["saveBox"].This<DialBox>().ToggleShow(this);
                        //System.Console.WriteLine("dialbox OK");
                        //Game1._messageQueue.Post(0, new Data { _msg = "OK" }, this["pauseMenu"]);

                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                    }

                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .This<Gui.Button>().SetAsNaviNodeFocus() // Get the focus here !!
                .Get<Addon.Draggable>().SetDraggable(false);


            // Game Save Buttons
            float posX = this["saveBox"]._rect.Width / 2;

            this["save"] = new Gui.Button(Input._mouse)
                .AppendTo(this["saveBox"])
                .SetSize(560, 96)
                .SetPivot(Position.CENTER)
                .SetPosition(posX, 120)
                .This<Gui.Button>().SetLabel("SAVE 1")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        //    this["box"].This<MessageBox>().ToggleShow();
                        //Screen.GoTo(_root["ScreenMap"].Init(), new Transition.FadeInOut().Init());
                        _isGotoMap = true;
                        this["saveBox"].This<DialBox>().ToggleShow(this);
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main);


            //this["save"].This<Gui.Base>().SetStyle(this["save"].This<Gui.Base>().Style.Clone());
            //this["save"].This<Gui.Base>().Style._font = Game1._font_Medium;

            this["save"].Clone()
                .AppendTo(this["saveBox"])
                .This<Gui.Button>().SetLabel("SAVE 2")
                .SetY(270)
                .This<Gui.Button>().SetStyle(Game1._style_Main);

            this["save"].Clone()
                .AppendTo(this["saveBox"])
                .This<Gui.Button>().SetLabel("SAVE 3")
                .SetY(420)
                .This<Gui.Button>().SetStyle(Game1._style_Main);

            #endregion

        }
        public override Node Init()
        {
            InitChilds();

            _naviGate.SetNaviGate(true);
            _naviGate.SetNaviNodeFocusAt(0);

            _isGotoMap = false;

            MediaPlayer.Play(Game1._song_Music1);
            MediaPlayer.Volume = Game1._sliderMusicVolume.Value * 0.24f;
            MediaPlayer.IsRepeating = true;

            return base.Init();
        }
        public override Node Update(GameTime gameTime)
        {

            UpdateChilds(gameTime);

            //Game1.UpdateNaviGateButton(_naviGate);

            //_naviGate.Update(UID.Get<NodeGui>());
            //_naviGate.UpdateSubmitButton(Keyboard.GetState().IsKeyDown(Keys.Enter));

            //if (Input.Button.OnPress("quit"))
            //    this["quit"].This<Gui.Button>().RunMessageProc(new Gui.Message { _node = this, _message = "ON_PRESS" });


            //if (Input.Button.OnPress("up", Keyboard.GetState().IsKeyDown(Keys.Up))) _naviGate.ToPrevNaviNode(UID.Get<NodeGui>());
            //if (Input.Button.OnPress("down", Keyboard.GetState().IsKeyDown(Keys.Down))) _naviGate.ToNextNaviNode(UID.Get<NodeGui>());

            //// back button
            //Input.Button.OnPress("quit", Keyboard.GetState().IsKeyDown(Keys.Back));


            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            batch.GraphicsDevice.Clear(new Color(10,20,30));

            Draw.Grid(batch, 0, 0, Game1._screenW, Game1._screenH, 32, 32, Color.Gray * .1f, 1);
            Draw.CenterBorderedStringXY(batch, Game1._font_Medium, "Powered by Retro2D", Game1._screenW / 2, Game1._screenH / 2-24, Color.OrangeRed, Color.Black);
            Draw.CenterBorderedStringXY(batch, Game1._font_Mega, "TOWER DEFENSE MONO", Game1._screenW / 2, Game1._screenH / 2-64, Color.Gold, Color.DarkOrange);

            Draw.CenterBorderedStringXY(batch, Game1._font_Main, "(C)MugenStudio by TES Silver ", Game1._screenW / 2, Game1._screenH-32, Color.Orange, Color.Black);

            RenderChilds(batch);

            return base.Render(batch);
        }
    }
}
