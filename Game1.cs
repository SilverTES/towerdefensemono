﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
//using MonoGame.Extended;
using Retro2D;
using System;
using System.IO;
using System.Reflection;
using static Retro2D.Node;


namespace TowerDefenseMono
{
    public partial class Game1 : Game
    {
        Camera2d cam = new Camera2d();

        Sprite _sprite;
        Sprite _sprite2;

        Node _layerUI;

        public static ContentManager GameContent { get; set; }

        static string _workPath = @"P:\Dev\TowerDefenseMono\Content\";
        static string _buildPath = @"Content\";

        FileSystemWatcher _fileWatcherWork;

        public Game1()
        {
            // Setup Window
            _window.Setup(this, Mode.RETRO, "TowerDefenseMono", _screenW, _screenH, 2, 0, false, true, false);
            
            // Set Content Directory
            Content.RootDirectory = "Content";

            GameContent = Content;
            
            // Create Build Version
            DateTime buildDate = new FileInfo(Assembly.GetExecutingAssembly().Location).LastWriteTime;
            Console.WriteLine("Build Date :" + buildDate.ToString());
            VERSION = buildDate.ToString();
            
            // Store in file
            File.WriteAllText(_pathVersion, VERSION);
        }
        protected override void Initialize()
        {
            _fileWatcherWork = new FileSystemWatcher(_buildPath);
            _fileWatcherWork.EnableRaisingEvents = true;
            _fileWatcherWork.NotifyFilter = NotifyFilters.LastWrite;

            _fileWatcherWork.Changed += (sender, e) =>
            {
                _fileWatcherWork.EnableRaisingEvents = false;

                Console.WriteLine($"> COPY : {e.FullPath} -----> {_workPath + e.Name}");
                File.Copy(e.FullPath, _workPath + e.Name, true);


                // Debug Update Changed File
                if (Path.GetExtension(e.Name) == ".json")
                {
                    Console.WriteLine("NEED TO RELOAD GlobalContent.XML !");
                    ContentLoader.AddContent(GameContent, _buildPath + "GlobalContent.xml");

                    //ContentLoader.LogAll();
                    //StyleContainer.LogAll();
                }

                //Console.WriteLine("ImportNodeFromFile : "+ e.FullPath);
                ImportNodeFromFile(_root["ScreenTitle"], _buildPath + "GUI_title.xml");


                _fileWatcherWork.EnableRaisingEvents = true;



            };



            ContentLoader.AddContent(Content, _buildPath + "GlobalContent.xml");
            ContentLoader.LogAll();

            // Init Base
            base.Initialize();

            // Init Window
            _window.Init(_font_Main);
            _window.SetScale(2);
            _window.SetFinalScreenSize(_finalScreenW, _finalScreenH);



            #region Options MessageBox

            // Options Box !
            _layerUI = new Node()
                .SetSize(Game1._screenW, Game1._screenH);

            _layerUI._naviGate = new NaviGate(_layerUI);
            _layerUI._naviGate.SetNaviGate(true);


            int initPosY = Game1._screenH + 360;
            //_root["box"] = new MessageBox(Input._mouse, 0, 60, 12)
            _root["box"] = new DialBox(Input._mouse, Game1._screenH / 2, initPosY, 24)
                .AppendTo(_layerUI)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false)
                .SetSize(640, 640)
                .SetPivot(Position.CENTER)
                .SetX(Game1._screenW / 2)
                .SetY(initPosY)
                .This<Gui.Base>().OnMessage((m) =>
                {
                    if (m._message == "ON_SHOW")
                    {
                        Console.WriteLine("Start BOX ON_SHOW");
                    }

                    if (m._message == "OFF_SHOW")
                    {
                        //_root["ScreenTitle"]._naviGate.SetNaviGate(false);
                        //_layerUI._naviGate.MoveNaviGateTo(_root["box"]);
                    }

                    if (m._message == "OFF_HIDE")
                    {
                        //_root["ScreenTitle"]._naviGate.SetNaviGate(true);
                        //_layerUI._naviGate.BackNaviGate();

                        //_root["box"]._naviGate.SetNaviGate(true);
                    }

                });

            //_root["box"]._naviGate.SetNaviGate(true);

            // Title
            new Gui.Button(Input._mouse)
                .AppendTo(_root["box"])
                .SetSize(160, 32)
                .SetPivot(Position.CENTER)
                .SetPosition(Position.TOP_CENTER)
                .This<Gui.Base>().SetLabel("Options")
                .This<Gui.Base>().OnMessage((m) =>
                {
                    if (m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        _root["box"].This<DialBox>().ToggleShow(_layerUI);
                    }
                })
                .This<Gui.Base>().SetStyle(Game1._style_Medium);


            // Button OK
            new Gui.Button(Input._mouse)
                .AppendTo(_root["box"])
                .SetSize(80, 32)
                .SetPivot(Position.CENTER)
                .SetX(Position.CENTER)
                .SetY(_root["box"]._rect.Height - 32)
                .This<Gui.Button>().SetLabel("OK")
                .This<Gui.Button>().OnMessage((m) =>
                {
                    if (m._message == "ON_PRESS" || m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);

                        Console.WriteLine("Accept Mission");
                        Game1._messageQueue.Post(0, new Data { _msg = "OK" }, _root["box"]);

                        //if (!this["pauseMenu"].This<DialBox>().IsShow())
                        //    _isPause = false;

                        //_offPause = true;
                    }
                })
                .This<Gui.Button>().SetStyle(Game1._style_Main)
                .Get<Addon.Draggable>().SetDraggable(false);


            float posX = _root["box"]._rect.Width / 2;

            var skinButton = new Gui.Button(Input._mouse)
                .AppendTo(_root["box"])
                .SetY(100)
                .SetX(posX - 180)
                .SetSize(96, 24)
                .SetPivot(Position.CENTER)
                .This<Gui.Base>().SetLabel("Military")
                .This<Gui.Base>().ShowOver()
                .This<Gui.Base>().OnMessage((m) =>
                {
                    if (m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        _style_Main._skinGui = _style_Medium._skinGui = m._node.This<Gui.Base>()._style._skinGui;
                    }
                })
                .This<Gui.Base>().SetStyle(new Style(){ _textBorder = true, _skinGui = _skinGui_00, _color = { _value = Color.Cyan }, _overBackgroundFillColor = { _value = Color.Yellow } });

            skinButton.Clone()
                .AppendTo(_root["box"])
                .This<Gui.Base>().SetLabel("Futura")
                .SetX(posX - 60)
                .This<Gui.Base>().SetStyle(new Style() { _textBorder = true, _skinGui = _skinGui_01, _color = { _value = Color.Cyan }, _overBackgroundFillColor = { _value = Color.Yellow } });

            skinButton.Clone()
                .AppendTo(_root["box"])
                .This<Gui.Base>().SetLabel("GreenM")
                .SetX(posX + 60)
                .This<Gui.Base>().SetStyle(new Style() { _textBorder = true, _skinGui = _skinGui_02, _color = { _value = Color.Cyan }, _overBackgroundFillColor = { _value = Color.Yellow } });

            skinButton.Clone()
                .AppendTo(_root["box"])
                .This<Gui.Base>().SetLabel("SciFi")
                .SetX(posX + 180)
                .This<Gui.Base>().SetStyle(new Style() { _textBorder = true, _skinGui = _skinGui_03, _color = { _value = Color.Cyan }, _overBackgroundFillColor = { _value = Color.Yellow } });


            new Gui.CheckBox(Input._mouse)
                .AppendTo(_root["box"])
                .SetPosition(posX - 60, 160)
                .SetSize(24, 24)
                .SetPivot(Position.CENTER)
                .This<Gui.Base>().SetLabelRight("FullScreen", 16)
                .This<Gui.CheckBox>().OnMessage((m) =>
                {
                    if (m._message == "ON_CLICK")
                    {
                        Game1._sound_Clock.Play(.1f * Game1._volumeSound, .001f, 0f);
                        //_window.ToggleFullScreen(-1);
                    }

                    if (m._message == "ON_CHECK")
                        _window.SetFullScreen(true, 0);

                    if (m._message == "OFF_CHECK")
                        _window.SetFullScreen(false, 0);

                });

            var cursor = new Gui.Image(Input._mouse, Game1._tex_Tower0, Color.White);
            cursor.SetSize(32, 32);

            _sliderMusicVolume = new Gui.Slider(Input._mouse, null, -1);
            _sliderMusicVolume
                .AppendTo(_root["box"])
                .SetSize(400, 32)
                .SetPivot(Position.CENTER)
                .This<Gui.Base>().SetLabelTop("Music Volume", -16)
                .This<Gui.Base>().SetLabelBottom("", 16)
                .This<Gui.Base>().ShowSkin(true)
                .This<Gui.Base>().ShowOver(false)
                //.This<Gui.Base>()._resize.SetResizable(true)
                .SetPosition(posX, 260)
                //.Get<Addon.Draggable>().SetDraggable(true)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .This<Gui.Slider>().OnMessage((m) =>
                {
                    //if (m._message == "ON_CLICK")
                    //{
                    //    Console.WriteLine("Cursor => " + m._message);
                    //    Game1._sound_Clock.Play(.1f, .001f, 0f);
                    //}
                    //if (m._message == "ON_DRAG_CURSOR")
                    //{
                    //    Console.WriteLine($"Cursor => {m._message} : {_sliderMusicVolume.Index}");
                    //}
                    //if (m._message == "OFF_DRAG_CURSOR")
                    //{
                    //    Console.WriteLine($"Cursor => {m._message} : {_sliderMusicVolume.Index}");
                    //}

                    if (m._message == "ON_CHANGE")
                    {
                        Console.WriteLine($"Cursor => {m._message} : {_sliderMusicVolume.Index}");
                    }

                    if (m._message == "IS_CHANGE")
                    {
                        _sliderMusicVolume._labelBottom = ((int)_sliderMusicVolume.Percent).ToString() + "%";

                        MediaPlayer.Volume = _sliderMusicVolume.Value * 0.08f;
                    }
                });

            _sliderMusicVolume.This<Gui.Slider>().SetValue(.5f).Update(null);
            Console.WriteLine("MaxValue = " + _sliderMusicVolume.This<Gui.Slider>().MaxValue);

            _sliderMusicVolume.This<Gui.Slider>().Cursor.ShowSkin(false);
            //_sliderMusicVolume.This<Gui.Base>().Style._color = Style.ColorValue.MakeColor(Color.DarkGray);
            _sliderMusicVolume.This<Gui.Base>().Style._backgroundColor = Style.ColorValue.MakeColor(Color.Black * .4f);
            _sliderMusicVolume.This<Gui.Base>().Style._overColor = Style.ColorValue.MakeColor(Color.Yellow);
            _sliderMusicVolume.This<Gui.Base>().Style._borderColor = Style.ColorValue.MakeColor(Color.Gray);


            _sliderSoundVolume = new Gui.Slider(Input._mouse, null, -1);
            _sliderSoundVolume
                .AppendTo(_root["box"])
                .SetSize(400, 32)
                .SetPivot(Position.CENTER)
                .This<Gui.Base>().SetLabelTop("SFX Volume", -16)
                .This<Gui.Base>().SetLabelBottom("", 16)
                .This<Gui.Base>().ShowSkin(true)
                .This<Gui.Base>().ShowOver(false)
                //.This<Gui.Base>()._resize.SetResizable(true)
                .SetPosition(posX, 380)
                //.Get<Addon.Draggable>().SetDraggable(true)
                .This<Gui.Base>().SetStyle(Game1._style_Main)
                .This<Gui.Slider>().OnMessage((m) =>
                {
                    //if (m._message == "ON_CLICK")
                    //{
                    //    Console.WriteLine("Cursor => " + m._message);
                    //    Game1._sound_Clock.Play(.1f, .001f, 0f);
                    //}
                    if (m._message == "IS_CHANGE")
                    {
                        //Console.WriteLine($"Cursor => {m._message} : {_sliderSoundVolume.Index}");
                        _sliderSoundVolume._labelBottom = ((int)_sliderSoundVolume.Percent).ToString() + "%";

                        _volumeSound = _sliderSoundVolume.Value * 2f;
                    }
                });

            _sliderSoundVolume.This<Gui.Slider>().SetValue(.5f);
            _sliderSoundVolume.This<Gui.Slider>().Cursor.ShowSkin(false);
            //_sliderSoundVolume.This<Gui.Base>().Style._color = Style.ColorValue.MakeColor(Color.DarkGray);
            _sliderSoundVolume.This<Gui.Base>().Style._backgroundColor = Style.ColorValue.MakeColor(Color.Black * .4f);
            _sliderSoundVolume.This<Gui.Base>().Style._overColor = Style.ColorValue.MakeColor(Color.Yellow);
            _sliderSoundVolume.This<Gui.Base>().Style._borderColor = Style.ColorValue.MakeColor(Color.Gray);


            //_root["box"].SetPosition(Game1._screenW / 2 + _root["box"]._rect.Width / 2, Game1._screenH / 2 + _root["box"]._rect.Height / 2);
            //_root["box"].Init();


            #endregion



            // Create Screens Instance
            _root["ScreenPlay"] = new ScreenPlay(Content);
            _root["ScreenTitle"] = new ScreenTitle(Content).Init();
            _root["ScreenMap"] = new ScreenMap(Content).Init();

            // Start Screen
            //Screen.Init(_root["ScreenPlay"]);
            //Screen.Init(_root["ScreenTitle"]);
            //Screen.Init(_naviState.CreateNaviState(_root["ScreenPlay"])); // Screen where to Start 
            Screen.Init(_naviState.CreateNaviState(_root["ScreenTitle"])); // Screen where to Start 
            //Screen.Init(_naviState.CreateNaviState(_root["ScreenMap"])); // Screen where to Start 

            // Setup Mouse Cursor 
            IsMouseVisible = true;
            ResizeMouseCursor();

            //_sprite = new Sprite();
            //_sprite.Add(_anim_Marine_Idle);
            //_sprite.Add(_anim_Marine_Walk);
            //_sprite.Add(_anim_Marine_Fire);
            //_sprite.Add(_anim_Monster_Idle);
            //_sprite.Add(_anim_Monster_Walk);
            //_sprite.Add(_anim_Monster_Walk_Front);
            //_sprite.Start("Walk_Front", 1, 0);

            _sprite = _sprite_Monster0.Clone();
            _sprite2 = _sprite_Monster0.Clone();

            _sprite_Monster0.Start("Fight", 1, 0);
            _sprite.Start("Walk", 1, 0);
            _sprite2.Start("Idle", 1, 0);

            //Console.WriteLine("DirectoryName : "+ Path.GetDirectoryName("P:/Dev/TowerDefenseMono/Content/GUI_title.xml"));

            //Game1.ImportNodeFromFile(_root["ScreenTitle"], _buildPath + "GUI_title.xml");



            cam.Pos = new Vector2(_screenW/2, _screenH/2);
            // cam.Zoom = 2.0f; // Example of Zoom in
            cam.Zoom = 1f; // Example of Zoom out

        }
        protected override void Update(GameTime gameTime)
        {
            // Window Get Mouse State
            _window.GetMouse(ref _relMouseX, ref _relMouseY, ref _mouseState);

            Input._mouse.Update(Game1._relMouseX, Game1._relMouseY, (Game1._mouseState.LeftButton == ButtonState.Pressed) ? 1 : 0, Game1._mouseState.ScrollWheelValue);

            //_layerUI.Update(gameTime);
            _layerUI.UpdateChilds(gameTime);

            // Update Current Screen
            Screen.Update(gameTime);

            // Update Sound Systems
            UpdatePlaySound();

            // Shake Window if active Shake Effect
            if (_shake.IsShake)
                _window.SetOriginPosition((int)_shake.GetVector2().X, (int)_shake.GetVector2().Y);

            #region DEBUG

            if (Input.Button.OnPress("incSlider", Keyboard.GetState().IsKeyDown(Keys.Right)))
            {
                _sliderMusicVolume.Move(.01f);
            }
            if (Input.Button.OnPress("decSlider", Keyboard.GetState().IsKeyDown(Keys.Left)))
            {
                _sliderMusicVolume.Move(-.01f);
            }


            // Show Options Box
            if (Input.Button.OnePress("Options", Keyboard.GetState().IsKeyDown(Keys.F10))) _root["box"].This<DialBox>().ToggleShow(null);

            // Change Skin
            if (Input.Button.OnePress("skin00", Keyboard.GetState().IsKeyDown(Keys.F1))) _style_Main._skinGui = _style_Medium._skinGui = _skinGui_00;
            if (Input.Button.OnePress("skin01", Keyboard.GetState().IsKeyDown(Keys.F2))) _style_Main._skinGui = _style_Medium._skinGui = _skinGui_01;
            if (Input.Button.OnePress("skin02", Keyboard.GetState().IsKeyDown(Keys.F3))) _style_Main._skinGui = _style_Medium._skinGui = _skinGui_02;
            if (Input.Button.OnePress("skin02", Keyboard.GetState().IsKeyDown(Keys.F4))) _style_Main._skinGui = _style_Medium._skinGui = _skinGui_03;

            // Debug : Frame Counter
            _frameCounter.Update(gameTime);

            // Debug : standard window key control , size , quality etc. 
            _window.UpdateStdWindowControl();


            // Resize Mouse window change
            if (_window.OnWindowResize || _window.OnToggleFullScreen || _window.OnSwitchMonitor)
                ResizeMouseCursor();

            // Debug : Quit if press escape
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape) || _isQuit)
                Exit();

            // Debug : Display Window State
            Window.Title = "Starter FPS :" + _frameCounter.Fps() + " FinalScreen : " + _window.FinalScreenW + " x " + _window.FinalScreenH + " Zoom X " + _window.Scale + "  Ratio :" + (float)_window.ScreenW / (float)_window.FinalScreenW;
            #endregion

            _messageQueue.Dispatch();


            //Game1._sprite_Monster0.Update();
            //_sprite.Update();
            //_sprite2.Update();


            base.Update(gameTime);
        }
        protected override void Draw(GameTime gameTime)
        {
            // Set Target to MainRenderTarget !
            _window.SetRenderTarget(_window.NativeRenderTarget);
            _window.Batch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null, cam.get_transformation(_screenW, _screenH)); //, DepthStencilState.None, RasterizerState.CullCounterClockwise );

                // Draw something here !
                Screen.Render(_window.Batch);

                // Draw Options Box
                _layerUI.Render(_window.Batch);
                _layerUI.RenderChilds(_window.Batch);

            // Draw Version
            Retro2D.Draw.RightMiddleBorderedString(_window.Batch, _font_Main, " Version " + VERSION, _screenW - 8, _screenH - 8, Color.LightYellow, Color.DarkRed);

            //batch.DrawString(Game1._mainFont, "MOUSE OVER " + Input._mouse._isOver, new Vector2(Input._mouse._x, Input._mouse._y), Color.DeepSkyBlue);
            //_window.Batch.DrawString(_font_Main, Input._mouse.AbsX + ":" + Input._mouse.AbsY, new Vector2(Input._mouse._x + 24, Input._mouse._y + 24), Color.DeepSkyBlue);

            //_sprite.Draw(_window.Batch, 800, 480, Color.White, .3f, .3f, Geo.RAD_0);
            //_sprite.Draw(_window.Batch, 200, 480, Color.White, .3f, .3f, Geo.RAD_0, 0, 0, SpriteEffects.FlipHorizontally, true);

            //_sprite.Draw(_window.Batch, 1400, 480, Color.White, 2f, 2f, Geo.RAD_0, 0, 0, SpriteEffects.FlipHorizontally);

            //_window.Batch.Draw(Retro2D.Draw._mouseCursor, new Vector2(_relMouseX, _relMouseY), Color.Yellow);

            //float scale = .6f;

            //_sprite_Monster0.Render(_window.Batch, 400, 400, Color.White, scale, scale);
            //_sprite.Render(_window.Batch, 500, 400, Color.White, scale, scale, 0, 0, 0, SpriteEffects.FlipHorizontally, true);
            //_sprite2.Render(_window.Batch, 600, 400, Color.White, scale, scale);//, 0, 0, 0, SpriteEffects.FlipHorizontally, true);


            _window.Batch.End();

            // Render MainTarget in FinalTarget
            _window.SetRenderTarget(_window.FinalRenderTarget);
            _window.Batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);

                _window.RenderMainTarget(Color.White);

            _window.Batch.End();

            // Flip to FinalRenderTarget ! 
            _window.SetRenderTarget(null);
            _window.Batch.Begin(SpriteSortMode.Deferred, BlendState.NonPremultiplied, SamplerState.AnisotropicClamp);

                _window.RenderFinalTarget(Color.White);

            _window.Batch.End();

            base.Draw(gameTime);
        }
    }
}