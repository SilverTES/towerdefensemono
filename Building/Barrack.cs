﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
//using MonoGame.Extended;
using Retro2D;
using System.Collections.Generic;

namespace TowerDefenseMono.Building
{
    class Barrack : BaseBuilding
    {
        #region Attributes

        public static Vector2 RangeLevel1 { get; private set; } = new Vector2(280, 210);
        public static Vector2 RangeLevel2 { get; private set; } = new Vector2(320, 240);
        public static Vector2 RangeLevel3 { get; private set; } = new Vector2(360, 270);

        List<Vector2> _listRallyPoint = new List<Vector2>();
        List<Unit.Marine> _listMarines = new List<Unit.Marine>();

        int _tempoRespawn = 800;
        int _ticRespawn = 0;
        bool _isRespawn = false;

        protected Button _buttonLevelUp;
        protected Button _buttonRallyPoint;

        bool _waitRallyPoint = false;
        bool _mouseInRange = false;

        ProgressBar _respawnBar;

        #endregion

        public Barrack(Player player, Input.Mouse mouse, Node layerUI, Vector2 defaultRallyPoint, Vector2 range) : base(player, mouse, layerUI, range)
        {
            _subType = UID.Get<Barrack>();

            SetName("Barrack");

            SetSize(80, 80);
            SetPivot(Position.CENTER);
            _rallyPoint = _defaultRallyPoint = defaultRallyPoint;

            _uiSelect = new UICircleSelect(_mouse, 96);
            _uiSelect.AppendTo(_layerUI);

            _buttonLevelUp = _uiSelect.AddButton(new Button(_player, Game1._tex_Icon_Tower00, _mouse, new Message(UID.Get<Button>(), "UPGRADE_BARRACK", this), "Upgrade Barrack", true, 110), Geo.RAD_U);
            _buttonRefund = _uiSelect.AddButton(new Button(_player, Game1._tex_Icon_Refund, _mouse, new Message(UID.Get<Button>(), "KILL_TOWER", this), "Refund +60", false, -90, 48, 48), Geo.RAD_D);
            _buttonRallyPoint = _uiSelect.AddButton(new Button(_player, Game1._tex_Icon_Goal, _mouse, new Message(UID.Get<Button>(), "RALLY_POINT", this), "Set Rally Point <R>", false, 0), Geo.RAD_DR);

            _listPosAttack.Add(new Vector2(0, -24));


            _listRallyPoint.Add(new Vector2());
            _listRallyPoint.Add(new Vector2());
            _listRallyPoint.Add(new Vector2());

            //_listRallyPoint.Add(_rallyPoint + (Geo.GetVector(-Geo.RAD_360 / 3 + Geo.RAD_90) * 32));
            //_listRallyPoint.Add(_rallyPoint + (Geo.GetVector(Geo.RAD_360 / 3 + Geo.RAD_90) * 32));
            //_listRallyPoint.Add(_rallyPoint + (Geo.GetVector(Geo.RAD_D) * 32));

            SetRallyPoint(_rallyPoint, 40);

            _respawnBar = new ProgressBar(_tempoRespawn, _tempoRespawn, 48, 8, Color.Green, Color.Black, Color.Black * .6f, 2, 8);

            Init();
        }

        public override Node Init()
        {
            _level = 1;
            Game1._sound_Open2.Play(.04f * Game1._volumeSound, .01f, 0);
            //System.Console.WriteLine("_defaultRallyPoint = " + _defaultRallyPoint);
            return base.Init();
        }

        public void SetRallyPoint(Vector2 rallyPoint, float radius = 40)
        {
            _rallyPoint = rallyPoint;
            // Create Rally Point of the Soldier Unit !
            _listRallyPoint[0] = _rallyPoint + (Geo.GetVector(-Geo.RAD_360 / 3 + Geo.RAD_90) * radius);
            _listRallyPoint[1] = _rallyPoint + (Geo.GetVector(Geo.RAD_360 / 3 + Geo.RAD_90) * radius);
            _listRallyPoint[2] = _rallyPoint + (Geo.GetVector(Geo.RAD_D) * radius);

        }

        public void MoveSoldierToRallyPoint()
        {
            for (int i = 0; i < _listMarines.Count; i++)
            {
                //_listSoldier[i].AddGoal(_listRallyPoint[i].X, _listRallyPoint[i].Y);
                _listMarines[i].SetTarget(null);
                _listMarines[i].GotoGoal(_listMarines[i].XY, _listRallyPoint[i]);
                _listMarines[i].SetRallyPoint(_listRallyPoint[i]);
            }
        }

        public override Node Update(GameTime gameTime)
        {
            _respawnBar.SetValue(_ticRespawn);

            #region MessageProcess
            if (_uiSelect.HasMessage())
            {
                if (_uiSelect._message._type == UID.Get<Button>())
                {
                    string receivedText = _uiSelect._message._data.ToString();

                    System.Console.WriteLine("Message = " + receivedText);

                    if (receivedText == "KILL_TOWER")
                    {
                        // Restore Buildable
                        new Buildable(_player, _mouse, _layerUI, XY, _defaultRallyPoint)
                            .AppendTo(_parent);

                        _autoDestroy = true;
                        _uiSelect.Hide();

                        Game1._sound_TowerKill.Play(.1f * Game1._volumeSound, .1f, 0);

                        // Kill all soldier from this Barrack
                        foreach(Unit.Marine unit in _listMarines)
                        {
                            unit.KillUnit();
                        }
                    }

                    if (receivedText == "RALLY_POINT")
                    {
                        _waitRallyPoint = true;
                        _uiSelect.Hide();
                    }

                    if (receivedText == "UPGRADE_BARRACK")
                    {
                        //new Tower(_mouse, _layerUI)
                        //    .SetPosition(XY)
                        //    .AppendTo(_parent);

                        //// Kill this UI & Selectable item
                        //_uiSelect.KillMe();
                        //KillMe();

                    }
                }

                _uiSelect.EndMessage();
            }

            if (HasMessage())
            {
                if (_message._type == UID.Get<Unit.Marine>())
                {
                    string receivedText = _message._data.ToString();

                    System.Console.WriteLine("Message = " + receivedText);

                    for (int i = 0; i < _listMarines.Count; i++)
                    {
                        System.Console.WriteLine("Soldier = " + i + " : " + _listMarines[i].IsDead);

                        if (_listMarines[i].IsDead)
                        {
                            _isRespawn = true;
                            //Unit.Soldier unit = new Unit.Soldier(_mouse, this);
                            //unit
                            //    .SetSpeed(2f)
                            //    .SetPosition(_x, _y)
                            //    .AppendTo(_parent);

                            //unit.AddGoal(_listRallyPoint[i].X, _listRallyPoint[i].Y);

                            //_listSoldier[i] = unit;
                        }

                    }


                }

                EndMessage();
            }

            if (_isRespawn)
                _ticRespawn++;

            if (_ticRespawn > _tempoRespawn)
            {
                _isRespawn = false;
                _ticRespawn = 0;

                bool playSound = false;
                
                for (int i = 0; i < _listMarines.Count; i++)
                {
                    if (_listMarines[i].IsDead)
                    {
                        Unit.Marine unit = new Unit.Marine(_player, _mouse, this, Unit.Marine.RangeLevel1);
                        unit
                            .SetSpeed(2f)
                            .SetPosition(_x, _y)
                            .AppendTo(_parent);

                        unit.AddGoal(_listRallyPoint[i].X, _listRallyPoint[i].Y);

                        unit.SetRallyPoint(_listRallyPoint[i]);

                        _listMarines[i] = unit;

                        playSound = true;
                    }

                }

                if (playSound && InRectView())
                    //Game1._sound_ReadyToFight.Play(.2f, .01f, 0f);
                    Game1.PlaySound(1, Game1._sounds_Unit[Misc.Rng.Next(0, 5)], .1f, .01f);
            }

            #endregion

            if (_state == State.IS_ACTIVE)
            {
                _mouseInRange = false;
                // Set Rally Point if click on Range Zone
                if (Collision2D.PointInEllipse(new Vector2(_mouse._x, _mouse._y), AbsXY, _range.X, _range.Y ))
                {
                    _mouseInRange = true;

                    if (_mouse._onClick && _isSelected && !_mouse._isOver && _waitRallyPoint)
                    {
                        System.Console.WriteLine("Set Rally Point : "+_mouse._x + ":"+_mouse._y);
                        
                        SetRallyPoint(new Vector2(_mouse.AbsX, _mouse.AbsY), 40);
                        MoveSoldierToRallyPoint();

                        if (InRectView())
                            //Game1._sound_Rodger.Play(.3f, .02f, 0f);
                            Game1.PlaySound(1, Game1._sounds_Unit[Misc.Rng.Next(0, 5)], .1f, .01f);

                        new Particles.RallyPointCursor(new Vector2(_mouse.AbsX, _mouse.AbsY)).AppendTo(_layerUI);

                        _waitRallyPoint = false;
                    }
                }

                if (!_isSelected)
                    _waitRallyPoint = false;

                // Create unit when is active
                if (_onFinishBuild)
                {
                    System.Console.WriteLine("Create Soldier");
                    for (int i = 0; i < _listRallyPoint.Count; i++)
                    {
                        Unit.Marine unit = new Unit.Marine(_player, _mouse, this, Unit.Marine.RangeLevel1);
                        unit
                            .SetSpeed(2f)
                            .SetPosition(_x, _y)
                            .AppendTo(_parent);

                        unit.AddGoal(_listRallyPoint[i].X, _listRallyPoint[i].Y);

                        unit.SetRallyPoint(_listRallyPoint[i]);

                        _listMarines.Add(unit);
                    }

                    if (InRectView())
                        //Game1._sound_ReadyToFight.Play(.4f, .01f, 0f);
                        Game1.PlaySound(1, Game1._sounds_Unit[Misc.Rng.Next(0, 5)], .1f, .01f);
                }

                //if (_target != null)
                //{
                //    //    _onAttack = false;
                //    //    _ticAttack++;

                //    //    if (_ticAttack > _tempoAttack)
                //    //    {
                //    //        _ticAttack = 0;
                //    //        _onAttack = true;
                //    //    }

                //    if (_target._type == UID.Get<BaseUnit>())
                //    {
                //        var unit = _target.This<BaseUnit>();

                //        //unit._isTargetBy = this; // Mark unit as target by 

                //        //if (_onAttack)
                //        //{

                //        //    unit.HitDamage(-_powerAttack);
                //        //    Game1._sound_Gun.Play(.01f, .1f, 0f);

                //        //}

                //        if (!Collision2D.PointInEllipse(_target.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2)
                //            || unit.IsDead())
                //        {

                //            System.Console.WriteLine("**** Marines return to RallyPoint !");

                //            unit._isTargetBy = null; // Mark unit as target

                //            _ticAttack = 0;
                //            _target = null;

                //            // Solider return to rallypoint
                //            for (int i=0; i< _listMarines.Count; i++)
                //            {
                //                if (!_listMarines[i].IsFight())
                //                {
                //                    _listMarines[i].UpdateRect();
                //                    _listMarines[i].GotoGoal(_listMarines[i].XY, _listRallyPoint[i]);
                //                }
                //                else
                //                    _listMarines[i].SetHoldPosition(_listRallyPoint[i]);
                //            }

                //        }
                //    }

                //}
            }

            // Highlight Soldier if selected !!
            if (_offSelected || (_offMouseOver && !_isSelected))
                foreach (var soldier in _listMarines)
                    soldier._isOwnerSelected = false;

            if (_onSelected || (_onMouseOver && !_isSelected))
                foreach (var soldier in _listMarines)
                    soldier._isOwnerSelected = true;


            if (_uiSelect.IsShow)
            {
                if (_buttonLevelUp._isMouseOver) { _displayRange = RangeLevel2; }
                if (_buttonRefund._isMouseOver) { _displayRange = Vector2.Zero; }
                if (_buttonRallyPoint._isMouseOver) { _displayRange = _range; }

                if (Input.Button.OnePress("RallyPoint", Keyboard.GetState().IsKeyDown(Keys.R))) // ShortCut Set Rally Point
                {
                    _waitRallyPoint = true;
                    _uiSelect.Hide();
                }
            }

            return base.Update(gameTime);
        }

        public override Node Render(SpriteBatch batch)
        {
            if (_waitRallyPoint)
            {
                Game1.DrawGradientCircle(batch, AbsXY, _displayRange.X, _displayRange.Y, Color.Yellow * .2f);

                if (_mouseInRange)
                {
                    Game1.DrawGradientCircle(batch, new Vector2(_mouse._x,_mouse._y), 32, 24, Color.Yellow);
                    Draw.FillSquare(batch, new Vector2(_mouse._x, _mouse._y), 4, Color.Yellow);
                }
            }

            float rx = 64;
            float ry = 64;

            batch.Draw(Game1._tex_Barrack, new Rectangle((int)(AbsX - rx), (int)(AbsY - ry), (int)rx * 2, (int)ry * 2), Color.White * _alphaTower);

            if (_isRespawn)
                _respawnBar.Render(batch, AbsX, AbsY - 64);



            // Debug
            //for (int i = 0; i < _listRallyPoint.Count; i++)
            //{
            //    Draw.Point(batch, _listRallyPoint[i] + _parent.AbsXY, 3, Color.Blue);
            //}

            // Debug
            //if (_isSelected)
            //{
            //    batch.DrawLine(AbsXY, _rallyPoint, Color.Yellow * .5f, 2);

            //    for (int i = 0; i < _listRallyPoint.Count; i++)
            //    {
            //        batch.DrawPoint(_listRallyPoint[i], Color.GreenYellow, 4);
            //    }
            //}

            return base.Render(batch);
        }
    }
}
