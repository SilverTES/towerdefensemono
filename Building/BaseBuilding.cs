﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using MonoGame.Extended;
using Retro2D;
using System.Collections.Generic;

namespace TowerDefenseMono
{
    class BaseBuilding : Selectable
    {
        protected enum State
        {
            IS_BUILD, // currently in construction !
            IS_ACTIVE // ready to fight !
        }

        #region Attributes

        public const int ZONE_RANGE = 0;

        // Optimisation !
        int _tempoScanRange = 30;
        int _ticScanRange = 0;

        protected bool _onTarget = false; // Detect one target !
        protected Node _target; // First enemy in zone range !

        protected Vector2 _defaultRallyPoint; // RallyPoint for allies unit !
        protected Vector2 _rallyPoint; // RallyPoint for allies unit !
        public Vector2 DefaultRallyPoint => _defaultRallyPoint;

        protected List<Vector2> _listPosAttack = new List<Vector2>();

        protected State _state;
        public int _level { get; protected set; }

        // Build
        protected int _tempoBuild = 40;
        protected int _ticBuild;
        protected bool _onFinishBuild = false;

        // Attack
        protected int _powerAttack = 8;
        protected float _speedAttack = .01f;
        protected float _ticAttack = 0;
        protected bool _onAttack = false;

        // Range
        protected Vector2 _range;
        protected Vector2 _displayRange;

        protected float _alphaTower = .2f;
        protected float _angle = Geo.RAD_0;

        protected RectangleF _rectZoneRange = new RectangleF(0,0,640,480);
        protected Node _layerUI;
        protected UICircleSelect _uiSelect;
        protected bool _autoDestroy = false;

        protected Button _buttonRefund;
        protected ProgressBar _buildBar;

        #endregion

        public BaseBuilding(Player player, Input.Mouse mouse, Node layerUI, Vector2 range) : base(player, mouse)
        {
            _type = UID.Get<BaseBuilding>();

            _range = range;
            _displayRange = _range;

            // Range of the building attack !
            _rectZoneRange.Width = _range.X * 2;
            _rectZoneRange.Height = _range.Y * 2;

            SetCollideZone(ZONE_RANGE, _rectZoneRange);

            _layerUI = layerUI;

            _buildBar = new ProgressBar(0, _tempoBuild, 80, 8, Color.Orange, Color.Black, Color.Black * .6f, 1, 8);

            _ticBuild = _tempoBuild;
        }
        public override Node Update(GameTime gameTime)
        {
            _onFinishBuild = false;
            _onTarget = false;

            _angle += .01f;
            if (_angle > Geo.RAD_360) _angle = Geo.RAD_0;

            UpdateRect();

            _z = -_y;

            _uiSelect.SetPosition(XY);

            _displayRange = _range;

            if (_state == State.IS_BUILD)
            {
                _ticBuild -= 1;

                if (_ticBuild <= 0)
                {
                    _ticBuild = 0;
                    _state = State.IS_ACTIVE;
                    _alphaTower = 1f;

                    _buildBar.SetVisible(false);

                    _onFinishBuild = true;

                    //new Smoke(_x, _y - 16, 4, true, false, 32, 24)
                    //    .AppendTo(_parent);
                }

                float value = _tempoBuild - _ticBuild;

                _buildBar.SetValue(value);
                //_buildBar.SetValue(Easing.GetValue(Easing.CubicEaseIn,value, 0, _tempoBuild, _tempoBuild));

                _uiSelect
                    .SetActive(false);
            }

            if (_state == State.IS_ACTIVE)
            {
                #region Collisions
                _rectZoneRange.Position = new Vector2(-_rectZoneRange.Width / 2, -_rectZoneRange.Height / 2) + XY;

                _ticScanRange++;
                UpdateCollideZone(ZONE_RANGE, _rectZoneRange, _ticScanRange > _tempoScanRange);
                if (_ticScanRange > _tempoScanRange) _ticScanRange = 0;

                var _listZone = Collision2D.ListCollideZoneByNodeType(GetCollideZone(ZONE_RANGE), UID.Get<BaseUnit>(), BaseUnit.ZONE_BODY);

                if (_listZone.Count>0 && _target == null)
                {
                    int minGoalCount = 1000;
                    
                    for (int i=0; i< _listZone.Count; i++)
                    {
                        Node collider = _listZone[i]._node;

                        if (Collision2D.PointInEllipse(collider.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2))
                        {
                            // if is a MobilUnit
                            if (collider._subType == UID.Get<Unit.Enemy0>())
                            {
                                BaseUnit unit = collider.This<Unit.Enemy0>();

                                // Select the most Far Unit ! near the goal
                                if (unit.QueueGoalCount < minGoalCount)
                                {
                                    minGoalCount = unit.QueueGoalCount;
                                    
                                    if (unit._subType == UID.Get<Unit.Enemy0>()) // Target only if is an enemy
                                        _target = unit;

                                    _onTarget = true;
                                }
                            }

                        }

                    }

                }
                #endregion

                #region UI Interactions
                _isMouseOver = Collision2D.PointInEllipse(new Vector2(_mouse._x, _mouse._y), AbsXY, _rect.Width / 2, _rect.Height / 2) && !_mouse._isOver;

                if (_onSelected)
                {
                    _uiSelect.Show();
                    //System.Console.WriteLine("Show UISelect");
                    Game1._sound_Key.Play(.05f * Game1._volumeSound, .4f, 0f);
                }

                if (_offSelected)
                {
                    _uiSelect.Hide();
                    //System.Console.WriteLine("Hide UISelect");
                }

                //if (_uiSelect._isActive)
                //_uiSelect.Update(gameTime);
                #endregion

            }

            if (_autoDestroy && _uiSelect._animate.Off("hide"))
            {
                // Kill this UI & Selectable item
                new ToonSmoke(_x, _y - 16, 1, true, false, 8, 4)
                    .AppendTo(_parent);

                _uiSelect.KillMe();
                KillMe();
            }

            
            

            return base.Update(gameTime);
        }
        public override Node Render(SpriteBatch batch)
        {
            float value = (_uiSelect._animate.Value() / 100f);

            if (_isMouseOver  && _state == State.IS_ACTIVE)
                Game1.DrawGradientCircle(batch, AbsXY, _displayRange.X , _displayRange.Y, Color.Yellow * .2f);

            if (!_isMouseOver && _isSelected)
                Game1.DrawGradientCircle(batch, AbsXY, _displayRange.X * value, _displayRange.Y * value, Color.LightSkyBlue * .2f);

            //batch.Draw(Game1._radar00, new Rectangle((int)(AbsX - rx), (int)(AbsY - ry) - 24, 64, 64), Color.ForestGreen);

            if (_isSelected)
            {
                batch.Draw(Game1._tex_Radar00, AbsXY, new Rectangle(0,0,256,256), Color.RoyalBlue * value *.2f, _angle, new Vector2(128, 128), value, SpriteEffects.None, 0);
                //Draw.CenterBorderedStringXY(batch, Game1._font_Main, _uiSelect._buttonOverMessage, AbsX, AbsY, Color.Yellow, Color.DarkRed);
            }


            //if (_uiSelect._isVisible && _state == State.IS_ACTIVE)
            //    Game1.DrawGradientCircle(batch, AbsXY, _rect.Width * value, _rect.Width * value, Color.CornflowerBlue * value *.5f);


            // Debug
            //batch.DrawRectangle(GetCollideZone(ZONE_RANGE)._rect, _target != null ? Color.OrangeRed:Color.Orange * .2f);


            //Draw.CenterBorderedStringXY(batch, Game1._font_Main, _name + " lvl " + _level, AbsX, AbsY - 80, Color.CadetBlue, Color.DarkBlue);


            _buildBar.Render(batch, AbsX, AbsY + _rect.Height/2);

            return base.Render(batch);
        }
    }
}
