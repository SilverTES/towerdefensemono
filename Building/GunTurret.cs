﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
//using MonoGame.Extended;
using Retro2D;
using System.Collections.Generic;

namespace TowerDefenseMono.Building
{
    class GunTurret : BaseBuilding
    {
        #region Attributes

        protected Button _buttonLevelUp;
        protected Button _buttonPowerUp;
        protected Button _buttonSpeedUp;
        public static Vector2 RangeLevel1 { get; private set; } = new Vector2(320, 240);
        public static Vector2 RangeLevel2 { get; private set; } = new Vector2(360, 270);
        public static Vector2 RangeLevel3 { get; private set; } = new Vector2(400, 300);

        #endregion

        public GunTurret(Player player, Input.Mouse mouse, Node layerUI, Vector2 defaultRallyPoint, Vector2 range) : base(player, mouse, layerUI, range)
        {
            _subType = UID.Get<GunTurret>();

            SetName("GunTurret");
            SetSize(64, 64);
            SetPivot(Position.CENTER);

            _rallyPoint = _defaultRallyPoint = defaultRallyPoint;

            _uiSelect = new UICircleSelect(_mouse, 96);
            _uiSelect.AppendTo(_layerUI);

            _buttonSpeedUp = _uiSelect.AddButton(new Button(_player, Game1._tex_Icon_Tower00, _mouse, new Message(UID.Get<Button>(), "SPEED_UP", this), "Speed Up Attack", true, 50), - Geo.RAD_90 - Geo.RAD_45);
            _buttonPowerUp = _uiSelect.AddButton(new Button(_player, Game1._tex_Icon_Tower00, _mouse, new Message(UID.Get<Button>(), "POWER_UP", this), "Power Up Attack", true, 50), - Geo.RAD_45);
            _buttonLevelUp = _uiSelect.AddButton(new Button(_player, Game1._tex_Icon_Tower00, _mouse, new Message(UID.Get<Button>(), "UPGRADE_TURRET", this), "Upgrade GunTurret", true, 110), Geo.RAD_U);
            _buttonRefund = _uiSelect.AddButton(new Button(_player, Game1._tex_Icon_Refund, _mouse, new Message(UID.Get<Button>(), "KILL_TOWER", this), "Refund +50", false, -60, 48, 48), Geo.RAD_D);

            _listPosAttack.Add(new Vector2(0, -48));

            Init();
        }
        public override Node Init()
        {
            _level = 1;

            _powerAttack = 20;
            _speedAttack = .012f;

            Game1._sound_Open2.Play(.04f * Game1._volumeSound, .01f, 0);

            return base.Init();
        }
        public override Node Update(GameTime gameTime)
        {
            #region MessageProcess
            if (_uiSelect.HasMessage())
            {
                string receivedText = _uiSelect._message._data.ToString();

                System.Console.WriteLine("Message = " + receivedText);

                if (receivedText == "KILL_TOWER")
                {

                    new Buildable(_player, _mouse, _layerUI, XY, _defaultRallyPoint)
                        .AppendTo(_parent);

                    _autoDestroy = true;
                    _uiSelect.Hide();

                    Game1._sound_TowerKill.Play(.1f * Game1._volumeSound, .1f, 0);

                }

                if (receivedText == "UPGRADE_TURRET")
                {

                    //new Tower(_mouse, _layerUI)
                    //    .SetPosition(XY)
                    //    .AppendTo(_parent);

                    //// Kill this UI & Selectable item
                    //_uiSelect.KillMe();
                    //KillMe();

                }

                _uiSelect.EndMessage();
            }

            #endregion

            if (_state == State.IS_ACTIVE)
            {
                if (_target != null)
                {
                    if (_onTarget)
                        Game1.PlaySound(3, Game1._sound_LaserReload, .02f, .01f);

                    _onAttack = false;
                    _ticAttack+=_speedAttack;

                    if (_ticAttack >= 1f)
                    {
                        _ticAttack = 0;
                        _onAttack = true;
                    }

                    if (_target._type == UID.Get<BaseUnit>())
                    {
                        var unit = _target.This<BaseUnit>();

                        //unit._isTargetBy = this; // Mark unit as target

                        if (_onAttack)
                        {
                            unit.ShowHitDamage(_powerAttack); // Visual Effect

                            unit.HitDamage(-_powerAttack);
                            //Game1._sound_Gun.Play(.04f, .1f, 0f);
                            if (InRectView(Gfx.ExtendRect(_parent._rectView, 300)))
                                Game1.PlaySound(2, Game1._sound_PowerGun, .04f, .01f);

                        }

                        if (!Collision2D.PointInEllipse(_target.XY, XY, _rectZoneRange.Width / 2, _rectZoneRange.Height / 2)
                            || unit.IsDead)
                        {
                            unit._isTargetBy = null; // Mark unit as target

                            _ticAttack = 0;
                            _target = null;
                        }
                    }

                }
            }

            if (_uiSelect.IsShow)
            {
                if (_buttonSpeedUp._isMouseOver) { _displayRange = Vector2.Zero; }
                if (_buttonLevelUp._isMouseOver) { _displayRange = RangeLevel2; }
                if (_buttonRefund._isMouseOver) { _displayRange = Vector2.Zero; }
            }

            return base.Update(gameTime); ;
        }
        public override Node Render(SpriteBatch batch)
        {
            float rx = 64;
            float ry = 64;

            batch.Draw(Game1._tex_GunTurret, new Rectangle((int)(AbsX - rx), (int)(AbsY - ry)-32, (int)rx * 2, (int)ry * 2), Color.White * _alphaTower);

            if (_target != null)
            {

                if (_onAttack)
                {
                    //Game1.DrawGradientCircle(batch, _target.AbsXY, 32, 24, Color.Red);
                    Draw.Circle(batch, _target.AbsXY, 16, 16, Color.Red, 8);
                    Draw.Line(batch, AbsXY + _listPosAttack[0], _target.AbsXY, Color.Red*.3f, 16);
                    Draw.Line(batch, AbsXY + _listPosAttack[0], _target.AbsXY, Color.Orange*.5f, 12);
                    Draw.Line(batch, AbsXY + _listPosAttack[0], _target.AbsXY, Color.Yellow, 8);
                }
                else
                {
                    Draw.Line(batch, AbsXY + _listPosAttack[0], _target.AbsXY, Color.Red * .5f, 1);
                    Draw.Line(batch, AbsXY + _listPosAttack[0], _target.AbsXY, Color.Red * .5f, 3);
                }
            }

            return base.Render(batch);
        }
    }
}
